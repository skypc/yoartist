/*
 *
 */
package com.skypc.yoartist.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.skypc.yoartist.ihandler.IHomeView;
import com.skypc.yoartist.ihandler.IUserView;
import com.skypc.yoartist.ihandler.IUserView2;

/**
 *
 * @author manoj
 *
 */
@Controller
public class HomeController {
	@Autowired
	private IUserView userView;

	/**
	 *
	 * @return
	 */

	@Autowired
	private IHomeView homeView;

	@Autowired
	private IUserView2 userView2;

	@RequestMapping(value="/")
	public ModelAndView getHome(){
		return new ModelAndView("index");
	}
}
