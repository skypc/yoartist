/*
 *
 */
package com.skypc.yoartist.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.ihandler.IHomeView;
import com.skypc.yoartist.ihandler.IUserView;
import com.skypc.yoartist.ihandler.IUserView2;

/**
 *
 *
 * @author musaddique
 *
 */

@Controller
@RequestMapping(value = "user")
public class UserController {

	@Autowired
	private IUserView userView;

	@Autowired
	private IUserView2 userView2;

	@Autowired
	private IHomeView homeView;
	
	@RequestMapping(value="signin")
	public ModelAndView signInAccount(HttpServletRequest request, RedirectAttributes attr,HttpServletResponse response)throws YOAException{
		return userView.signInAccount(request,attr,response);
	}

	@RequestMapping(value="register")
	public ModelAndView registerUser(HttpServletRequest request, RedirectAttributes attr)throws YOAException{
		return userView.registerUser(request,attr);
	}
	
	@RequestMapping(value="confirmEmail")
	public ModelAndView confirmEmail(@RequestParam("token") String token, HttpServletRequest request, RedirectAttributes attr)throws YOAException{
		return userView.confirmEmail(token,request,attr);
	}
	
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, SessionStatus sessionStatus) {
		sessionStatus.setComplete();
		request.getSession().invalidate();
		return "redirect:/";
	}
	
	
	@RequestMapping(value="home")
	public ModelAndView homePage(HttpServletRequest request, RedirectAttributes attr)throws YOAException{
		return userView.homePage(request,attr);		
	}
	
	@RequestMapping(value="shared")
	public ModelAndView sharePage(HttpServletRequest request, RedirectAttributes attr)throws YOAException{
		return userView.sharePage(request,attr);		
	}
	

	@RequestMapping(value="profile")
	public ModelAndView profilePage(HttpServletRequest request, RedirectAttributes attr)throws YOAException{
		return new ModelAndView("profile");
	}

	@RequestMapping(value="submitPost",method=RequestMethod.POST)
	public ModelAndView submitPost(@RequestParam("comments") String comments,@RequestParam("uploadedId") Integer uploadedId, HttpServletRequest request, RedirectAttributes attr)throws YOAException{
		return userView.submitPost(comments,uploadedId,request,attr);		
	}
	
	@RequestMapping(value="messages")
	public ModelAndView message()throws YOAException{
		return new ModelAndView("message");		
	}
	@RequestMapping(value="connections")
	public ModelAndView connections()throws YOAException{
		return new ModelAndView("connections");		
	}
}



