package com.skypc.yoartist.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.skypc.yoartist.exception.YOAException;

/**
 * 
 * 
 * @author vivek
 *
 */

@Controller
@RequestMapping(value="errorHandler")
public class ErrorController {

	@RequestMapping(value="/404", method = RequestMethod.GET)
	public ModelAndView handle404Error(HttpServletRequest request) throws YOAException{
		
		return new ModelAndView("error/pages-error-404");
	}
	
}
