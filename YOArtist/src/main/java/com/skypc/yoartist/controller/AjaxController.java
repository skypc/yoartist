package com.skypc.yoartist.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.skypc.yoartist.bean.ConnectionActivity;
import com.skypc.yoartist.bean.CurrentLoginUser;
import com.skypc.yoartist.bean.EducationBean;
import com.skypc.yoartist.bean.EmploymentBean;
import com.skypc.yoartist.bean.MessageUserBean;
import com.skypc.yoartist.bean.MessageUserChatsBean;
import com.skypc.yoartist.bean.NotificationBean;
import com.skypc.yoartist.bean.ResponseBean;
import com.skypc.yoartist.bean.SummaryBean;
import com.skypc.yoartist.bean.UserToConnectBean;
import com.skypc.yoartist.constants.YOArtistErrorCode;
import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.ihandler.IAjaxView;

@RestController
@RequestMapping(value="ajax")
public class AjaxController {
	
	@Autowired
	private IAjaxView ajaxView;
	
	@RequestMapping("uerPostMedia")
	@ResponseBody
	public String uploadMedia(HttpServletRequest request,@RequestParam("uploadFile") MultipartFile file )throws YOAException{
		return ajaxView.uploadUserPost(request, file);
	}
	@RequestMapping("likePost")
	@ResponseBody
	public String likePost(HttpServletRequest request,@RequestParam("postId") long postId)throws YOAException{
		return ajaxView.likePost(request, postId);
	}
	
	@RequestMapping("users-to-connect")
	@ResponseBody
	public List<UserToConnectBean> usersToConnect(HttpServletRequest request)throws YOAException{
		return ajaxView.usersToConnect(request);
	}
	
	@RequestMapping("users-for-search-box")
	@ResponseBody
	public List<UserToConnectBean> usersForSearchBox(HttpServletRequest request)throws YOAException{
		return ajaxView.usersToAutoSearchBox(request);
	}
	
	@RequestMapping("comment-on-post")
	@ResponseBody
	public String commentOnPost(HttpServletRequest request,@RequestParam("postId") long postId,@RequestParam("comment") String comment)throws YOAException{
		return ajaxView.commentOnPost(request, postId,comment);
	}
	
	@RequestMapping("send-connect-request")
	@ResponseBody
	public String sendConnectRequest(HttpServletRequest request,@RequestParam("userId") long userId)throws YOAException{
		return ajaxView.sendConnectRequest(request, userId);
	}
	
	@RequestMapping("accept-connect-request")
	@ResponseBody
	public String acceptConnectRequest(HttpServletRequest request,@RequestParam("userId") long userId,@RequestParam("isAccept") boolean isAccept)throws YOAException{
		return ajaxView.acceptConnectRequest(request, userId,isAccept);
	}
	
	@RequestMapping("send-message-to-user")
	@ResponseBody
	public String connectUser(HttpServletRequest request,@RequestParam("userId") long userId,@RequestParam("message") String message)throws YOAException{
		return ajaxView.sendMessageToUser(request, userId,message);
	}
	@RequestMapping("get-limited-notifications")
	@ResponseBody
	public List<NotificationBean> getLimitedNotification(HttpServletRequest request)throws YOAException{
		return ajaxView.getNotifications(request,true);
	}
	
	@RequestMapping("get-all-notifications")
	@ResponseBody
	public List<NotificationBean> getAllNotification(HttpServletRequest request)throws YOAException{
		return ajaxView.getNotifications(request,false);
	}
	
	@RequestMapping("get-notifications-count")
	@ResponseBody
	public int getNotificationCount(HttpServletRequest request)throws YOAException{
		return ajaxView.getNotificationOrConnectionCount(request,true);
	}
	
	@RequestMapping("get-all-connect-request")
	@ResponseBody
	public List<NotificationBean> getAllConnectRequest(HttpServletRequest request)throws YOAException{
		return ajaxView.getAllConnectRequest(request);
	}
	
	@RequestMapping("get-connect-request-count")
	@ResponseBody
	public int getConnectRequestCount(HttpServletRequest request)throws YOAException{
		return ajaxView.getNotificationOrConnectionCount(request,false);
	}
	
	@RequestMapping("get-message-users")
	@ResponseBody
	public List<MessageUserBean> getMessageUsers(HttpServletRequest request)throws YOAException{
		return ajaxView.getMessageUsers(request);
	}
	
	@RequestMapping("get-message-user-chats")
	@ResponseBody
	public List<MessageUserChatsBean> getMessageUsersChat(HttpServletRequest request,@RequestParam("userId") long userId)throws YOAException{
		return ajaxView.getMessageUsersChat(request,userId);
	}
	
	@RequestMapping("get-current-login-user")
	@ResponseBody
	public CurrentLoginUser getCurrentLoginUser(HttpServletRequest request)throws YOAException{
		return ajaxView.getCurrentLoginUser(request);
	}
	
	@RequestMapping("connected-user")
	@ResponseBody
	public List<UserToConnectBean> connectedUser(HttpServletRequest request)throws YOAException{
		return ajaxView.connectedUser(request);
	}
	
	@RequestMapping("connection-activity")
	@ResponseBody
	public List<ConnectionActivity> getConnectionActivity(HttpServletRequest request,@RequestParam("isFirst") boolean isFirst)throws YOAException{
		return ajaxView.getConnectionActivity(request,isFirst);
	}
	
	@RequestMapping(value="deleteUploadedMedia", method=RequestMethod.POST)
	@ResponseBody
	public String deleteUploadedMedia(HttpServletRequest request,@RequestParam("id") String id)throws YOAException{
		//return ajaxView.getConnectionActivity(request,isFirst);
		return "1";
	}
	
	@RequestMapping(value="user-connection",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseBean> getUserConnection(HttpServletRequest request)throws YOAException{
		
		try{
			return new ResponseEntity<ResponseBean>(ajaxView.getUserConnection(request), HttpStatus.OK);
		}
		catch(YOAException yo){
			if(yo.getErrCode()!=null && yo.getErrCode().equals(YOArtistErrorCode.UNAUTHORIZED_USER)){
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.UNAUTHORIZED);
			}
			else{
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(Exception e){
			return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@RequestMapping(value="sharepost",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseBean> sharePost(HttpServletRequest request)throws YOAException{
		
		try{
			return new ResponseEntity<ResponseBean>(ajaxView.sharePost(request), HttpStatus.OK);
		}
		catch(YOAException yo){
			if(yo.getErrCode()!=null && yo.getErrCode().equals(YOArtistErrorCode.UNAUTHORIZED_USER)){
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.UNAUTHORIZED);
			}
			else{
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(Exception e){
			return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	
	@RequestMapping(value="get-user-top-profile",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ResponseBean> getSessionUserProfile(HttpServletRequest request)throws YOAException{
		try{
			return new ResponseEntity<ResponseBean>(ajaxView.getSessionUserProfile(request), HttpStatus.OK);
		}
		catch(YOAException yo){
			if(yo.getErrCode()!=null && yo.getErrCode().equals(YOArtistErrorCode.UNAUTHORIZED_USER)){
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.UNAUTHORIZED);
			}
			else{
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(Exception e){
			return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="education",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseBean> saveEducationData(HttpServletRequest request,@RequestBody EducationBean education)throws YOAException{
		System.out.println();
		try{
			return new ResponseEntity<ResponseBean>(ajaxView.saveEducationDetails(request,education), HttpStatus.OK);
		}
		catch(YOAException yo){
			if(yo.getErrCode()!=null && yo.getErrCode().equals(YOArtistErrorCode.UNAUTHORIZED_USER)){
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.UNAUTHORIZED);
			}
			else{
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(Exception e){
			return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	@RequestMapping(value="education/{id}",method=RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<ResponseBean> updateEducationData(HttpServletRequest request,EducationBean education,@PathVariable("id") long id)throws YOAException{
		try{
			return new ResponseEntity<ResponseBean>(ajaxView.updateEducationDetails(request,education,id), HttpStatus.OK);
		}
		catch(YOAException yo){
			if(yo.getErrCode()!=null && yo.getErrCode().equals(YOArtistErrorCode.UNAUTHORIZED_USER)){
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.UNAUTHORIZED);
			}
			else{
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(Exception e){
			return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@RequestMapping(value="employment", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseBean> saveEmploymentData(HttpServletRequest request, EmploymentBean employment, HttpServletResponse response){
		try{
			return new ResponseEntity<ResponseBean>(ajaxView.saveEmploymentDetails(employment, request), HttpStatus.OK);
		}
		catch(YOAException yo){
			if(yo.getErrCode()!=null && yo.getErrCode().equals(YOArtistErrorCode.UNAUTHORIZED_USER)){
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.UNAUTHORIZED);
			}
			else{
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(Exception e){
			return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="updateEmploymentDetails", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseBean> updateEmploymentData(HttpServletRequest request, EmploymentBean employment, HttpServletResponse response){
		try{
			return new ResponseEntity<ResponseBean>(ajaxView.updateEmploymentDetails(employment, request), HttpStatus.OK);
		}
		catch(YOAException yo){
			if(yo.getErrCode()!=null && yo.getErrCode().equals(YOArtistErrorCode.UNAUTHORIZED_USER)){
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.UNAUTHORIZED);
			}
			else{
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(Exception e){
			return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="employment/{id}", method=RequestMethod.DELETE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	@ResponseBody
	public ResponseEntity<ResponseBean> deleteEmploymentData(HttpServletRequest request, @PathVariable("id") long id, HttpServletResponse response){
		try{
			return new ResponseEntity<ResponseBean>(ajaxView.deleteEmpData(id, request), HttpStatus.OK);
		}
		catch(YOAException yo){
			if(yo.getErrCode()!=null && yo.getErrCode().equals(YOArtistErrorCode.UNAUTHORIZED_USER)){
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.UNAUTHORIZED);
			}
			else{
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(Exception e){
			return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	
	@RequestMapping(value="get-emp-data",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ResponseBean> getEmpData(HttpServletRequest request)throws YOAException{
		try{
			return new ResponseEntity<ResponseBean>(ajaxView.getEmpData(request), HttpStatus.OK);
		}
		catch(YOAException yo){
			if(yo.getErrCode()!=null && yo.getErrCode().equals(YOArtistErrorCode.UNAUTHORIZED_USER)){
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.UNAUTHORIZED);
			}
			else{
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(Exception e){
			return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="summary", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseBean> saveSummaryData(HttpServletRequest request, SummaryBean summary, HttpServletResponse response){
		try{
			return new ResponseEntity<ResponseBean>(ajaxView.saveSummaryDetails(summary, request), HttpStatus.OK);
		}
		catch(YOAException yo){
			if(yo.getErrCode()!=null && yo.getErrCode().equals(YOArtistErrorCode.UNAUTHORIZED_USER)){
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.UNAUTHORIZED);
			}
			else{
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(Exception e){
			return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="get-summary-data",method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ResponseBean> getSummaryData(HttpServletRequest request)throws YOAException{
		try{
			return new ResponseEntity<ResponseBean>(ajaxView.getSummaryData(request), HttpStatus.OK);
		}
		catch(YOAException yo){
			if(yo.getErrCode()!=null && yo.getErrCode().equals(YOArtistErrorCode.UNAUTHORIZED_USER)){
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.UNAUTHORIZED);
			}
			else{
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(Exception e){
			return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="summary/{id}", method=RequestMethod.DELETE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	@ResponseBody
	public ResponseEntity<ResponseBean> deleteSummaryData(HttpServletRequest request, @PathVariable("id") long id, HttpServletResponse response){
		try{
			return new ResponseEntity<ResponseBean>(ajaxView.deleteSummaryData(id, request), HttpStatus.OK);
		}
		catch(YOAException yo){
			System.out.println(yo);
			if(yo.getErrCode()!=null && yo.getErrCode().equals(YOArtistErrorCode.UNAUTHORIZED_USER)){
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.UNAUTHORIZED);
			}
			else{
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(Exception e){
			return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="updateSummaryDetails", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ResponseBean> updateEmploymentData(HttpServletRequest request, SummaryBean summary, HttpServletResponse response){
		try{
			return new ResponseEntity<ResponseBean>(ajaxView.updateSummaryDetails(summary, request), HttpStatus.OK);
		}
		catch(YOAException yo){
			if(yo.getErrCode()!=null && yo.getErrCode().equals(YOArtistErrorCode.UNAUTHORIZED_USER)){
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.UNAUTHORIZED);
			}
			else{
				return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		catch(Exception e){
			return new ResponseEntity<ResponseBean>(new ResponseBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
