package com.skypc.yoartist.factory;

import com.skypc.yoartist.constants.YOAConstants;
import com.skypc.yoartist.helper.MailHelper;

public class BeanFacotry {
	public Object getFactoryObject(String requestStr){
		Object obj=null;
		if(requestStr.equals(YOAConstants.BEAN_MAIL_HELPER)){
			MailHelper mailHelper = new MailHelper();
			obj=mailHelper;
		}
		return obj;
	}
}
