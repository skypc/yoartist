/*
 *
 */
package com.skypc.yoartist.dao;

import java.io.Serializable;
import java.util.List;

import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.util.KeyValue;

// TODO: Auto-generated Javadoc
/**
 * The Interface YOADao.
 * @author musaddique
 *
 * @param <S> the generic type
 */
public interface YOADao<S> {

	public S getById(Class cls, Serializable id) throws YOAException;

	public S getByConditions(Class cls, List<KeyValue> conditions) throws YOAException;

	public List<S> getObjectList(Class cls,List<KeyValue> conditions,List<KeyValue> orderByColumnNames,Integer startLimit,Integer offset) throws YOAException;

	public Long save(S s) throws YOAException;

	public void save(List<S> objects) throws YOAException;

	public void saveOrUpdate(S s) throws YOAException;

	
	public S merge(S s) throws YOAException;

	public void delete(S s) throws YOAException;

	public void deleteById(Class cls, Serializable id) throws YOAException;

	public void deleteListOfObjects(List<S> objects) throws YOAException;

	public void deleteByConditions(Class cls, List<KeyValue> conditions) throws YOAException;

	public List<Object[]> executeSqlQuery(String sQuery, List<KeyValue> conditions) throws YOAException;

	public List<String> executeSqlQueryForString(String sQuery, List<KeyValue> conditions) throws YOAException;

	public List<Object> executeHqlQuery(String sQuery, List<KeyValue> conditions) throws YOAException;

	public List<Object> executeHqlQueryDistinctRootEntities(String sQuery, List<KeyValue> conditions)
			throws YOAException;

	public int deleteByHqlConditions(String hqlQuery, List<KeyValue> conditions)throws YOAException;

	public Object getSingleColumnBySqlQuery(String sQuery, List<KeyValue> conditions)throws YOAException;

	public int updateBySqlQuery(String dbQuery, List<KeyValue> conditions1) throws YOAException;

	public List<Object> getObjectListByHQLQuery(String getBlockedDomainList, List<KeyValue> conditions) throws YOAException;

	public List<Object[]> getColumnByHqlQuery(String hqlQuery, List<KeyValue> conditions) throws YOAException;
	
	public int updateByHQLQuery(String query, List<KeyValue> conditions)throws YOAException;
	
	public List<Object> executeHqlQueryWithLimit(String sQuery, List<KeyValue> conditions, int startIndex, int offset,
			boolean getDistinctRootEntities) throws YOAException;
	public S getByDifferentConditions(Class cls, List<KeyValue> conditions) throws YOAException;

	public Object getSingleColumnByHqlQuery(String sQuery, List<KeyValue> conditions) throws YOAException;

	public void saveSharedPost(Long postId, List<Long> connectionIds, Long userId) throws YOAException;
	


}
