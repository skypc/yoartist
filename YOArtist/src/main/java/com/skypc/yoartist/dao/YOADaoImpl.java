/*
 *
 */
package com.skypc.yoartist.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.DistinctRootEntityResultTransformer;

import com.skypc.yoartist.constants.YOAConstants;
import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.util.KeyValue;
import com.skypc.yoartist.vo.SharedPost;

/**
 * The Class INVDAOImpl.
 *
 * @author musaddique
 *
 * @param <S>
 *            the generic type
 */
public class YOADaoImpl<S> implements YOADao<S> {

	/** The session factory. */
	private SessionFactory sessionFactory;

	public YOADaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public S getById(Class cls, Serializable id) throws YOAException {
		Session session = null;
		S s = null;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			s = (S) session.get(cls, id);
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
		} catch (HibernateException he) {
			throw new YOAException(he.getMessage());
		}

		finally {
			if (session != null) {
				session.close();
			}
		}
		return s;
	}

	@Override
	public S getByConditions(Class cls, List<KeyValue> conditions) throws YOAException {
		Session session = null;
		S s = null;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			Criteria criteria = session.createCriteria(cls);
			if (conditions != null) {
				for (KeyValue keyValue : conditions) {
					criteria.add(Restrictions.eq(keyValue.getKey(), keyValue.getValue()));
				}
			}
			s = (S) criteria.uniqueResult();
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
		} catch (HibernateException he) {
			throw new YOAException(he.getMessage());
		}

		finally {
			if (session != null) {
				session.close();
			}
		}
		return s;
	}

	@Override
	public List<S> getObjectList(Class cls, List<KeyValue> conditions, List<KeyValue> orderByColumnNames,
			Integer startLimit, Integer offset) throws YOAException {
		Session session = null;
		List<S> sList = null;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			Criteria where = session.createCriteria(cls);
			if (conditions != null) {
				for (KeyValue condition : conditions) {
					where.add(Restrictions.eq(condition.getKey(), condition.getValue()));
				}
			}
			if (orderByColumnNames != null && !orderByColumnNames.isEmpty()) {
				for (KeyValue order : orderByColumnNames) {
					if (order.getValue().toString().equalsIgnoreCase(YOAConstants.ORDER_BY_DESC))
						where.addOrder(Order.desc(order.getKey()));
					else
						where.addOrder(Order.asc(order.getKey()));
				}
			}
			if (startLimit != null) {
				where.setFirstResult(startLimit);
			}
			if (offset != null) {
				where.setMaxResults(offset);
			}
			sList = where.list();

			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
		} catch (HibernateException he) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			throw new YOAException(he.getMessage());
		}

		finally {
			if (session != null) {
				session.close();
			}
		}
		return sList;
	}

	@Override
	public Long save(S s) throws YOAException {
		Session session = null;
		Long id = 0l;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			id = (Long) session.save(s);
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
		} catch (HibernateException he) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			throw new YOAException(he.getMessage());
		}

		finally {
			if (session != null) {
				session.close();
			}
		}
		return id;
	}

	@Override
	public void save(List<S> objects) throws YOAException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			for (int i = 0; i < objects.size(); i++) {
				S s = objects.get(i);
				session.saveOrUpdate(s);
				if (i % YOAConstants.BATCH_SIZE == 0) {
					session.flush();
					session.clear();
				}
			}

			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
		} catch (HibernateException he) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			throw new YOAException(he.getMessage());
		}

		finally {
			if (session != null) {
				session.close();
			}
		}

	}

	@Override
	public void saveOrUpdate(S s) throws YOAException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.saveOrUpdate((s));
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
		} catch (HibernateException he) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			throw new YOAException(he.getMessage());
		}

		finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public S merge(S s) throws YOAException {
		Session session = null;
		S s1 = null;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			s1 = (S) session.merge(s);
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
		} catch (HibernateException he) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			throw new YOAException(he.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return s1;
	}

	@Override
	public void delete(S s) throws YOAException {
		// TODO Auto-generated method stub
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.delete(s);

			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
		} catch (HibernateException he) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			throw new YOAException(he.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteById(Class cls, Serializable id) throws YOAException {
		// TODO Auto-generated method stub
		Session session = null;
		S s = null;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			s = (S) session.get(cls, id);
			if (s != null) {
				session.delete(s);
			}

			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
		} catch (HibernateException he) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			throw new YOAException(he.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void deleteListOfObjects(List<S> objects) throws YOAException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteByConditions(Class cls, List<KeyValue> conditions) throws YOAException {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> executeSqlQuery(String sQuery, List<KeyValue> conditions) throws YOAException {
		Session session = null;
		List<Object[]> list = null;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			SQLQuery query = session.createSQLQuery(sQuery);
			if (conditions != null) {
				for (KeyValue kv : conditions) {
					if (kv.getKey().startsWith("sql_in_clause_str")) {
						query.setParameterList(kv.getKey(), (List<String>) kv.getValue());
					}
					if (kv.getKey().startsWith("sql_in_clause_int")) {
						query.setParameterList(kv.getKey(), (List<Integer>) kv.getValue());
					} else if (kv.getKey().startsWith("limit_first_index")) {
						query.setFirstResult((Integer) kv.getValue());
					} else if (kv.getKey().equalsIgnoreCase("limit_max_offset")) {
						query.setMaxResults((Integer) kv.getValue());
					} else {
						query.setParameter(kv.getKey(), kv.getValue());
					}
				}
			}

			list = query.list();
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
			if (list == null) {
				list = null;
			}
			return list;
		} catch (HibernateException he) {
			throw new YOAException(he.getMessage());
		}

		finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public List<Object> executeHqlQuery(String sQuery, List<KeyValue> conditions) throws YOAException {
		return executeHqlQuery(sQuery, conditions, false);
	}

	@Override
	public List<Object> executeHqlQueryWithLimit(String sQuery, List<KeyValue> conditions, int startIndex, int offset,
			boolean getDistinctRootEntities) throws YOAException {
		Session session = null;
		List<Object> list = null;

		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query query = session.createQuery(sQuery);
			if (conditions != null) {
				for (KeyValue kv : conditions) {
					query.setParameter(kv.getKey(), kv.getValue());
				}
			}
			if (getDistinctRootEntities) {
				query.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);
			}
			if (offset > 0) {
				query.setFirstResult(startIndex);
				query.setMaxResults(offset);
			}
			list = query.list();
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
			if (list == null || list.isEmpty()) {
				list = null;
			}
			return list;
		} catch (HibernateException he) {
			throw new YOAException(he.getMessage());
		}

		finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public List<Object> executeHqlQueryDistinctRootEntities(String sQuery, List<KeyValue> conditions)
			throws YOAException {
		return executeHqlQuery(sQuery, conditions, true);
	}

	private List<Object> executeHqlQuery(String sQuery, List<KeyValue> conditions, boolean getDistinctRootEntities)
			throws YOAException {
		Session session = null;
		List<Object> list = null;

		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query query = session.createQuery(sQuery);
			if (conditions != null) {
				for (KeyValue kv : conditions) {
					if (kv.getValue() instanceof Collection) {
						query.setParameterList(kv.getKey(), (Collection) kv.getValue());
					} else {
						query.setParameter(kv.getKey(), kv.getValue());
					}
				}
			}
			if (getDistinctRootEntities) {
				query.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);
			}
			list = query.list();
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
			if (list == null || list.isEmpty()) {
				list = null;
			}
			return list;
		} catch (HibernateException he) {
			throw new YOAException(he.getMessage());
		}

		finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public int deleteByHqlConditions(String hqlQuery, List<KeyValue> conditions) throws YOAException {
		Session session = null;
		int result = 0;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query query = session.createQuery(hqlQuery);
			if (conditions != null) {
				for (KeyValue kv : conditions) {
					query.setParameter(kv.getKey(), kv.getValue());
				}
			}

			result = query.executeUpdate();
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}

		} catch (HibernateException he) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			throw new YOAException(he.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return result;
	}

	@Override
	public Object getSingleColumnBySqlQuery(String sQuery, List<KeyValue> conditions) throws YOAException {
		Session session = null;
		Object obj = null;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			SQLQuery query = session.createSQLQuery(sQuery);
			if (conditions != null) {
				for (KeyValue kv : conditions) {
					query.setParameter(kv.getKey(), kv.getValue());
				}
			}
			obj = query.uniqueResult();

			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}

		} catch (HibernateException he) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			throw new YOAException(he.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return obj;
	}

	@Override
	public int updateBySqlQuery(String dbQuery, List<KeyValue> conditions) throws YOAException {
		Session session = null;
		int result = 0;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query query = session.createSQLQuery(dbQuery);
			if (conditions != null) {
				for (KeyValue kv : conditions) {
					query.setParameter(kv.getKey(), kv.getValue());
				}
			}

			result = query.executeUpdate();
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}

		} catch (HibernateException he) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			throw new YOAException(he.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return result;

	}

	@Override
	public List<String> executeSqlQueryForString(String sQuery, List<KeyValue> conditions) throws YOAException {
		Session session = null;
		List<String> list = null;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			SQLQuery query = session.createSQLQuery(sQuery);
			if (conditions != null) {
				for (KeyValue kv : conditions) {
					query.setParameter(kv.getKey(), kv.getValue());
				}
			}
			list = query.list();
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
			if (list == null) {
				list = null;
			}
			return list;
		} catch (HibernateException he) {
			throw new YOAException(he.getMessage());
		}

		finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public List<Object> getObjectListByHQLQuery(String hqlQuery, List<KeyValue> conditions) throws YOAException {
		List<Object> objects = new ArrayList<Object>();
		objects = executeHqlQuery(hqlQuery, conditions);
		return objects;
	}

	@Override
	public List<Object[]> getColumnByHqlQuery(String hqlQuery, List<KeyValue> conditions) throws YOAException {
		Session session = null;
		List<Object[]> list = null;

		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query query = session.createQuery(hqlQuery);
			if (conditions != null) {
				for (KeyValue kv : conditions) {
					if (kv.getKey().startsWith("in_clause_long")) {
						query.setParameterList(kv.getKey(), (List<Long>) kv.getValue());
					} else if (kv.getKey().startsWith("in_clause_str")) {
						query.setParameterList(kv.getKey(), (List<String>) kv.getValue());
					} else {
						query.setParameter(kv.getKey(), kv.getValue());
					}
				}
			}
			list = query.list();
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
			if (list == null || list.isEmpty()) {
				list = null;
			}
			return list;
		} catch (HibernateException he) {
			throw new YOAException(he.getMessage());
		}

		finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public int updateByHQLQuery(String dbQuery, List<KeyValue> conditions) throws YOAException {
		Session session = null;
		int result = 0;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query query = session.createQuery(dbQuery);
			if (conditions != null) {
				for (KeyValue kv : conditions) {
					if (kv.getKey().startsWith("in_clause_long")) {
						query.setParameterList(kv.getKey(), (List<Long>) kv.getValue());
					} else {
						query.setParameter(kv.getKey(), kv.getValue());
					}
				}
			}

			result = query.executeUpdate();
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}

		} catch (HibernateException he) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			throw new YOAException(he.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return result;

	}

	@Override
	public S getByDifferentConditions(Class cls, List<KeyValue> conditions) throws YOAException {
		Session session = null;
		S s = null;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			Criteria criteria = session.createCriteria(cls);
			if (conditions != null) {
				for (KeyValue keyValue : conditions) {
					String key = null;
					if (keyValue.getKey().startsWith("eq_")) {
						key = keyValue.getKey().replace("eq_", "");
						criteria.add(Restrictions.eq(key, keyValue.getValue()));
					} else if (keyValue.getKey().startsWith("ne_")) {
						key = keyValue.getKey().replace("ne_", "");
						criteria.add(Restrictions.ne(key, keyValue.getValue()));
					} else if (keyValue.getKey().startsWith("gt_")) {
						key = keyValue.getKey().replace("gt_", "");
						criteria.add(Restrictions.gt(key, keyValue.getValue()));
					} else if (keyValue.getKey().startsWith("lt_")) {
						key = keyValue.getKey().replace("lt_", "");
						criteria.add(Restrictions.lt(key, keyValue.getValue()));
					} else if (keyValue.getKey().startsWith("like_")) {
						key = keyValue.getKey().replace("like_", "");
						criteria.add(Restrictions.like(key, keyValue.getValue()));
					} else if (keyValue.getKey().startsWith("between_")) {
						key = keyValue.getKey().replace("between_", "");
						String value[] = String.valueOf(keyValue.getValue()).split(",");
						criteria.add(Restrictions.between(key, value[0], value[1]));
					} else if (keyValue.getKey().startsWith("isNull_")) {
						key = keyValue.getKey().replace("isNull_", "");
						criteria.add(Restrictions.isNull(key));
					} else if (keyValue.getKey().startsWith("isNotNull_")) {
						key = keyValue.getKey().replace("isNotNull_", "");
						criteria.add(Restrictions.isNotNull(key));
					} else if (keyValue.getKey().startsWith("isEmpty_")) {
						key = keyValue.getKey().replace("isEmpty_", "");
						criteria.add(Restrictions.isEmpty(key));
					} else if (keyValue.getKey().startsWith("isNotEmpty_")) {
						key = keyValue.getKey().replace("isNotEmpty_", "");
						criteria.add(Restrictions.isNotEmpty(key));
					}
				}
			}
			s = (S) criteria.uniqueResult();
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
		} catch (HibernateException he) {
			throw new YOAException(he.getMessage());
		}

		finally {
			if (session != null) {
				session.close();
			}
		}
		return s;
	}

	@Override
	public Object getSingleColumnByHqlQuery(String sQuery, List<KeyValue> conditions) throws YOAException {
		Session session = null;
		Object obj = null;
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query query = session.createQuery(sQuery);
			if (conditions != null) {
				for (KeyValue kv : conditions) {
					query.setParameter(kv.getKey(), kv.getValue());
				}
			}
			obj = query.uniqueResult();

			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}

		} catch (HibernateException he) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			throw new YOAException(he.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return obj;
	}

	@Override
	public void saveSharedPost(Long postId, List<Long> connectionIds, Long userId) throws YOAException {
		Session session = null;
		
		try {
			session = sessionFactory.openSession();
			session.getTransaction().begin();
			
			int count = 0;
			for(Long id : connectionIds){
				count++;
				SharedPost sharedPost = new SharedPost();
				sharedPost.setModifiedDate(new Date());
				sharedPost.setCreatedDate(new Date());
				sharedPost.setPostId(postId);
				sharedPost.setSharedBy(userId);
				sharedPost.setSharedTo(id);
				session.save(sharedPost);
				if(count%50==0){
					session.flush();
				}
				
			}
			
			if (!session.getTransaction().wasCommitted()) {
				session.getTransaction().commit();
			}
		} catch (HibernateException he) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			throw new YOAException(he.getMessage());
		}

		finally {
			if (session != null) {
				session.close();
			}
		}
		
	}

}
