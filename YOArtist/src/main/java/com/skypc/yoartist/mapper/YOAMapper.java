/*
 *
 */
package com.skypc.yoartist.mapper;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.skypc.yoartist.bean.CurrentLoginUser;
import com.skypc.yoartist.bean.EducationBean;
import com.skypc.yoartist.bean.MessageUserBean;
import com.skypc.yoartist.bean.MessageUserChatsBean;
import com.skypc.yoartist.bean.NotificationBean;
import com.skypc.yoartist.bean.PostCommentsBean;
import com.skypc.yoartist.bean.PostsBean;
import com.skypc.yoartist.bean.UserBean;
import com.skypc.yoartist.bean.UserToConnectBean;
import com.skypc.yoartist.enums.EnumConstants.NotificationType;
import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.helper.NotificationHelper;
import com.skypc.yoartist.util.DateUtil;
import com.skypc.yoartist.util.Util;
import com.skypc.yoartist.vo.Connection;
import com.skypc.yoartist.vo.EducationalDetailsVO;
import com.skypc.yoartist.vo.IGenericVO;
import com.skypc.yoartist.vo.MessagesVO;
import com.skypc.yoartist.vo.NotificationVO;
import com.skypc.yoartist.vo.User;

public class YOAMapper {

	public <T extends Object> T mapBeanWithResultSetArray(Class<T> className, List<Object[]> resultSet) {
		Field[] fields = className.getFields();
		try {
			T instance = className.newInstance();

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<PostsBean> mapResultSetWithPostBean(List<Object[]> resultSet) throws YOAException {
		List<PostsBean> beans = new ArrayList<>();
		resultSet.forEach(o -> {
			PostsBean bean = new PostsBean();
			bean.setUserId(o[0] == null ? "" : String.valueOf(o[0]));
			if (o[1] != null) {
				String imgStr = null;
				try {
					imgStr = Util.getEncodedImage((byte[]) o[1]);
				} catch (Exception e) {

				}
				if (imgStr != null)
					bean.setUserImage(imgStr);
			}
			bean.setPostId(o[2] == null ? "" : String.valueOf(o[2]));
			bean.setPostDesc(o[3] == null ? "" : String.valueOf(o[3]));
			bean.setFileName(o[4] == null ? "" : String.valueOf(o[4]));

			String filePath = o[5] == null ? "" : String.valueOf(o[5]);
			if (filePath != null && !filePath.isEmpty()) {
				filePath = filePath.substring(filePath.indexOf("/userPostFile"), filePath.length());
			}
			bean.setFilePath(filePath);
			Date mdate = (Date) o[6];

			bean.setTime(DateUtil.getDateASTimeAgo(mdate));
			String uName = o[7] == null ? "" : String.valueOf(o[7]);
			
			//bean.setLikeCount(likeCount);
			bean.setUserName(uName);
			beans.add(bean);
		});
		return beans;
	}

	public List<UserToConnectBean> mapResultSetWithConnectBean(List<Object[]> resultSet) throws YOAException {
		List<UserToConnectBean> beans = new ArrayList<>();
		resultSet.forEach(o -> {
			UserToConnectBean bean = new UserToConnectBean();
			bean.setUserId(o[0] == null ? "" : String.valueOf(o[0]));
			if (o[1] != null) {
				String imgStr = null;
				try {
					imgStr = Util.getEncodedImage((byte[]) o[1]);
				} catch (Exception e) {

				}
				if (imgStr != null) {
					bean.setUserImage(imgStr);
					bean.setImage(true);
				}
			}
			String fName = o[2] == null ? "" : String.valueOf(o[2]);
			String lName = o[3] == null ? "" : String.valueOf(o[3]);
			String fullName = "";
			if (fName != null && !fName.isEmpty()) {
				fullName += fName;
			}
			if (lName != null && !lName.isEmpty()) {
				fullName += " " + lName;
			}
			bean.setUserName(fullName);
			beans.add(bean);

		});
		return beans;
	}

	public List<PostCommentsBean> mapResultSetWithCommentBean(List<Object[]> resultSet) throws YOAException {
		List<PostCommentsBean> beans = new ArrayList<>();
		resultSet.forEach(o -> {
			PostCommentsBean bean = new PostCommentsBean();
			bean.setUserId(o[0] == null ? "" : String.valueOf(o[0]));
			if (o[1] != null) {
				String imgStr = null;
				try {
					imgStr = Util.getEncodedImage((byte[]) o[1]);
				} catch (Exception e) {

				}
				if (imgStr != null)
					bean.setUserImage(imgStr);
			}
			String fName = o[2] == null ? "" : String.valueOf(o[2]);
			String lName = o[3] == null ? "" : String.valueOf(o[3]);
			String fullName = "";
			if (fName != null && !fName.isEmpty()) {
				fullName += fName;
			}
			if (lName != null && !lName.isEmpty()) {
				fullName += " " + lName;
			}
			bean.setUserName(fullName);
			bean.setCommentId(o[4] == null ? "" : String.valueOf(o[4]));
			bean.setComment(o[5] == null ? "" : String.valueOf(o[5]));
			Date mdate = (Date) o[6];

			bean.setCommentDate(DateUtil.getDateASTimeAgo(mdate));
			beans.add(bean);
		});
		return beans;
	}

	public List<NotificationBean> mapResultSetWithNotificatioinVo(List<IGenericVO> objList) {
		List<NotificationBean> beans = new ArrayList<>();
		objList.forEach(o -> {
			NotificationVO nv = (NotificationVO) o;
			NotificationBean bean = new NotificationBean();
			bean.setUserId(nv.getFromUser().getId());
			bean.setFullName(nv.getFromUser().getFullName());
			if (nv.getFromUser().getProfilePhoto() != null) {
				String imgStr = null;
				try {
					imgStr = Util.getEncodedImage(nv.getFromUser().getProfilePhoto());
				} catch (Exception e) {

				}
				if (imgStr != null)
					bean.setUserImage(imgStr);
			}
			bean.setLink(NotificationHelper.getNotificationLinkByType(nv.getType(), nv.getFromUser().getId()));
			bean.setDescription(nv.getDescription());
			bean.setTime(DateUtil.getDateASTimeAgo(nv.getModifiedDateTime()));
			beans.add(bean);
		});
		return beans;
	}

	public List<NotificationBean> mapResultSetWithConnection(List<Object[]> resultSet) {
		List<NotificationBean> beans = new ArrayList<>();
		resultSet.forEach(o -> {
			NotificationBean bean = new NotificationBean();
			bean.setUserId(o[0] == null ? 0 : (Long) (o[0]));
			String fName = o[1] == null ? "" : String.valueOf(o[1]);
			String lName = o[2] == null ? "" : String.valueOf(o[2]);
			String fullName = "";
			if (fName != null && !fName.isEmpty()) {
				fullName += fName;
			}
			if (lName != null && !lName.isEmpty()) {
				fullName += " " + lName;
			}
			bean.setFullName(fullName);
			if (o[3] != null) {
				String imgStr = null;
				try {
					imgStr = Util.getEncodedImage((byte[]) o[3]);
				} catch (Exception e) {

				}
				if (imgStr != null)
					bean.setUserImage(imgStr);
			}
			Date mdate = o[4] == null ? null : (Date) o[4];
			if (mdate != null)
				bean.setTime(DateUtil.getDateASTimeAgo(mdate));
			bean.setLink(
					NotificationHelper.getNotificationLinkByType(NotificationType.CONNECT_REQUEST, bean.getUserId()));
			bean.setDescription(NotificationHelper.getNotificationDescByType(NotificationType.CONNECT_REQUEST));
			beans.add(bean);
		});
		return beans;
	}

	public List<MessageUserBean> mapResultSetWithMsgUserBean(List<Object[]> resultSet) {
		List<MessageUserBean> beans = new ArrayList<>();
		resultSet.forEach(o -> {
			MessageUserBean bean = new MessageUserBean();
			bean.setUserId(o[0] == null ? 0 : (Long) (o[0]));
			String fName = o[1] == null ? "" : String.valueOf(o[1]);
			String lName = o[2] == null ? "" : String.valueOf(o[2]);
			String fullName = "";
			if (fName != null && !fName.isEmpty()) {
				fullName += fName;
			}
			if (lName != null && !lName.isEmpty()) {
				fullName += " " + lName;
			}
			bean.setFullName(fullName);
			if (o[3] != null) {
				String imgStr = null;
				try {
					imgStr = Util.getEncodedImage((byte[]) o[3]);
				} catch (Exception e) {

				}
				if (imgStr != null)
					bean.setUserImage(imgStr);
			}
			Date mdate = o[4] == null ? null : (Date) o[4];
			if (mdate != null)
				bean.setTime(DateUtil.getDateASTimeAgo(mdate));
			beans.add(bean);
		});
		return beans;
	}

	public List<MessageUserChatsBean> mapResultSetWithMessageVO(List<Object> objList, long sessionUserId) {
		List<MessageUserChatsBean> beans = new ArrayList<>();
		objList.forEach(o -> {
			MessagesVO m = (MessagesVO) o;
			MessageUserChatsBean bean = new MessageUserChatsBean();
			bean.setUserId(m.getSourceUser().getId());
			bean.setFullName(m.getSourceUser().getFullName());
			if (m.getSourceUser().getProfilePhoto() != null) {
				String imgStr = null;
				try {
					imgStr = Util.getEncodedImage(m.getSourceUser().getProfilePhoto());
				} catch (Exception e) {

				}
				if (imgStr != null)
					bean.setUserImage(imgStr);
			}
			bean.setMessage(m.getMessage());
			bean.setTime(DateUtil.getDateASTimeAgo(m.getModifiedDateTime()));
			if (bean.getUserId() == sessionUserId) {
				bean.setLoginUser(true);
			}
			beans.add(bean);
		});
		return beans;
	}

	public CurrentLoginUser mapResultSetWithCurrentLoginUser(List<Object[]> resultSet) {
		CurrentLoginUser bean = new CurrentLoginUser();
		resultSet.forEach(o -> {
			bean.setUserId(o[0] == null ? 0 : (Long) (o[0]));
			String fName = o[1] == null ? "" : String.valueOf(o[1]);
			String lName = o[2] == null ? "" : String.valueOf(o[2]);
			String fullName = "";
			if (fName != null && !fName.isEmpty()) {
				fullName += fName;
			}
			if (lName != null && !lName.isEmpty()) {
				fullName += " " + lName;
			}
			bean.setFullName(fullName);
			if (o[3] != null) {
				String imgStr = null;
				try {
					imgStr = Util.getEncodedImage((byte[]) o[3]);
				} catch (Exception e) {

				}
				if (imgStr != null)
					bean.setUserImage(imgStr);
			}
			
		});
		return bean;
	}

	public List<UserToConnectBean> mapUsersWithConnectBean(List<User> connectedUsers) throws YOAException {
		List<UserToConnectBean> usersBean = new ArrayList<UserToConnectBean>();
		
		if(connectedUsers!=null && !connectedUsers.isEmpty()){
			for(User user : connectedUsers){
				UserToConnectBean userToConnectBean = new UserToConnectBean();
				if(user!=null){
					userToConnectBean.setUserId(String.valueOf(user.getId()));
					if(user.getFullName()!=null){
						userToConnectBean.setUserName(user.getFirstName());
					}
					if(user.getProfilePhoto()!=null){
						userToConnectBean.setUserImage(Util.getEncodedImage(user.getProfilePhoto()));
					}
				}
				usersBean.add(userToConnectBean);
			}
			
			
		}
		
		return usersBean;
		
	}
	
	public UserBean mapUserToUserProfileBean(User user){
		UserBean userBean = new UserBean();
		userBean.setId(user.getId());
		userBean.setFirstName(user.getFirstName());
		userBean.setLastName(user.getLastName());
		userBean.setEmail(user.getEmail());
		if(user.getBaseUser()!=null && user.getBaseUser().getAddress()!=null){
			userBean.setAddress(user.getBaseUser().getAddress().getFullAddress());
		}
		return userBean;
		
	}
	
	public void mapEducationBeanToVO(EducationalDetailsVO edu, EducationBean bean){
		edu.setUniversity(bean.getSchoolName());
		edu.setCountry(bean.getCountry());
		edu.setCreatedDate(new Date());
		edu.setDegree(bean.getDegree());
		edu.setStartYear(bean.getFromYear());
		edu.setEndYear(bean.getToYear());
		edu.setFieldOfStudy(bean.getFieldOfStudy());
		edu.setGrade(bean.getGrade());
		edu.setModifiedDate(new Date());
	}
}
