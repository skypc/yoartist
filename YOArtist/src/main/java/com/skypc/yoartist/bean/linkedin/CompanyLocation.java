package com.skypc.yoartist.bean.linkedin;

public class CompanyLocation {
	private String name;
    private CompanyCountry country;

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public CompanyCountry getCountry ()
    {
        return country;
    }

    public void setCountry (CompanyCountry country)
    {
        this.country = country;
    }
}
