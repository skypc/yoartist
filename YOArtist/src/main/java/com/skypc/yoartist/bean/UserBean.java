package com.skypc.yoartist.bean;

import java.util.List;
import java.util.Map;

public class UserBean {
	private long id;
	private String firstName;
	private String lastName;
	private String Address;
	private String email;
	private String mobileNumber;
	private String summery;
	private Map<String,String> socialContacts=null;
	private List<String> awards;
	private List<String> recognitions;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getSummery() {
		return summery;
	}
	public void setSummery(String summery) {
		this.summery = summery;
	}
	public Map<String, String> getSocialContacts() {
		return socialContacts;
	}
	public void setSocialContacts(Map<String, String> socialContacts) {
		this.socialContacts = socialContacts;
	}
	public List<String> getAwards() {
		return awards;
	}
	public void setAwards(List<String> awards) {
		this.awards = awards;
	}
	public List<String> getRecognitions() {
		return recognitions;
	}
	public void setRecognitions(List<String> recognitions) {
		this.recognitions = recognitions;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	
	
}
