package com.skypc.yoartist.bean;

public class UserProfileLeftBean {
	private long id;
	private String fullName;
	private long following;
	private long followers;
	private long posts;
	private String image;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public long getFollowing() {
		return following;
	}
	public void setFollowing(long following) {
		this.following = following;
	}
	public long getFollowers() {
		return followers;
	}
	public void setFollowers(long followers) {
		this.followers = followers;
	}
	public long getPosts() {
		return posts;
	}
	public void setPosts(long posts) {
		this.posts = posts;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	
}
