package com.skypc.yoartist.bean.linkedin;

public class Positions {
	private Values[] values;

    private String _total;

    public Values[] getValues ()
    {
        return values;
    }

    public void setValues (Values[] values)
    {
        this.values = values;
    }

    public String get_total ()
    {
        return _total;
    }

    public void set_total (String _total)
    {
        this._total = _total;
    }

    /*@Override
    public String toString()
    {
        return "ClassPojo [values = "+values+", _total = "+_total+"]";
    }*/
}
