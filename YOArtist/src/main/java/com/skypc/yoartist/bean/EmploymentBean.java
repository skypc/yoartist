package com.skypc.yoartist.bean;

public class EmploymentBean {

	private long id;

	private String jobPosition;

	private String orgName;

	private String startDate;

	private String endDate;

	private boolean isCurrentlyWorking;

	private String state;

	private String country;

	private String employmentDuration;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getJobPosition() {
		return jobPosition;
	}

	public void setJobPosition(String jobPosition) {
		this.jobPosition = jobPosition;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public boolean isCurrentlyWorking() {
		return isCurrentlyWorking;
	}

	public void setIsCurrentlyWorking(boolean isCurrentlyWorking) {
		this.isCurrentlyWorking = isCurrentlyWorking;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmploymentDuration() {
		return employmentDuration;
	}

	public void setEmploymentDuration(String employmentDuration) {
		this.employmentDuration = employmentDuration;
	}

	
}
