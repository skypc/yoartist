package com.skypc.yoartist.bean.linkedin;

public class Location {
	private String name;
    private LocationCountry country;

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public LocationCountry getCountry ()
    {
        return country;
    }

    public void setCountry (LocationCountry country)
    {
        this.country = country;
    }
}
