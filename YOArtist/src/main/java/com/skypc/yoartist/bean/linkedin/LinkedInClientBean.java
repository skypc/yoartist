package com.skypc.yoartist.bean.linkedin;

public class LinkedInClientBean {
	private String id;
	private String pictureUrl;
	private String emailAddress;
	private String headline;
	private Positions positions;
	private String lastName;
	private Location location;
	private String industry;
	private String firstName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public Positions getPositions() {
		return positions;
	}

	public void setPositions(Positions positions) {
		this.positions = positions;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
	

	/*@Override
	public String toString() {
		return "ClassPojo [id = " + id + ", headline = " + headline + ", positions = " + positions + ", lastName = "
				+ lastName + ", location = " + location + ", industry = " + industry + ", firstName = " + firstName
				+ "]";
	}*/

}
