package com.skypc.yoartist.bean;

import java.util.Comparator;
import java.util.Date;

public class ConnectionActivity implements Comparator<ConnectionActivity>{
	private long userId1;
	private long userId2;
	private String connName1;
	private String connName2;
	private String userImage1;
	private String userImage2;
	private long postId;
	private String type;
	private Date date;
	private String time;
	private String desc;
	
	
	public long getUserId1() {
		return userId1;
	}
	public void setUserId1(long userId1) {
		this.userId1 = userId1;
	}
	public long getUserId2() {
		return userId2;
	}
	public void setUserId2(long userId2) {
		this.userId2 = userId2;
	}
	public String getConnName1() {
		return connName1;
	}
	public void setConnName1(String connName1) {
		this.connName1 = connName1;
	}
	public String getConnName2() {
		return connName2;
	}
	public void setConnName2(String connName2) {
		this.connName2 = connName2;
	}
	public String getUserImage1() {
		return userImage1;
	}
	public void setUserImage1(String userImage1) {
		this.userImage1 = userImage1;
	}
	public String getUserImage2() {
		return userImage2;
	}
	public void setUserImage2(String userImage2) {
		this.userImage2 = userImage2;
	}
	public long getPostId() {
		return postId;
	}
	public void setPostId(long postId) {
		this.postId = postId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	@Override
	public int compare(ConnectionActivity c1, ConnectionActivity c2) {
		return c2.date.compareTo(c1.date);
	}
	
	
	
}
