package com.skypc.yoartist.bean.linkedin;

public class Values {
	private String id;

    private StartDate startDate;
    
    private EndDate endDate;

    private String title;

    private CompanyLocation location;

    private Company company;

    private String isCurrent;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public StartDate getStartDate ()
    {
        return startDate;
    }

    public void setStartDate (StartDate startDate)
    {
        this.startDate = startDate;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public CompanyLocation getLocation ()
    {
        return location;
    }

    public void setLocation (CompanyLocation location)
    {
        this.location = location;
    }

    public Company getCompany ()
    {
        return company;
    }

    public void setCompany (Company company)
    {
        this.company = company;
    }
	
	public String getIsCurrent() {
		return isCurrent;
	}

	public void setIsCurrent(String isCurrent) {
		this.isCurrent = isCurrent;
	}

	public EndDate getEndDate() {
		return endDate;
	}

	public void setEndDate(EndDate endDate) {
		this.endDate = endDate;
	}

    

    /*@Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", startDate = "+startDate+", title = "+title+", location = "+location+", company = "+company+", isCurrent = "+isCurrent+"]";
    }*/
}
