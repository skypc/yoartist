package com.skypc.yoartist.bean;

public class MessageCodeBean {
	private String message;
	private int code;
	public MessageCodeBean(String message, int code) {
		super();
		this.message = message;
		this.code = code;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	
	
	

}
