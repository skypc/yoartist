package com.skypc.yoartist.handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;

import com.skypc.yoartist.bean.ConnectionActivity;
import com.skypc.yoartist.bean.CurrentLoginUser;
import com.skypc.yoartist.bean.EducationBean;
import com.skypc.yoartist.bean.EmploymentBean;
import com.skypc.yoartist.bean.MessageUserBean;
import com.skypc.yoartist.bean.MessageUserChatsBean;
import com.skypc.yoartist.bean.NotificationBean;
import com.skypc.yoartist.bean.ResponseBean;
import com.skypc.yoartist.bean.SummaryBean;
import com.skypc.yoartist.bean.UserBean;
import com.skypc.yoartist.bean.UserToConnectBean;
import com.skypc.yoartist.constants.YOAConstants;
import com.skypc.yoartist.constants.YOArtistErrorCode;
import com.skypc.yoartist.constants.YOArtistErrorMessages;
import com.skypc.yoartist.enums.EnumConstants.ConnectStatus;
import com.skypc.yoartist.enums.EnumConstants.NotificationType;
import com.skypc.yoartist.enums.FileSystemDir;
import com.skypc.yoartist.enums.UserPostStatus;
import com.skypc.yoartist.enums.UserStatus;
import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.helper.ActivityHelper;
import com.skypc.yoartist.helper.NotificationExecutor;
import com.skypc.yoartist.helper.NotificationHelper;
import com.skypc.yoartist.ihandler.IAjaxView;
import com.skypc.yoartist.logger.YOALogger;
import com.skypc.yoartist.mapper.YOAMapper;
import com.skypc.yoartist.query.DBQueries;
import com.skypc.yoartist.service.IService;
import com.skypc.yoartist.util.DateUtil;
import com.skypc.yoartist.util.KeyValue;
import com.skypc.yoartist.util.Util;
import com.skypc.yoartist.vo.Connection;
import com.skypc.yoartist.vo.EducationalDetailsVO;
import com.skypc.yoartist.vo.EmployementDetailsVO;
import com.skypc.yoartist.vo.IGenericVO;
import com.skypc.yoartist.vo.LikeVO;
import com.skypc.yoartist.vo.MessagesVO;
import com.skypc.yoartist.vo.NotificationVO;
import com.skypc.yoartist.vo.PostComments;
import com.skypc.yoartist.vo.SummaryVO;
import com.skypc.yoartist.vo.User;
import com.skypc.yoartist.vo.UserActivityVO;
import com.skypc.yoartist.vo.UserPosts;

public class AjaxViewHandler implements IAjaxView {

	private static final String AJAX_VIEW_HANDLER = "AJAX_VIEW_HANDLER";
	@Autowired
	private IService service;

	@Override
	public String uploadUserPost(HttpServletRequest request, MultipartFile multipartFile) throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null)
			if (multipartFile != null) {
				String postPath = FileSystemDir.USER_POST_FILES_PATH.getPath();
				String uploadFileAbsPath = Util.uploadFile(multipartFile, postPath);
				multipartFile.getOriginalFilename();
				String fileName = multipartFile.getOriginalFilename();
				Long userId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
				User user = (User) service.getById(User.class, userId);

				UserPosts userPosts = new UserPosts();
				userPosts.setFileName(fileName);
				userPosts.setFilePath(uploadFileAbsPath);
				userPosts.setStatus(UserPostStatus.DRAFT.name());
				userPosts.setUser(user);
				userPosts.setCreatedDate(new Date());
				userPosts.setModifiedDate(new Date());
				Long id = service.save(userPosts);
				return String.valueOf(id);
			}
		return "0";
	}

	@Override
	public List<UserToConnectBean> usersToConnect(HttpServletRequest request) throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			List<Long> usersIds = new ArrayList<>();
			Long userId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("userId", userId));
			List<Object> resultSet = service.executeHqlQuery(DBQueries.GET_USERS_CONNECTIONS, conditions);
			if (resultSet != null) {
				Iterator<Object> iterator = resultSet.iterator();
				while (iterator.hasNext()) {
					Object object = (Object) iterator.next();
					usersIds.add((Long) object);
				}
			}
			List<Object[]> usersResultSet = null;
			conditions.clear();
			conditions.add(new KeyValue("currentUserId", userId));
			if (!usersIds.isEmpty()) {
				conditions.add(new KeyValue("in_clause_long_connectionIds", usersIds));
				usersResultSet = service.getColumnByHqlQuery(
						DBQueries.GET_USERS_FOR_CONNECTIONS_BLOCK + DBQueries.GET_USERS_FOR_CONNECTIONS_BLOCK_IN_CLAUSE,
						conditions);
			} else
				usersResultSet = service.getColumnByHqlQuery(DBQueries.GET_USERS_FOR_CONNECTIONS_BLOCK, conditions);
			if (usersResultSet != null && !usersResultSet.isEmpty()) {
				YOAMapper mapper = new YOAMapper();
				List<UserToConnectBean> connectBeans = mapper.mapResultSetWithConnectBean(usersResultSet);
				return connectBeans;
			}
		}

		return null;
	}

	@Override
	public String commentOnPost(HttpServletRequest request, long postId, String comment) throws YOAException {

		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long userId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			User user = (User) service.getById(User.class, userId);
			UserPosts post = (UserPosts) service.getById(UserPosts.class, postId);
			PostComments c = new PostComments();
			c.setComments(comment);
			c.setPost(post);
			c.setUser(user);
			c.setStatus(UserPostStatus.POSTED.name());
			c.setCreatedDate(new Date());
			c.setModifiedDate(new Date());
			Long save = service.save(c);
			if (save > 0) {
				try {
					User sourceUser = new User();
					sourceUser.setId(userId);
					if (post.getUser().getId() != userId.longValue()) {
						User destUser = new User();
						destUser.setId(post.getUser().getId());
						saveNotification(sourceUser, destUser, NotificationType.POST_COMMENTS, postId);
					}
					saveUserActivity(user, null, postId, NotificationType.POST_COMMENTS);

				} catch (Exception e) {
					YOALogger.error(AJAX_VIEW_HANDLER, "commentOnPost", e.getMessage());
				}
				return "1";
			} else
				return "0";

		}

		return "0";
	}

	@Override
	public String sendConnectRequest(HttpServletRequest request, long userId) throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long sessionUserId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			Connection connection = new Connection();
			connection.setUserId(sessionUserId);
			connection.setConnectId(userId);
			connection.setStatus(ConnectStatus.PENDING.name());
			connection.setCreatedDate(new Date());
			connection.setModifiedDate(new Date());
			Long save = service.save(connection);
			if (save > 0) {
				User sourceUser = (User) service.getById(User.class, sessionUserId);
				User destUser = (User) service.getById(User.class, userId);
				NotificationVO notification = new NotificationVO();
				notification.setFromUser(sourceUser);
				notification.setToUser(destUser);
				notification
						.setDescription(NotificationHelper.getNotificationDescByType(NotificationType.CONNECT_REQUEST));
				notification.setType(NotificationType.CONNECT_REQUEST);
				notification.setRead(false);
				notification.setCreatedDateTime(new Date());
				notification.setModifiedDateTime(new Date());
				NotificationExecutor executor = NotificationExecutor.getInstance(service);
				executor.processNotification(notification);

				return "1";
			} else
				return "0";

		}

		return "0";
	}

	@Override
	public String sendMessageToUser(HttpServletRequest request, long userId, String message) throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long sessionUserId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			User sourceUser = (User) service.getById(User.class, sessionUserId);
			User destUser = (User) service.getById(User.class, userId);

			MessagesVO messageVO = new MessagesVO();
			messageVO.setSourceUser(sourceUser);
			messageVO.setDestinationUser(destUser);
			messageVO.setMessage(message);
			messageVO.setRead(false);
			messageVO.setCreatedDateTime(new Date());
			messageVO.setModifiedDateTime(new Date());
			Long save = service.save(messageVO);
			if (save > 0) {
				return "1";
			} else
				return "0";

		}

		return "0";
	}

	@Override
	public List<NotificationBean> getNotifications(HttpServletRequest request, boolean isLimited) throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long sessionUserId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("toUser.id", sessionUserId));
			List<KeyValue> orderByconditions = new ArrayList<>();
			orderByconditions.add(new KeyValue("modifiedDateTime", YOAConstants.ORDER_BY_DESC));

			List<IGenericVO> objectList = null;
			if (isLimited) {
				objectList = service.getObjectList(NotificationVO.class, conditions, orderByconditions, 0, 5);
				conditions.clear();
				conditions.add(new KeyValue("userId", sessionUserId));
				service.updateByHQLQuery(DBQueries.MARK_READ_NOTIFICATION, conditions);
			} else {
				objectList = service.getObjectList(NotificationVO.class, conditions, orderByconditions, null, null);
				conditions.clear();
				conditions.add(new KeyValue("userId", sessionUserId));
				service.updateByHQLQuery(DBQueries.MARK_READ_NOTIFICATION, conditions);
			}
			YOAMapper mapper = new YOAMapper();
			List<NotificationBean> beans = mapper.mapResultSetWithNotificatioinVo(objectList);
			return beans;
		}
		return null;
	}

	@Override
	public int getNotificationOrConnectionCount(HttpServletRequest request, boolean isNotification)
			throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long sessionUserId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("userId", sessionUserId));
			String query = null;
			Object countObj = null;
			if (isNotification) {
				query = DBQueries.GET_NOTIFICATION_COUNT;
			} else {
				query = DBQueries.GET_CONNECT_REQUEST_COUNT;
			}
			countObj = service.getSingleColumnByHqlQuery(query, conditions);
			if (countObj != null) {
				return ((Long) countObj).intValue();
			}
		}
		return 0;
	}

	@Override
	public String acceptConnectRequest(HttpServletRequest request, long userId, boolean isAccept) throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long sessionUserId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			if (isAccept) {
				List<KeyValue> conditions = new ArrayList<>();
				conditions.add(new KeyValue("userId", userId));
				conditions.add(new KeyValue("connectId", sessionUserId));
				conditions.add(new KeyValue("status", ConnectStatus.ACCEPTED.name()));
				int result = service.updateByHQLQuery(DBQueries.UPDATE_CONNECTION_WITH_STATUS, conditions);
				if (result > 0) {
					Connection connection = new Connection();
					connection.setUserId(sessionUserId);
					connection.setConnectId(userId);
					connection.setStatus(ConnectStatus.ACCEPTED.name());
					connection.setCreatedDate(new Date());
					connection.setModifiedDate(new Date());
					Long save = service.save(connection);
					if (save > 0) {
						User sourceUser = (User) service.getById(User.class, sessionUserId);
						User destUser = (User) service.getById(User.class, userId);
						try {
							saveNotification(sourceUser, destUser, NotificationType.ACCEPT_CONNECT, 0);
						} catch (Exception e) {
							YOALogger.error(AJAX_VIEW_HANDLER, "acceptConnectRequest", e.getMessage());
						}
						try {
							saveUserActivity(sourceUser, destUser, 0, NotificationType.ACCEPT_CONNECT);
						} catch (Exception e) {
							YOALogger.error(AJAX_VIEW_HANDLER, "acceptConnectRequest", e.getMessage());
						}
						return "1";
					}
				}
			} else {
				List<KeyValue> conditions = new ArrayList<>();
				conditions.add(new KeyValue("userId", userId));
				conditions.add(new KeyValue("connectId", sessionUserId));
				conditions.add(new KeyValue("status", ConnectStatus.DECLINE.name()));
				int result = service.updateByHQLQuery(DBQueries.UPDATE_CONNECTION_WITH_STATUS, conditions);
				if (result > 0) {
					return "2";
				}
			}
		}
		return "0";
	}

	/**
	 * @param currentUser
	 * @param secondUser
	 * @throws YOAException
	 */
	private void saveUserActivity(User currentUser, User secondUser, long postId, NotificationType type)
			throws YOAException {
		long secondUserId = 0;
		String seconduserName = "";
		UserActivityVO userActivity = new UserActivityVO();
		userActivity.setUserId(currentUser);
		if (secondUser != null) {
			userActivity.setSecondUserId(secondUser);
			secondUserId = secondUser.getId();
			seconduserName = secondUser.getFullName();
		}
		if (postId > 0) {
			UserPosts post = new UserPosts();
			userActivity.setPostId(post);
		}
		userActivity.setDescription(ActivityHelper.getNotificationDescByType(type, currentUser.getId(), secondUserId,
				currentUser.getFullName(), seconduserName, postId));
		userActivity.setCreatedDateTime(new Date());
		userActivity.setModifiedDateTime(new Date());
		userActivity.setType(type.name());
		service.save(userActivity);
	}

	/**
	 * @param sourceUser
	 * @param destUser
	 */
	private void saveNotification(User sourceUser, User destUser, NotificationType type, long postId)
			throws YOAException {
		NotificationVO notification = new NotificationVO();
		notification.setFromUser(sourceUser);
		notification.setToUser(destUser);
		String desc = NotificationHelper.getNotificationDescByType(type);
		if (desc.contains("#postId#")) {
			desc = desc.replaceAll("#postId#", String.valueOf(postId));
		}
		notification.setDescription(desc);
		notification.setType(type);
		notification.setRead(false);
		notification.setCreatedDateTime(new Date());
		notification.setModifiedDateTime(new Date());
		NotificationExecutor executor = NotificationExecutor.getInstance(service);
		executor.processNotification(notification);
	}

	@Override
	public List<NotificationBean> getAllConnectRequest(HttpServletRequest request) throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long sessionUserId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("sessionUserId", sessionUserId));

			List<Object[]> objectList = service.getColumnByHqlQuery(DBQueries.GET_ALL_PENDING_CONNECTIONS, conditions);
			if (objectList != null) {
				YOAMapper mapper = new YOAMapper();
				List<NotificationBean> beans = mapper.mapResultSetWithConnection(objectList);
				conditions.clear();
				conditions.add(new KeyValue("userId", sessionUserId));
				service.updateByHQLQuery(DBQueries.MARK_READ_CONNECTIONS, conditions);
				return beans;
			}
		}
		return null;
	}

	@Override
	public List<MessageUserBean> getMessageUsers(HttpServletRequest request) throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long sessionUserId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("sessionUserId", sessionUserId));

			List<Object[]> objectList = service.getColumnByHqlQuery(DBQueries.GET_ALL_ACCEPTED_CONNECTIONS, conditions);
			if (objectList != null) {
				YOAMapper mapper = new YOAMapper();
				List<MessageUserBean> beans = mapper.mapResultSetWithMsgUserBean(objectList);
				return beans;
			}
		}
		return null;
	}

	@Override
	public List<MessageUserChatsBean> getMessageUsersChat(HttpServletRequest request, long userId) throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long sessionUserId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("sessionId", sessionUserId));
			conditions.add(new KeyValue("userId", userId));
			List<Object> resultSet = service.executeHqlQuery(DBQueries.GET_USER_CONVERSATION, conditions);
			if (resultSet != null) {
				YOAMapper mapper = new YOAMapper();
				List<MessageUserChatsBean> beans = mapper.mapResultSetWithMessageVO(resultSet, sessionUserId);
				conditions.clear();
				conditions.add(new KeyValue("userId", sessionUserId));
				service.updateByHQLQuery(DBQueries.MARK_READ_MESSAGES, conditions);
				return beans;
			}

		}
		return null;
	}

	@Override
	public CurrentLoginUser getCurrentLoginUser(HttpServletRequest request) throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long sessionUserId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("sessionId", sessionUserId));
			List<Object[]> resultSet = service.getColumnByHqlQuery(DBQueries.GET_CURRENT_LOGIN_USER, conditions);
			if (resultSet != null) {
				YOAMapper mapper = new YOAMapper();
				CurrentLoginUser bean = mapper.mapResultSetWithCurrentLoginUser(resultSet);
				return bean;
			}

		}
		return null;
	}

	@Override
	public String likePost(HttpServletRequest request, long postId) throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long sessionUserId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("postId", postId));
			conditions.add(new KeyValue("userId", sessionUserId));
			Object id = service.getSingleColumnByHqlQuery(DBQueries.GET_IS_LIKES_FOR_POST, conditions);
			NotificationType type = null;
			if (id != null) {
				LikeVO like = new LikeVO();
				like.setId(Long.valueOf(String.valueOf(id)));
				service.delete(like);
				type = NotificationType.POST_UNLIKE;
			} else {
				UserPosts post = new UserPosts();
				post.setId(postId);
				User user = new User();
				user.setId(sessionUserId);
				LikeVO like = new LikeVO();
				like.setUser(user);
				like.setPost(post);
				like.setCreatedDateTime(new Date());
				like.setModifiedDateTime(new Date());
				service.save(like);
				type = NotificationType.POST_LIKE;
			}
			if (type != null) {
				try {
					User sourceUser = new User();
					sourceUser.setId(sessionUserId);
					conditions.clear();
					conditions.add(new KeyValue("postId", postId));
					Object postUserId = service.getSingleColumnByHqlQuery(DBQueries.GET_USER_ID_FROM_POST_ID,
							conditions);
					if (postUserId != null) {
						long userId = Long.valueOf(String.valueOf(postUserId));
						if (userId != sessionUserId.longValue()) {
							User destUser = new User();
							destUser.setId(userId);
							saveNotification(sourceUser, destUser, type, postId);
						}
						User currentUser = (User) service.getById(User.class, sessionUserId);
						UserPosts post = new UserPosts();
						post.setId(postId);
						saveUserActivity(currentUser, null, postId, type);
					}
				} catch (Exception e) {
					YOALogger.error(AJAX_VIEW_HANDLER, "commentPost", e.getMessage());
				}
			}

		}
		return null;
	}

	@Override
	public List<UserToConnectBean> usersToAutoSearchBox(HttpServletRequest request) throws YOAException {

		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long userId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("currentUserId", userId));
			List<Object[]> usersResultSet = service
					.getColumnByHqlQuery(DBQueries.GET_USERS_FOR_CONNECTIONS_IN_SEARCH_BLOCK, conditions);
			if (usersResultSet != null && !usersResultSet.isEmpty()) {
				YOAMapper mapper = new YOAMapper();
				List<UserToConnectBean> connectBeans = mapper.mapResultSetWithConnectBean(usersResultSet);
				return connectBeans;
			}
		}

		return null;

	}

	@Override
	public List<UserToConnectBean> connectedUser(HttpServletRequest request) throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			List<Long> usersIds = new ArrayList<>();
			Long userId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("userId", userId));
			conditions.add(new KeyValue("status", ConnectStatus.ACCEPTED.name()));
			List<Object> resultSet = service.executeHqlQuery(DBQueries.GET_ACTIVE_USER_CONNECTIONS, conditions);
			if (resultSet != null) {
				Iterator<Object> iterator = resultSet.iterator();
				while (iterator.hasNext()) {
					Object object = (Object) iterator.next();
					usersIds.add((Long) object);
				}
			}
			List<Object[]> usersResultSet = null;
			conditions.clear();
			conditions.add(new KeyValue("currentUserId", userId));
			if (!usersIds.isEmpty()) {
				conditions.add(new KeyValue("in_clause_long_connectionIds", usersIds));
				usersResultSet = service.getColumnByHqlQuery(
						DBQueries.GET_USERS_FOR_CONNECTIONS_BLOCK + DBQueries.GET_USERS_FOR_CONNECTIONS_BLOCK_IN_CLAUSE,
						conditions);
			} else
				usersResultSet = service.getColumnByHqlQuery(DBQueries.GET_USERS_FOR_CONNECTIONS_BLOCK, conditions);
			if (usersResultSet != null && !usersResultSet.isEmpty()) {
				YOAMapper mapper = new YOAMapper();
				List<UserToConnectBean> connectBeans = mapper.mapResultSetWithConnectBean(usersResultSet);
				return connectBeans;
			}
		}
		return null;
	}

	@Override
	public List<ConnectionActivity> getConnectionActivity(HttpServletRequest request, boolean isFirst)
			throws YOAException {
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			List<ConnectionActivity> userActivities = new ArrayList<>();
			List<Long> actIds = new ArrayList<>();
			Long userId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("userId", userId));
			String query = null;
			if (isFirst) {
				query = DBQueries.GET_USER_ACTIVITY_DATA;
			} else {
				query = DBQueries.GET_USER_ACTIVITY_ISNOTREAD_DATA;
			}
			List<Object[]> resultSet = service.executeSqlQuery(query, conditions);
			for (Object[] o : resultSet) {
				long fromUserId = o[0] == null ? 0 : Long.valueOf(String.valueOf(o[0]));
				long secondUserId = o[1] == null ? 0 : Long.valueOf(String.valueOf(o[1]));
				String desc = o[2] == null ? "" : String.valueOf(o[2]);
				String type = o[3] == null ? "" : String.valueOf(o[3]);
				Date date = o[4] == null ? null : (Date) o[4];
				long postId = o[5] == null ? 0 : Long.valueOf(String.valueOf(o[5]));
				long actId = o[6] == null ? 0 : Long.valueOf(String.valueOf(o[6]));
				if (type != null && !type.isEmpty()) {
					if (type.equals(NotificationType.ACCEPT_CONNECT.name())) {
						if (fromUserId != userId.longValue() && secondUserId != userId.longValue()) {
							User fromUser = (User) service.getById(User.class, fromUserId);
							User secondUser = (User) service.getById(User.class, secondUserId);
							ConnectionActivity conActivity = new ConnectionActivity();
							conActivity.setUserId1(fromUserId);
							conActivity.setUserId2(secondUserId);
							conActivity.setConnName1(fromUser.getFullName());
							conActivity.setConnName2(secondUser.getFullName());
							conActivity.setType(type);
							String userImage1 = null;
							String userImage2 = null;
							if (fromUser.getProfilePhoto() != null) {
								try {
									userImage1 = Util.getEncodedImage(fromUser.getProfilePhoto());
								} catch (Exception e) {

								}
							}
							conActivity.setUserImage1(userImage1);
							if (secondUser.getProfilePhoto() != null) {
								try {
									userImage2 = Util.getEncodedImage(secondUser.getProfilePhoto());
								} catch (Exception e) {

								}
							}
							conActivity.setUserImage2(userImage2);
							conActivity.setDesc(desc);
							conActivity.setDate(date);
							conActivity.setTime(DateUtil.getDateASTimeAgo(date));
							userActivities.add(conActivity);
						}
					} else if (type.equals(NotificationType.POST_LIKE.name())
							|| type.equals(NotificationType.POST_UNLIKE.name())
							|| type.equals(NotificationType.POST_COMMENTS.name())) {
						conditions.clear();
						conditions.add(new KeyValue("userId", userId));
						conditions.add(new KeyValue("postId", postId));
						Object result = service.getSingleColumnByHqlQuery(DBQueries.CHECK_IS_POST_BY_USER, conditions);
						if (result == null) {
							User fromUser = (User) service.getById(User.class, fromUserId);
							ConnectionActivity conActivity = new ConnectionActivity();
							conActivity.setUserId1(fromUserId);
							conActivity.setConnName1(fromUser.getFullName());
							String userImage1 = null;
							if (fromUser.getProfilePhoto() != null) {
								try {
									userImage1 = Util.getEncodedImage(fromUser.getProfilePhoto());
								} catch (Exception e) {

								}
							}
							conActivity.setUserImage1(userImage1);
							conActivity.setType(type);
							conActivity.setDesc(desc);
							conActivity.setDate(date);
							conActivity.setTime(DateUtil.getDateASTimeAgo(date));
							userActivities.add(conActivity);
						}

					}
					actIds.add(actId);
				}

			}
			conditions.clear();
			conditions.add(new KeyValue("in_clause_long_actIds", actIds));
			service.updateByHQLQuery(DBQueries.MARK_READ_USERACTIVITY, conditions);
			return userActivities;
		} else
			return null;
	}

	@Override
	public ResponseBean getUserConnection(HttpServletRequest request) throws YOAException {
		/**
		 * TODO get user id from session get session user's connection
		 * 
		 */
		ResponseBean response = new ResponseBean();

		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long userId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			List<KeyValue> conditions = new ArrayList<KeyValue>();
			YOAMapper mapper = new YOAMapper();

			conditions.add(new KeyValue("userId", userId));
			conditions.add(new KeyValue("status", ConnectStatus.ACCEPTED.name()));

			response.setResult(mapper.mapUsersWithConnectBean(getConnectedUsers(conditions)));
			response.setResponseCode(HttpStatus.OK.name());
			return response;

		} else {
			throw new YOAException(YOArtistErrorCode.UNAUTHORIZED_USER, YOArtistErrorMessages.SESSION_EXPIRED);
		}
	}

	public List<User> getConnectedUsers(List<KeyValue> conditions) throws YOAException {
		/**
		 * get all connected users
		 * 
		 */

		List<Object> usersObject = service.executeHqlQuery(DBQueries.GET_USER_CONNECTION, conditions);
		List<User> users = new ArrayList<User>();

		for (Object obj : usersObject) {
			User user = (User) obj;
			users.add(user);
		}

		return users;
	}

	@Override
	public ResponseBean sharePost(HttpServletRequest request) throws YOAException {
		/**
		 * TODO check session user get connections id(public if connection ids
		 * is empty else customs)
		 * 
		 */
		ResponseBean response = new ResponseBean();
		if (request.getSession().getAttribute(YOAConstants.SESSION_USER_ID) != null) {
			Long userId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			Long postId = Long.valueOf(request.getParameter("postId"));
			List<String> connectionIdsStr = (Arrays.asList(request.getParameter("connectionIds").split(",")));
			List<Long> connectionIds = new ArrayList<Long>();
			String query;
			if (connectionIdsStr != null && !connectionIdsStr.isEmpty() && !connectionIdsStr.contains("")) {
				// Insert entry for custom
				for (String connectionIdStr : connectionIdsStr) {
					connectionIds.add((Long.valueOf(connectionIdStr)));
				}

			} else {
				// Insert connection for public
				List<KeyValue> conditions = new ArrayList<KeyValue>();
				conditions.add(new KeyValue("sessionUserId", userId));
				conditions.add(new KeyValue("status", UserStatus.ACTIVE.name()));
				query = DBQueries.GET_ALL_ACTIVE_USER_ID_EXCEPT_SESSION_USER_ID;
				List<Object> userIds = service.executeHqlQuery(query, conditions);
				for (Object obj : userIds) {
					connectionIds.add((Long.valueOf(String.valueOf(obj))));
				}

			}
			service.saveSharedPost(postId, connectionIds, userId);

			response.setResponseCode(HttpStatus.OK.name());
			return response;

		} else {
			throw new YOAException(YOArtistErrorCode.UNAUTHORIZED_USER, YOArtistErrorMessages.SESSION_EXPIRED);
		}

	}

	@Override
	public ResponseBean getSessionUserProfile(HttpServletRequest request) throws YOAException {
		ResponseBean response = new ResponseBean();
		Object currentUserId = request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
		if (currentUserId != null) {
			Long userId = (Long) currentUserId;
			User user = (User) service.getById(User.class, userId);
			if (user == null) {
				response.setResponseCode(HttpStatus.NO_CONTENT.name());
			} else {
				YOAMapper mapper = new YOAMapper();
				UserBean userBean = mapper.mapUserToUserProfileBean(user);
				response.setResponseCode(HttpStatus.OK.name());
				response.setResult(userBean);
			}
		} else {
			throw new YOAException(YOArtistErrorCode.UNAUTHORIZED_USER, YOArtistErrorMessages.SESSION_EXPIRED);
		}

		return response;
	}

	@Override
	public ResponseBean saveEducationDetails(HttpServletRequest request, EducationBean education) throws YOAException {
		ResponseBean response = new ResponseBean();
		Object currentUserId = request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
		if (currentUserId != null) {
			Long userId = (Long) currentUserId;
			User user = (User) service.getById(User.class, userId);
			if (user == null) {
				response.setResponseCode(HttpStatus.NO_CONTENT.name());
			} else {
				EducationalDetailsVO edu = new EducationalDetailsVO();
				YOAMapper mapper = new YOAMapper();
				mapper.mapEducationBeanToVO(edu, education);
				edu.setUser(user);
				service.save(edu);
				response.setResponseCode(HttpStatus.OK.name());
			}
		}
		return response;
	}

	@Override
	public ResponseBean updateEducationDetails(HttpServletRequest request, EducationBean education, long id)
			throws YOAException {
		ResponseBean response = new ResponseBean();
		EducationalDetailsVO edu=(EducationalDetailsVO)service.getById(EducationalDetailsVO.class, id);
			if (edu == null) {
				response.setResponseCode(HttpStatus.NO_CONTENT.name());
			} else {
				YOAMapper mapper = new YOAMapper();
				mapper.mapEducationBeanToVO(edu, education);
				service.saveOrUpdate(edu);
				response.setResponseCode(HttpStatus.OK.name());
			}
		return response;
	}

	@Override
	public ResponseBean saveEmploymentDetails(EmploymentBean employment, HttpServletRequest request) throws YOAException {
		
		ResponseBean response = new ResponseBean();
		final String DATE_FORMAT = "MM/dd/yyyy";
		User user = null;
		
		Date startDate;
		Date endDate = null;
		
		long userId;
		long employementId;
		EmployementDetailsVO employementDetails = new EmployementDetailsVO();
		
		Object currentUserId = request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
		if (currentUserId != null) {
			userId = (Long) currentUserId;
			user = (User) service.getById(User.class, userId);
			startDate = DateUtil.formatDateToDate(employment.getStartDate(), DATE_FORMAT);
			if(!employment.isCurrentlyWorking()){
				endDate = DateUtil.formatDateToDate(employment.getEndDate(), DATE_FORMAT);
			}
			
			employementDetails.setOrgName(employment.getOrgName());
			employementDetails.setCountry(employment.getCountry());
			employementDetails.setCreatedDate(new Date());
			employementDetails.setDeleted(Boolean.FALSE);
			employementDetails.setEndDate(endDate);
			employementDetails.setCurrentlyWorking(employment.isCurrentlyWorking());
			employementDetails.setJobPosition(employment.getJobPosition());
			employementDetails.setModifiedDate(new Date());
			employementDetails.setStartDate(startDate);
			employementDetails.setState(employment.getState());
			employementDetails.setUser(user);

			employementDetails.setEmploymentDuration(Util.getEmploymentDuration(startDate, endDate));
			
			employementId = service.save(employementDetails);
			employementDetails.setId(employementId);
			response.setResponseCode(HttpStatus.OK.name());
		}else{
			throw new YOAException(YOArtistErrorCode.UNAUTHORIZED_USER, YOArtistErrorCode.UNAUTHORIZED_USER);
		}
		return response;
	}

	@Override
	public ResponseBean getEmpData(HttpServletRequest request) throws YOAException {
		ResponseBean response = new ResponseBean();
		List<EmploymentBean> employmentDetails = new ArrayList<>();
		String query;
		long userId;
		
		Object currentUserId = request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
		if (currentUserId != null) {
			userId = (Long) currentUserId;
			
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("userId", userId));
			query = "select e from EmployementDetailsVO e join e.user u where u.id=:userId";
			List<Object> empDetailsObj = service.executeHqlQuery(query, conditions);
			
			populateEmpObjToBean(employmentDetails, empDetailsObj);
			
			response.setResponseCode(HttpStatus.OK.name());
			response.setResult(employmentDetails);
		}else{
			throw new YOAException(YOArtistErrorCode.UNAUTHORIZED_USER, YOArtistErrorCode.UNAUTHORIZED_USER);
		}
		return response;
	}

	
	private void populateEmpObjToBean(List<EmploymentBean> employmentDetails, List<Object> empDetailsObj) {
		
		if(empDetailsObj!=null){
			for(Object empObj: empDetailsObj){
				EmployementDetailsVO emp = (EmployementDetailsVO) empObj;
				EmploymentBean empBean = new EmploymentBean();
				empBean.setCountry(emp.getCountry());
				empBean.setId(emp.getId());
				empBean.setIsCurrentlyWorking(emp.isCurrentlyWorking());
				empBean.setState(emp.getState());
				empBean.setStartDate(DateUtil.formatDateToString(emp.getStartDate(), "dd-MM-yyyy"));
				if(emp.isCurrentlyWorking()){
					empBean.setEmploymentDuration(DateUtil.formatDateToString(emp.getStartDate(), "MMMM yyyy") + "  -  Present" );
				}else{
					empBean.setEmploymentDuration(DateUtil.formatDateToString(emp.getStartDate(), "MMMM yyyy") + "  -  " + DateUtil.formatDateToString(emp.getEndDate(), "MMMM yyyy"));
					empBean.setEndDate(DateUtil.formatDateToString(emp.getEndDate(), "dd-MM-yyyy"));
				}
				empBean.setJobPosition(emp.getJobPosition());
				empBean.setOrgName(emp.getOrgName());
				
				employmentDetails.add(empBean);
			}
		}
	}

	@Override
	public ResponseBean deleteEmpData(long id, HttpServletRequest request) throws YOAException {
		ResponseBean response = new ResponseBean();
		
		Object currentUserId = request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
		if (currentUserId != null) {
			service.deleteById(EmployementDetailsVO.class,
					id);
			
			response.setResponseCode(HttpStatus.OK.name());
		}else{
			throw new YOAException(YOArtistErrorCode.UNAUTHORIZED_USER, YOArtistErrorCode.UNAUTHORIZED_USER);
		}
		return response;
	}

	@Override
	public ResponseBean updateEmploymentDetails(EmploymentBean employment, HttpServletRequest request)throws YOAException {
		ResponseBean response = new ResponseBean();
		final String DATE_FORMAT = "MM/dd/yyyy";
		
		Date startDate;
		Date endDate = null;
		
		EmployementDetailsVO employementDetails = null;
		
		Object currentUserId = request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
		if (currentUserId != null) {
			employementDetails = (EmployementDetailsVO) service.getById(EmployementDetailsVO.class, employment.getId());
			startDate = DateUtil.formatDateToDate(employment.getStartDate(), DATE_FORMAT);
			if(!employment.isCurrentlyWorking()){
				endDate = DateUtil.formatDateToDate(employment.getEndDate(), DATE_FORMAT);
			}
			
			employementDetails.setOrgName(employment.getOrgName());
			employementDetails.setCountry(employment.getCountry());
			employementDetails.setEndDate(endDate);
			employementDetails.setCurrentlyWorking(employment.isCurrentlyWorking());
			employementDetails.setJobPosition(employment.getJobPosition());
			employementDetails.setStartDate(startDate);
			employementDetails.setState(employment.getState());

			employementDetails.setEmploymentDuration(Util.getEmploymentDuration(startDate, endDate));
			
			service.merge(employementDetails);
			response.setResponseCode(HttpStatus.OK.name());
		}else{
			throw new YOAException(YOArtistErrorCode.UNAUTHORIZED_USER, YOArtistErrorCode.UNAUTHORIZED_USER);
		}
		return response;
	}

	@Override
	public ResponseBean saveSummaryDetails(SummaryBean summary, HttpServletRequest request) throws YOAException {
		ResponseBean response = new ResponseBean();
		User user = null;
		
		long userId;
		SummaryVO summaryVo = new SummaryVO();
		
		Object currentUserId = request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
		if (currentUserId != null) {
			userId = (Long) currentUserId;
			user = (User) service.getById(User.class, userId);
			
			summaryVo.setSummary(summary.getSummary());
			summaryVo.setModifiedDate(new Date());
			summaryVo.setDeleted(Boolean.FALSE);
			summaryVo.setCreatedDate(new Date());
			summaryVo.setUser(user);
			
			service.save(summaryVo);
			response.setResponseCode(HttpStatus.OK.name());
		}else{
			throw new YOAException(YOArtistErrorCode.UNAUTHORIZED_USER, YOArtistErrorCode.UNAUTHORIZED_USER);
		}
		return response;
	}

	@Override
	public ResponseBean getSummaryData(HttpServletRequest request) throws YOAException {
		ResponseBean response = new ResponseBean();
		SummaryBean summaryBean = new SummaryBean();
		
		String query;
		long userId;
		
		Object currentUserId = request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
		if (currentUserId != null) {
			userId = (Long) currentUserId;
			
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("userId", userId));
			query = "select s from SummaryVO s join s.user u where u.id=:userId";
			List<Object> summaryObj = service.executeHqlQuery(query, conditions);
			
			populateSummaryObjToBean(summaryBean, summaryObj);
			
			response.setResponseCode(HttpStatus.OK.name());
			response.setResult(summaryBean);
		}else{
			throw new YOAException(YOArtistErrorCode.UNAUTHORIZED_USER, YOArtistErrorCode.UNAUTHORIZED_USER);
		}
		return response;
		
	}

	private void populateSummaryObjToBean(SummaryBean summaryBean, List<Object> summaryObj) {
		SummaryVO s;
		if(summaryObj !=null){
			for(Object obj: summaryObj){
				if(obj!=null){
					s = (SummaryVO) obj;
					summaryBean.setSummary(s.getSummary());
					summaryBean.setId(s.getId());
				}
			}
		}
		
	}

	@Override
	public ResponseBean deleteSummaryData(long id, HttpServletRequest request) throws YOAException {
		ResponseBean response = new ResponseBean();
		
		Object currentUserId = request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
		if (currentUserId != null) {
			service.deleteById(SummaryVO.class,
					id);
			
			response.setResponseCode(HttpStatus.OK.name());
		}else{
			throw new YOAException(YOArtistErrorCode.UNAUTHORIZED_USER, YOArtistErrorCode.UNAUTHORIZED_USER);
		}
		return response;
	}

	@Override
	public ResponseBean updateSummaryDetails(SummaryBean summary, HttpServletRequest request) throws YOAException {
		ResponseBean response = new ResponseBean();
		
		
		SummaryVO summaryDetails = null;
		
		Object currentUserId = request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
		if (currentUserId != null) {
			summaryDetails = (SummaryVO) service.getById(SummaryVO.class, summary.getId());
			summaryDetails.setSummary(summary.getSummary());
			summaryDetails.setModifiedDate(new Date());
			service.merge(summaryDetails);
			
			response.setResponseCode(HttpStatus.OK.name());
		}else{
			throw new YOAException(YOArtistErrorCode.UNAUTHORIZED_USER, YOArtistErrorCode.UNAUTHORIZED_USER);
		}
		return response;
	}

}
