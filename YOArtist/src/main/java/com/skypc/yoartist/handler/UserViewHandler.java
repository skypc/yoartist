/*
 *
 */
package com.skypc.yoartist.handler;

import static com.skypc.yoartist.constants.YOAConstants.REDIRECT_USER_HOME_PAGE;
import static com.skypc.yoartist.constants.YOAConstants.SESSION_USER_ID;
import static com.skypc.yoartist.constants.YOAConstants.SESSION_USER_FULL_NAME;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.skypc.yoartist.bean.MessageCodeBean;
import com.skypc.yoartist.bean.PostsBean;
import com.skypc.yoartist.constants.MessagesConstant;
import com.skypc.yoartist.constants.RequestParam;
import com.skypc.yoartist.constants.YOAConstants;
import com.skypc.yoartist.enums.UserPostStatus;
import com.skypc.yoartist.enums.UserStatus;
import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.helper.MailHelper;
import com.skypc.yoartist.helper.MessagesReader;
import com.skypc.yoartist.ihandler.IUserView;
import com.skypc.yoartist.logger.YOALogger;
import com.skypc.yoartist.mail.YOAMailApi;
import com.skypc.yoartist.mapper.YOAMapper;
import com.skypc.yoartist.query.DBQueries;
import com.skypc.yoartist.security.AESEncriptor;
import com.skypc.yoartist.service.IService;
import com.skypc.yoartist.util.KeyValue;
import com.skypc.yoartist.vo.IGenericVO;
import com.skypc.yoartist.vo.User;
import com.skypc.yoartist.vo.UserPosts;

public class UserViewHandler implements IUserView {

	private static final String CATEGORY_NAME = "UserViewHandler";

	@Autowired
	private IService service;

	@Autowired
	@Lazy
	private YOAMailApi yoaMailApi;

	@Override
	public ModelAndView registerUser(HttpServletRequest request, RedirectAttributes attr) throws YOAException {
		ModelAndView mav = new ModelAndView("redirect:/");
		String fullName = request.getParameter(RequestParam.FULL_NAME);
		String email = request.getParameter(RequestParam.REGISTER_EMAIL);
		String password = request.getParameter(RequestParam.REGISTER_PASSWORD);
		long id = 0;
		if (fullName != null && !fullName.isEmpty() && email != null && !email.isEmpty() && password != null
				&& !password.isEmpty()) {
			List<KeyValue> conditions = new ArrayList<>();
			conditions.add(new KeyValue("email", email));
			conditions.add(new KeyValue("status", UserStatus.ACTIVE.name()));
			Object existObj = service.getSingleColumnByHqlQuery(DBQueries.GET_ID_BY_EMAIL_AND_STATUS, conditions);
			if (existObj != null) {
				MessageCodeBean message = MessagesReader.getMessage(MessagesConstant.USER_EXIST_ERROR, false);
				attr.addFlashAttribute(MessagesConstant.MESSAGE, message.getMessage());
				attr.addFlashAttribute(MessagesConstant.CODE, message.getCode());
			} else {
				User user = new User();
				String firstName=null;
				String lastName=null;
				if(fullName!=null && !fullName.isEmpty()){
					fullName=fullName.trim();
					firstName=fullName;
					if(fullName.contains(" ")){
						firstName=fullName.substring(0,fullName.indexOf(" "));
						lastName=fullName.substring(fullName.indexOf(" ")+1,fullName.length());
					}
				}
				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setEmail(email);
				user.setPassword(AESEncriptor.encrypt(password));
				user.setStatus(UserStatus.PENDING.name());
				user.setCreatedDate(new Date());
				user.setModifiedDate(new Date());

				// uuid

				String token = UUID.randomUUID().toString();
				user.setEmailVerificationUUID(token);

				conditions.clear();
				conditions.add(new KeyValue("email", email));
				conditions.add(new KeyValue("status", UserStatus.PENDING.name()));
				Object obj = service.getSingleColumnByHqlQuery(DBQueries.GET_ID_BY_EMAIL_AND_STATUS, conditions);
				if (obj != null) {
					id = Long.valueOf(String.valueOf(obj));
					user.setId(id);
					service.saveOrUpdate(user);
				} else {
					id = service.save(user);
				}
				if (id > 0) {
					MessageCodeBean message = MessagesReader.getMessage(MessagesConstant.EMAIL_SEND_SUCCESS, true);
					attr.addFlashAttribute(MessagesConstant.MESSAGE, message.getMessage().replace("#email#", email));
					attr.addFlashAttribute(MessagesConstant.CODE, message.getCode());
				}

				MailHelper mailHelper = new MailHelper();
				System.out.println(token);
				//mailHelper.sendRegistraionVerificationMail(fullName, email, token, yoaMailApi);
			}
		}

		return mav;
	}

	@Override
	public ModelAndView signInAccount(HttpServletRequest request, RedirectAttributes attr, HttpServletResponse response)
			throws YOAException {
		final String METHOD_NAME = "signInAccount";
		String email = request.getParameter(RequestParam.LOGIN_EMAIL);
		String password = request.getParameter(RequestParam.LOGIN_PASSWORD);
		if (request.getParameterValues("rememberMe") != null) {
			String rem[] = request.getParameterValues("rememberMe");
			if (rem.length > 0 && rem[0].equalsIgnoreCase("on")) {
				Cookie checkobxCookie = new Cookie("checkbox", "1");
				response.addCookie(checkobxCookie);
			} else {
				Cookie checkobxCookie = new Cookie("checkbox", "0");
				response.addCookie(checkobxCookie);
			}
		}
		ModelAndView mav = null;

		List<KeyValue> conditions = new ArrayList<>();
		conditions.add(new KeyValue("email", email));
		conditions.add(new KeyValue("password", AESEncriptor.encrypt(password)));
		User dbUser = (User) service.getByConditions(User.class, conditions);

		if (dbUser == null) {
			mav = new ModelAndView("redirect:/");
			attr.addFlashAttribute("loginErrorMsg", "Invalid username or password");
			return mav;
		} else if (dbUser.getStatus().equals(UserStatus.PENDING.name())) {
			mav = new ModelAndView("redirect:/");
			attr.addFlashAttribute("loginErrorMsg", "You didn't confirm your email. please confirm first");
			return mav;
		} else if (dbUser.getStatus().equals(UserStatus.DEACTIVE.name())) {
			mav = new ModelAndView("redirect:/");
			attr.addFlashAttribute("loginErrorMsg", "account is Deactivated! please contact to support");
			return mav;
		} else {
			createSessionAttributes(request, dbUser);

			mav = new ModelAndView("redirect:" + REDIRECT_USER_HOME_PAGE);

			if (request.getParameterValues("rememberMe") != null) {
				String rem[] = request.getParameterValues("rememberMe");
				if (rem.length > 0 && rem[0].equalsIgnoreCase("on")) {

					Cookie userCookie = new Cookie("loginName", email);
					Cookie passwordCookie = new Cookie("loginPass", password);
					Cookie checkobxCookie = new Cookie("checkbox", "1");
					response.addCookie(userCookie);
					response.addCookie(passwordCookie);
					response.addCookie(checkobxCookie);
				} else {
					Cookie userCookie = new Cookie("loginName", "");
					Cookie passwordCookie = new Cookie("loginPass", "");
					Cookie checkobxCookie = new Cookie("checkbox", "0");
					response.addCookie(userCookie);
					response.addCookie(passwordCookie);
					response.addCookie(checkobxCookie);
				}
			} else {
				Cookie userCookie = new Cookie("loginName", "");
				Cookie passwordCookie = new Cookie("loginPass", "");
				response.addCookie(userCookie);
				response.addCookie(passwordCookie);

			}
			return mav;
		}
	}

	/**
	 * @param request
	 * @param METHOD_NAME
	 * @param base64Image
	 * @param dbUser
	 */
	private void createSessionAttributes(HttpServletRequest request, User dbUser) {
		final String METHOD_NAME="createSessionAttributes";
		String base64Image=null;
		
		request.getSession().setAttribute(SESSION_USER_ID, dbUser.getId());
		if (dbUser.getFullName() != null)
			request.getSession().setAttribute(SESSION_USER_FULL_NAME, dbUser.getFullName());

		if (dbUser.getProfilePhoto() != null) {
			byte[] encodeBase64 = Base64.encodeBase64(dbUser.getProfilePhoto());
			try {
				base64Image = new String(encodeBase64, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				YOALogger.error(CATEGORY_NAME, METHOD_NAME, e.getMessage());
				base64Image = "";
			}
			request.getSession().setAttribute("profileImage", base64Image);
		}
	}

	@Override
	public ModelAndView homePage(HttpServletRequest request, RedirectAttributes attr) throws YOAException {
		ModelAndView mav = new ModelAndView("home");
		List<KeyValue> conditions=new ArrayList<>();
		Long userId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
		conditions.add(new KeyValue("status",UserPostStatus.POSTED.name()));
		List<Object[]> resultSet = service.getColumnByHqlQuery(DBQueries.GET_USER_POST_FOR_HOME_PAGE, conditions);
		
		if(resultSet!=null){
			YOAMapper mapper = new YOAMapper();
			List<PostsBean> posts = mapper.mapResultSetWithPostBean(resultSet);
			for(PostsBean postsBean : posts) {
				conditions.clear();
				conditions.add(new KeyValue("postId",Long.valueOf(postsBean.getPostId())));
				resultSet = service.getColumnByHqlQuery(DBQueries.GET_COMMENT_FOR_POST, conditions);
				if(resultSet!=null){
					postsBean.setCommentList(mapper.mapResultSetWithCommentBean(resultSet));
				}
				conditions.clear();
				conditions.add(new KeyValue("postId",Long.valueOf(postsBean.getPostId())));
				Object count = service.getSingleColumnByHqlQuery(DBQueries.GET_COUNT_LIKES_FOR_POST, conditions);
				if(count!=null){
					postsBean.setLikeCount(String.valueOf(count));
				}
				conditions.clear();
				conditions.add(new KeyValue("postId",Long.valueOf(postsBean.getPostId())));
				conditions.add(new KeyValue("userId",Long.valueOf(userId)));
				Object id = service.getSingleColumnByHqlQuery(DBQueries.GET_IS_LIKES_FOR_POST, conditions);
				if(id!=null){
					postsBean.setLike(true);
				}
				
			}
			mav.addObject("posts", posts);
		}
		return mav;
	}

	@Override
	public ModelAndView confirmEmail(String token, HttpServletRequest request, RedirectAttributes attr)
			throws YOAException {
		ModelAndView mav = new ModelAndView("redirect:"+REDIRECT_USER_HOME_PAGE); 
		List<KeyValue> conditions = new ArrayList<>();
		conditions.add(new KeyValue("emailVerificationUUID", token));
		User dbUser =(User) service.getByConditions(User.class, conditions);
		if(dbUser==null){
			mav = new ModelAndView("redirect:/");
			MessageCodeBean message = MessagesReader.getMessage(MessagesConstant.INVALID_VERIFICATION_TOKEN, false);
			attr.addFlashAttribute(MessagesConstant.MESSAGE, message.getMessage());
			attr.addFlashAttribute(MessagesConstant.CODE, message.getCode());
			return mav;
		}else{
			createSessionAttributes(request, dbUser);
			dbUser.setEmailVerificationUUID(null);
			dbUser.setStatus(UserStatus.ACTIVE.name());
			dbUser.setModifiedDate(new Date());
			service.saveOrUpdate(dbUser);
		}
		return mav;
	}

	@Override
	public ModelAndView submitPost(String comments, final Integer uploadedId1, HttpServletRequest request,
			RedirectAttributes attr) throws YOAException {
		ModelAndView mav=new ModelAndView("redirect:/user/home");
		long uploadedId=uploadedId1;
		UserPosts userPosts = new UserPosts();
		if(uploadedId>0){
			userPosts=(UserPosts)service.getById(UserPosts.class, uploadedId);
			userPosts.setStatus(UserPostStatus.POSTED.name());
			userPosts.setPostDescription(comments);
			userPosts.setModifiedDate(new Date());
			service.saveOrUpdate(userPosts);
			
			
		}else{
			Long userId = (Long) request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
			User user=(User)service.getById(User.class, userId);
			userPosts.setStatus(UserPostStatus.POSTED.name());
			userPosts.setPostDescription(comments);
			userPosts.setUser(user);
			userPosts.setCreatedDate(new Date());
			userPosts.setModifiedDate(new Date());
			Long id = service.save(userPosts);
			
		}
		return mav;
	}

	@Override
	public ModelAndView sharePage(HttpServletRequest request, RedirectAttributes attr) throws YOAException {
		return null;
	}

}