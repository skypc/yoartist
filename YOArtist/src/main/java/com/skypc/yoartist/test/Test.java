package com.skypc.yoartist.test;

import java.util.Date;

import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.security.AESEncriptor;
import com.skypc.yoartist.util.DateUtil;

class P{
	public void priint(){
		System.out.println("parent");
	}
}

class C extends P{
	public void priint(String s){
		System.out.println("child");
	}
}
public class Test {

	
	public static void main(String[] args)  {
		P p=new P();
		P p1=new C();
		C c=new C();
		p.priint();
		p1.priint();
		c.priint();
		c.priint("ds");
		
		System.out.println(DateUtil.formatDateToString(new Date(), "MMMM-YYYY"));
		System.out.println(AESEncriptor.decrypt("WjQDvyRBlWwryGQW8sMdRQ=="));
	}
	
}
