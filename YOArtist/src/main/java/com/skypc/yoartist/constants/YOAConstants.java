/*
 *
 */
package com.skypc.yoartist.constants;

// TODO: Auto-generated Javadoc
/**
 * The Interface InnovisionConstant.
 */
public interface YOAConstants {

	/** The session user id. */
	String SESSION_USER_ID = "SessionUserId";

	/** The session user name. */
	String SESSION_USER_FULL_NAME = "SessionFullName";

	/** The session user email. */
	String SESSION_USER_EMAIL = "SessionUserEmail";

	/** The session user type. */
	String SESSION_USER_ROLE = "SessionUserRole";

	String SESSION_USER_TYPE = "SessionUserType";

	String SESSION_SUB_ADMIN_LINKS = "SessionSubAdminLinks";

	/** The redirect sign in. */
	String REDIRECT_ADMIN_LOGIN = "/admin/login";

	String REDIRECT_LOGIN = "login";

	String BEAN_MAIL_HELPER = "MailHelper";

	String REDIRECT_ADMIN_DASHBOARD_PAGE = "/admin/dashboard";
	
	String REDIRECT_USER_HOME_PAGE = "/user/home";

	String VERIFIED_ORGANIZATION = "VERIFIED";

	String PENDING_ORGANIZATION = "PENDING";

	String ACTIVE_USER = "ACTIVE";

	String PENDING_USER = "PENDING";

	String DEACTIVE_USER = "DEACTIVE";

	
	String ORDER_BY_ASC = "asc";
	
	String ORDER_BY_DESC = "desc";
	
	long ONE_MB = 1024__000;

	String SUCCESS = "success";

	String UNSUCCESS = "unsuccess";

	String SUCCESS_LOGIN = "success login";

	String GUEST_DASHBOARD = "getGuestDashboard";

	String HOME_PAGE = "/";
	
	final String FILE_SYSTEM_POST_DIR_PATH = "fileSystem.yoartist.dir.path.postFiles";

	public static final String SELF = "SELF";

	public static final String PART_OFF = "PART_OFF";

	public static final String INVALID_USER = "Invalid email address or password";

	public static final String USER_DASHBOARD = "user/dashboard";

	public static final String GUEST_USER_DASHBOARD = "getGuestUserDashboard";
	
	public static final String RECAPTCHA_SECRET_KEY = "6LfN_icTAAAAAP1cjlWC68yO7DA11LmLSRjtkFkT";

	public static final String RECAPTCHA_URL = "https://www.google.com/recaptcha/api/siteverify";

	public static final String RECAPTCHA_USER_AGENT = "Mozilla/5.0";

	public static final String REDIRECT_TO_DASHBOARD = "/";

	final String LINKED_IN_CLEINT_KEY = "linkedin.client.key";
	final String LINKED_IN_CLEINT_SECRET = "linkedin.client.secret";
	final String LINKED_IN_CALL_BACK_URL = "linkedin.call.back.url";
	final String LINKED_IN_AUTH_URL = "linkedin.auth.url";
	final String LINKED_IN_ACCESS_TOKEN = "access_token";

	final String ORCID_IN_CLEINT_KEY = "orcid.client.key";
	final String ORCID_IN_CLEINT_SECRET = "orcid.client.secret";
	final String ORCID_IN_CALL_BACK_URL = "orcid.call.back.url";
	final String ORCID_IN_AUTH_URL = "orcid.auth.url";
	final String ORCID_IN_ACCESS_TOKEN = "access_token";

	String SING_UP_TYPE_LINKED_IN = "linkedIn";
	String SING_UP_TYPE_ORC_ID = "orcId";

	String SIGN_UP_CLIENT_LINKED_IN_STATE = "786signUpCleint786";
	String SIGN_IN_CLIENT_LINKED_IN_STATE = "786signInCleint786";
	String SIGN_UP_ORG_LINKED_IN_STATE = "786signUpOrg786";

	String LINKED_IN_CLIENT_SINC_STATE = "LINKEDIN_ACTION_SINC";

	String SESSION_FIRST_NAME = "SessionFirstName";

	String SESSION_LAST_NAME = "SessionLastName";

	String LINKED_IN_CLIENT_STATE_SINC = "LINKEDIN_ACTION_SINC";

	String DATE_FORMATE_DATE_MONTH_STR_YEAR = "dd MMMM yyyy";

	String DATE_FORMATE_DATE_TIME_CUSTOME = "dd MMMM yyyy, hh.mm a";

	int LIMIT_RECORD_IN_DASH_BOARD = 10;
	int LIMIT_RECORD_IN_NOTIFICATION = 10;



	String PROJECT_ERROR_MESSAGE = "projectErrorMessage";

	int BATCH_SIZE = 50;

	String DATE_TIME_FORMAT = "dd-MMMM-yyyy, hh:mm a";


}
