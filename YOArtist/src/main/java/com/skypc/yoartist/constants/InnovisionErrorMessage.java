/*
 *
 */
package com.skypc.yoartist.constants;

public interface InnovisionErrorMessage {

    // @formatter:off
    public static final String MISSING_PARAM = "Please provide mandatory inputs";
    public static final String LOGIN_SUCCESS = "Login success";
    public static final String TRANSACTION_FAILED = "Failure during transaction";
    public static final String CAPTCHA_MISSING = "Please check the captcha";
    public static final String INVALID_CAPTCHA = "Captcha not verified";
    public static final String ERROR_MESSAGE = "ErrorMessage";
    public static final String DOMAIN_BLOCKED_MESSAGE = "is blocked please register with different domain name";
    public static final String MISMATCHED_EMAILID = "Organization domain name doesn't matched with given email address";
    public static final String INVALID_EMAIL_ADDRESS = "Invalid Email address";
    public static final String ROLE_NOT_FOUND = "Role not found";
    public static final String EMAIL_ADDRESS_ALREADY_EXISTS = "Email address already exists";
    public static final String MISSING_PARAMETER_USER_TYPE = "Please select user type";
    public static final String MISMATCHED_ORGANIZATION_AND_DOMAIN = "Domain doesn't belongs to above organization";
    public static final String MISMATCHED_ORGANIZATION_EMAI_AND_PROFESSIONAL_EMAIL = "Organization email address doesn't matched with given professional email address";

    public static final String MESSAGE_CODE = "messageCode";

    public static final int UNSUCCESS = 1001;

    public static final int SUCCESS = 1000;
    public static final String UNABLE_TO_READ_PROPERTY_FILE = "UNABLE TO READ PROPERTY FILE";
    public static final String INVALID_REQUIRED_PARAM = "Invalid required param";
    public static final String USER_NOT_FOUND = "User not found";
    public static final String USERNAME_ALREADY_EXISTS = "User name already exists";
    public static final String INVALID_USER = "Invalid user";
    public static final String INVALID_DATE_FORMAT = "Invalid date format";
    public static final String UNABLE_TO_CREATE_JSON = "Failed to create json";

    public static final String EDUCATION_DETAILS_DELETED = "Education details are delered already so failed to update";
    public static final String DEACTIVE_USER = "Your account has been deactivated please contact to admin";
    public static final Object REGISTRATION_FAILED = "Registration failed";
    public static final Object REGISTRATION_SUCCESS = "Registration successfully and link has sent to your email.";
    public static final String MAX_ADMIN_REACHED = "Maximum admin limit reached";
    public static final String PROJECT_POST_OR_UPDATE_ERR = "Invalid data in project";
    public static final String PROJECT_POST_OR_UPDATE_ERR_DESC_TAB = "Please fill all mandatory data in description tab";
    public static final String PROJECT_POST_OR_UPDATE_ERR_DELI_TAB = "Please fill all mandatory data in deliverables and outcomes tab";
    public static final String PROJECT_POST_OR_UPDATE_ERR_EVAL_TAB = "Please fill all mandatory data in evaluation criteria tab";
    public static final String PROJECT_POST_OR_UPDATE_ERR_TRMS_TAB = "Please fill all mandatory data in terms and conditions tab";
    public static final String PROJECT_POST_OR_UPDATE_ERR_DATA = "Project not exist";
    public static final String PROJ_INVITATION_SUCCESS = "Invitation sent successfully to selected user(s).";
    public static final String PROJ_SAVE_SUCCESS = "Project saved successfully.";
    public static final String PROJ_UPDATE_SUCCESS = "Project updated successfully.";
    public static final String PROJ_CANCEL_SUCCESS = "Project cancelled successfully.";
    public static final String PROJ_IN_DISPUTE_SUCCESS = "Project maked as 'in dispute' successfully.";
    public static final String PROJ_COMPLETE_SUCCESS = "Project maked as 'completed' successfully.";
    public static final String PROJ_RATED_SUCCESS = "Project review and rating submitted successfully.";
    public static final String PROJ_AWARD_SUCCESS = "Project awarded successfully.";
    public static final String PROJ_AWARD_FAIL_NOT_EXIT = "Posted project or submitted proposal not exist.";
    public static final String PROJ_AWARD_FAIL_ALREADY_AWARDED = "Project already awarded.";
    public static final String PROJ_AWARD_FAIL_INVALID_PROPOSAL = "Submitted proposal is not valid or researcher is no more active.";

    public static final String PROJ_EDIT_ERR_CLOSED = "Project is already completed or cancelled.";
    public static final String PROJ_EDIT_WARN_AWARDED = "Project is already awarded.";
    public static final String DATA_OBJECT_NOT_EXIST = "Data object not exist for id parameter.";
	public static final String INCONSITENT_DATA = "Data is inconsistent";

	
	
	
}
