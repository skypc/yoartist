/*
 *
 */
package com.skypc.yoartist.constants;

public class InnovisionErrorCodes {


    // @formatter:off
    // (+ve) values indicate errors found at runtime, for various reasons
    public static final int OBJECT_NOT_FOUND 			= 6;
    public static final int OPERATION_NOT_SUPPORTED 		= 5;
    public static final int INVALID_SESSION 			= 4;
    public static final int AUTHENTICATION_FAILED		= 3;
    public static final int USER_NOT_FOUND 			= 2;
    public static final int USER_ALREADY_EXIST 			= 1;

    public static final int TRANSACTION_FAILED 			= 574;
    // the only status for a successful API response
    public static final int SUCCESS 				= 0;

    // (-ve) values indicate errors caused by developer faults
    public static final int MISSING_REQUIRED_PARAM 		= -1;
    public static final int INVALID_REQUIRED_PARAM		= -2;
    public static final int INCONSITENT_DATASTORE 		= -3;
    public static final int FAILED_DATASTORE_ACCESS 		= -4;
    public static final int FAILED_DATASTORE_QUERY 		= -5;
    public static final int FAILED_TO_CREATE_RESPONSE 		= -6;
    public static final int INVALID_EMAIL_ADDRESS 		= -7;
    public static final String UNABLE_TO_READ_PROPERTY_FILE 	= "-8";
	public static final String INCONSITENT_DATA = "-9";

}
