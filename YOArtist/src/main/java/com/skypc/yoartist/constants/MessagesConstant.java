package com.skypc.yoartist.constants;

public interface MessagesConstant {
	
	String MESSAGE="MESSAGE";
	String CODE="CODE";
	int ERROR_CODE=1001;
	int SUCCESS_CODE=1000;
	String USER_EXIST_ERROR="user.exist.error";
	String EMAIL_SEND_SUCCESS="registration.email.sent.success";
	String INVALID_VERIFICATION_TOKEN="invalid.email.token.error";
	
	
	

}
