package com.skypc.yoartist.constants;

public interface NotificationDescription {
	
	String CLIENT_SUBMIT_PROPOSAL_ON_OPEN_PROJECT = "<a href=\"#contextPath#/user/viewPublicProfile/#researcherId#\">#researcherName#</a> "
			+ "has submitted "
			+ "<a href=\"#contextPath#/user/client/reviewProposal/#proposalId#\">proposal</a> "
			+ "for project <a href=\"#contextPath#/user/projectDetails/#projectId#\">#projectName#</a>";
	
	String CLIENT_UPDATE_PROPOSAL_ON_OPEN_PROJECT = "<a href=\"#contextPath#/user/viewPublicProfile/#researcherId#\">#researcherName#</a> "
			+ "has updated "			
			+ "<a href=\"#contextPath#/user/client/reviewProposal/#proposalId#\">proposal</a> "
			+ "for project <a href=\"#contextPath#/user/projectDetails/#projectId#\">#projectName#</a>";
	String CLIENT_UPDATE_MILESTONE_ON_PROJECT="<a href=\"#contextPath#/user/viewPublicProfile/#researcherId#\">#researcherName#</a> "
			+ "has updated <a href=\"#contextPath#/user/projectDashboard/#projectId#?tabType=milestoneTab\">milestone</a> "
			+ "for project <a href=\"#contextPath#/user/projectDashboard/#projectId#\">#projectName#</a>";
	String CLIENT_DELETE_MILESTONE_ON_PROJECT="<a href=\"#contextPath#/user/viewPublicProfile/#researcherId#\">#researcherName#</a> "
			+ "has deleted <a href=\"#contextPath#/user/projectDashboard/#projectId#?tabType=milestoneTab\">milestone</a> "
			+ "for project <a href=\"#contextPath#/user/projectDashboard/#projectId#\">#projectName#</a>";
	
	String CLIENT_DISPUTE_RAISED_ON_PROJECT="<a href=\"#contextPath#/user/viewPublicProfile/#researcherId#\">#researcherName#</a> "
			+ "has raised dispute "
			+ "for project <a href=\"#contextPath#/user/projectDashboard/#projectId#\">#projectName#</a>";
	String CLIENT_RATING_GIVEN_ON_PROJECT="<a href=\"#contextPath#/user/viewPublicProfile/#researcherId#\">#researcherName#</a> "
			+ "has given rating "
			+ "for project <a href=\"#contextPath#/user/projectDashboard/#projectId#\">#projectName#</a>"; 
	
	String CLIENT_DECLINE_INVITATION_ON_PROJECT="<a href=\"#contextPath#/user/viewPublicProfile/#researcherId#\">#researcherName#</a> "
			+ "has declined invitation "
			+ "for project <a href=\"#contextPath#/user/projectDetails/#projectId#\">#projectName#</a>";
	
	/**researcher notifications**/
	String RESEARCHER_INVITATION="<a href=\"#contextPath#/user/viewPublicProfile/#clientId#\">#clientName#</a> "
			+ "has invited you "
			+ "for project <a href=\"#contextPath#/user/projectDetails/#projectId#\">#projectName#</a>";
	String RESEARCHER_EDITED_OPEN_PROJECT_BY_CLIENT="<a href=\"#contextPath#/user/viewPublicProfile/#clientId#\">#clientName#</a> "
			+ "has updated "			
			+ "project <a href=\"#contextPath#/user/projectDetails/#projectId#\">#projectName#</a>";
	
	String RESEARCHER_AWARDED_PROJECT_BY_CLIENT="<a href=\"#contextPath#/user/viewPublicProfile/#clientId#\">#clientName#</a> "
			+ "has awarded "			
			+ "project <a href=\"#contextPath#/user/projectDashboard/#projectId#\">#projectName#</a> to you";	
	String RESEARCHER_AWARDED_PROJECT_TO_OTHER="project <a href=\"#contextPath#/user/projectDetails/#projectId#\">#projectName#</a>"
			+ " is awarded to other researcher";
	String RESEARCHER_CANCELLED_PROJECT_ON_SUBMITTED_PROPOSAL="<a href=\"#contextPath#/user/viewPublicProfile/#clientId#\">#clientName#</a>"
			+ " has cancelled project <a href=\"#contextPath#/user/projectDetails/#projectId#\">#projectName#</a> "
			+ "on which you have submitted <a href=\"#contextPath#/user/client/reviewProposal/#proposalId#\">proposal</a>";
	
	String RESEARCHER_CANCELLED_ON_ASSIGN_PROJECT_BY_CLIENT="client <a href=\"#contextPath#/user/viewPublicProfile/#clientId#\">#clientName#</a>"
			+ " has cancelled project <a href=\"#contextPath#/user/projectDashboard/#projectId#\">#projectName#</a> on which your are working";
	String RESEARCHER_DISPUTED_RAISED_ON_ASSIGN_PROJECT_BY_CLIENT="client <a href=\"#contextPath#/user/viewPublicProfile/#clientId#\">#clientName#</a>"
			+ " has raised dispute for project <a href=\"#contextPath#/user/projectDashboard/#projectId#\">#projectName#</a>";
	
	String RESEARCHER_REVIEW_AND_RATING_BY_CLIENT="<a href=\"#contextPath#/user/viewPublicProfile/#clientId#\">#clientName#</a> "
			+ "has given rating "
			+ "for project <a href=\"#contextPath#/user/projectDashboard/#projectId#\">#projectName#</a>";
	
	String RESEARCHER_DECLINE_PROPOSAL_ON_OPEN_PROJECT = "<a href=\"#contextPath#/user/viewPublicProfile/#clientId#\">#clientName#</a> "
			+ "has declined your "
			+ "<a href=\"#contextPath#/user/researcher/viewProposalPage?proposalId=#proposalId#\">proposal</a> "
			+ "for project <a href=\"#contextPath#/user/projectDetails/#projectId#\">#projectName#</a>";
	
	String RESEARCHER_SHORTLISTED_PROPOSAL_ON_OPEN_PROJECT = "<a href=\"#contextPath#/user/viewPublicProfile/#clientId#\">#clientName#</a> "
			+ "has shortlisted your "
			+ "<a href=\"#contextPath#/user/researcher/viewProposalPage?proposalId=#proposalId#\">proposal</a> "
			+ "for project <a href=\"#contextPath#/user/projectDetails/#projectId#\">#projectName#</a>";
	
	String RESEARCHER_WITHDRAW_SHORTLISTED_PROPOSAL_ON_OPEN_PROJECT = "<a href=\"#contextPath#/user/viewPublicProfile/#clientId#\">#clientName#</a> "
			+ "has remove your "
			+ "<a href=\"#contextPath#/user/researcher/viewProposalPage?proposalId=#proposalId#\">proposal</a> from his shortlist, "
			+ "for project <a href=\"#contextPath#/user/projectDetails/#projectId#\">#projectName#</a>";
	
}
