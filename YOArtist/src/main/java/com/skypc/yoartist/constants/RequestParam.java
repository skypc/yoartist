/*
 *
 */
package com.skypc.yoartist.constants;

public interface RequestParam {

	public static final String FULL_NAME = "fullName";

	public static final String REGISTER_EMAIL = "email";
	
	public static final String LOGIN_EMAIL = "loginEmail";
	
	public static final String REGISTER_PASSWORD = "password";
	
	public static final String LOGIN_PASSWORD = "loginPassword";
	
	public static final String USER_ADDRESS = "address";

	public static final String USER_STATE = "state";

	public static final String USER_COUNTRY = "country";

	public static final String USER_CONTACT_NUMBER = "contactNumber";
	
	public static final String USER_PHONE_CODE = "phoneCode";

	public static final String USER_ID = "userId";

	public static final String PROFILE_IMAGE = "profileImg";

	public static final String DEREE = "degree";

	public static final String DEGREE_NAME = "degreeName";

	public static final String UNIVERSITY = "university";

	public static final String EDUCATION_START_DATE = "startDate";

	public static final String EDUCATION_END_DATE = "endDate";

	public static final String EDUCATION_STATE = "state";

	public static final String EDUCATION_COUNTRY = "country";

	public static final String ID = "id";

	public static final String EDUCATION_DETAILS_ID = "educationDetailId";

	public static final String OTHER_DEGREE = "otherDegree";

	public static final String OTHER_DEGREE_NAME = "otherDegreeName";

	public static final String OTHER_UNIVERSITY = "otherUniversity";

	public static final String ORG_NAME = "orgName";
	
	public static final String JOB_POSITION = "jobPosition";

	public static final String HIGHEST_DEGREE = "highestDegree";

	public static final String CURRENTLY_WORKED_HERE = "isCurrentlyWorking";

	public static final String EMPLOYMENT_DETAILS_ID = "employeeDetailsId";

	public static final String WORK_CATEGORY = "workCatagory";

	public static final String WORK_TYPE = "workType";

	public static final String TITLE = "title";

	public static final String JOURNAL_TITLE = "journalTitle";

	public static final String OTHER_CONTRIBUTOR_NAMES = "otherContributorNames";

	public static final String URL = "url";

	public static final String COUNTRY = "country";

	public static final String RELATION_TYPE = "relationType";

	public static final String SHORT_DESCRIPTION = "shortDesc";

	public static final String PUBLICATION_DATE = "publicationDate";

	public static final String PUBLICATION_ID = "publicationId";

	public static final String LINKEDIN_USER_ID = "linkedInUserId";

	public static final String INVITED_BY = "invitedBy";

	public static final String INVITED_UUID = "invitedUUID";

	public static final String POST_PROPOSAL_TAB = "PostProposalTab";

	public static final String POST_PROJECT_ACTION = "PROJECT_ACTION";

	public static final String POST_PROJECT_TAB = "postProjectTab";

	public static final String POST_PROJECT_JOB_TYPE = "jobType";

	public static final String POST_PROJECT_VO = "postProjectVO";

	public static final String SUBMIT_PROPOSAL_ID = "proposalId";

	public static final String AWARDED_PROJ_PROP_MAP_ID = "awardedProjPropMapId";

	public static final String POST_PROJECT_ID = "postProjectId";

	public static final String POST_PROJECT_LOGO = "postProjectLogo";

	public static final String POST_PROJECT_FILES = "attachments[]";

	public static final String POST_PROJECT_PROPOSALS = "postProjectProposals";

	public static final String POST_PROJECT_INVITEES = "postProjectInvitees";

	public static final String POST_PROJECT_SELECTED_INVITEES = "postProjectSelectedInvitees";

	public static final String POST_PROJECT_STATUS = "postProjectStatus";

	public static final String POST_PROJECT_STATUS_DESC = "postProjectStatusDesc";

	public static final String POST_PROJECT_RATING = "postProjectRating";

	public static final String USER_IDS = "userIds";

	public static final String FILE_ID = "fileId";

	public static final String FILE_TYPE = "fileType";

	public static final String FILE = "file";

	public static final String MSG_PROJ_ID = "postProjectDashId";
	public static final String MSG_TYPE = "messageTypeStr";
	public static final String MSG_STR = "messageStr";
	public static final String PROJ_MESSAGE_ID = "projectMessageId";

	public static final String PROJ_PROP_SUBMITTED_USER_ID = "propSubmittedUserId";
}
