package com.skypc.yoartist.constants;

public interface ActivityConstant {

	String CONNECTION_ACCEPTED="<a href=\"#contextPath#/user/profile/#userId1#\">#user_name1#</a> is connected with <a href=\"#contextPath#/user/profile/#userId2#\">#user_name2#</a>";
	String POST_LIKE="<a href=\"#contextPath#/user/profile/#userId#\">#user_name#</a> has like  <a href=\"#contextPath#/user/post/#postId#\">post</a>";
	String POST_UNLIKE="<a href=\"#contextPath#/user/profile/#userId#\">#user_name#</a> has unlike  <a href=\"#contextPath#/user/post/#postId#\">post</a>";
	String POST_COMMENTS="<a href=\"#contextPath#/user/profile/#userId#\">#user_name#</a> has comments on <a href=\"#contextPath#/user/post/#postId#\">post</a>";
}
