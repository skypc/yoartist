package com.skypc.yoartist.constants;

public interface CalendarConstants {

	int JANUARY = 0;
	int FEBRUARY = 1;
	int MARCH = 2;
	int APRIL = 3;
	int MAY = 4;
	int JUNE = 5;
	int JULY = 6;
	int AUGUST = 7;
	int SEPTEMBER = 8;
	int OCTOBER = 9;
	int NOVEMBER = 10;
	int DECEMBER = 11;
	
	String JANUARY_MONTH = "January";
	String FEBRUARY_MONTH = "February";
	String MARCH_MONTH = "March";
	String APRIL_MONTH = "April";
	String MAY_MONTH = "May";
	String JUNE_MONTH = "June";
	String JULY_MONTH = "July";
	String AUGUST_MONTH = "August";
	String SEPTEMBER_MONTH = "September";
	String OCTOBER_MONTH = "October";
	String NOVEMBER_MONTH = "November";
	String DECEMBER_MONTH = "December";
}
