package com.skypc.yoartist.constants;

public interface NotificationConstant {

	String SEND_CONNECT_REQUEST="Added you as a connection";
	String ACCEPT_CONNECT_REQUEST="Accepted your connection request";
	String POST_LIKE="like your <a href=\"#contextPath#/user/post/#postId#\">post</a>";
	String POST_COMMENTS="comments on your <a href=\"#contextPath#/user/post/#postId#\">post</a>";
	String POST_UN_LIKE="unlike your <a href=\"#contextPath#/user/post/#postId#\">post</a>";
	String CONNECT_REQUEST_LINK="/user/profile/#userId#";
}
