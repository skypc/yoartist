package com.skypc.yoartist.constants;

public class YOATables {
	
	public static final String TABLE_USER="user";
	public static final String TABLE_ORGANISATION="organisation";
	public static final String TABLE_EDUCATIONAL_DETAILS="education_details";
	public static final String TABLE_EMPLOYMENT_DETAILS="employment_details";
	public static final String TABLE_PUBLICATION_DETAILS="publication_details";
	public static final String TABLE_BLOCKED_DOMAIN="blocked_domain";
	public static final String TABLE_COUNTRY="country";
	public static final String TABLE_STATE="state";
	public static final String TABLE_ADMIN_INVITED_USER="admin_invited_user";
	public static final String TABLE_POST_PROJECT = "post_project";	
	public static final String TABLE_PROJECT_DESCRIPTION = "project_description";
	public static final String TABLE_PROJECT_ATTACHMENT = "project_attachment";
	public static final String TABLE_DELIVERABLES_OUTCOMES = "deliverables_outcomes";
	public static final String TABLE_EVALUATION_CRITERIA = "evaluation_criteria";
	public static final String TABLE_GENERAL_TERMS = "general_terms";
	public static final String TABLE_SPECIAL_TERMS = "special_terms";
	public static final String TABLE_TRANSACTION = "transaction";
	public static final String TABLE_MESSAGES = "messages";
	public static final String TABLE_MESSAGES_ATTACHMENTS = "messages_attachments";
	public static final String TABLE_MILESTONE_DETAILS = "milestone_details";
	public static final String TABLE_AWARDED_PROJECTS = "awarded_projects";
	public static final String TABLE_SUBMIT_PROPOSAL = "submit_proposal";
	public static final String TABLE_TECHNICAL_BACKGROUND = "technical_background";
	public static final String TABLE_NOTIFICATION = "notification";
	public static final String TABLE_USER_ACTIVITY = "user_activity";
	public static final String TABLE_WATCH_LIST= "watch_list";
	public static final String TABLE_PROJECT_INVITES = "project_invites";
	public static final String TABLE_PROJECTS_KEYWORDS = "projects_keywords";
	public static final String TABLE_PROJECT_CATAGORY = "project_catagory";
	public static final String TABLE_PROJECT_SUB_CATAGORY = "project_sub_catagory";
	public static final String TABLE_KEYWORD = "keyword";
	public static final String PROJECT_LOG = "project_log";
}
