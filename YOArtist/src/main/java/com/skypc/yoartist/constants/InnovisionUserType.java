/*
 * User Type Constants - client, researcher and guest
 */
package com.skypc.yoartist.constants;

public interface InnovisionUserType {

	/** User type - client */
	String CLIENT = "client";

	/** User type - researcher */
	String RESEARCHER = "researcher";

	/** User type - guest */
	String GUEST = "guest";

}
