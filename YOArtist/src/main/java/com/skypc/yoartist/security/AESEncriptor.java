package com.skypc.yoartist.security;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class AESEncriptor {

	private static byte[] key = {
	        0x74, 0x68, 0x69, 0x73, 0x49, 0x73, 0x41, 0x53, 0x65, 0x63, 0x72, 0x65, 0x74, 0x4b, 0x65, 0x79
	    };//"thisIsASecretKey";
	   	private static final String keySring = "$@|3$@&#$^$%*#$%";
	   	

	    static {
	        key = keySring.getBytes();
	    }

	    /**
	     * @param args the command line arguments
	     */
	    public static void main(String args[]) {

	        try {

	            System.out.println("input parameter :" + "admin123");
	            final String encryptedStr = AESEncriptor.encrypt("admin123");
	            System.out.println("Encrypted : " + encryptedStr);
	            final String decryptedStr = AESEncriptor.decrypt(encryptedStr);
	            System.out.println("Decrypted : " + decryptedStr);

	        } catch (Exception e) {
	            e.printStackTrace();
	        }

	    }
	    public static String encrypt(String strToEncrypt) {
	        try {
	        	
	            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
	            final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
	            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
	            final String encryptedString = Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes()));
	            return encryptedString;
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return null;

	    }

	    public static String decrypt(String strToDecrypt) {
	        try {
	            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
	            final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
	            cipher.init(Cipher.DECRYPT_MODE, secretKey);
	            final String decryptedString = new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
	            return decryptedString;
	        } catch (Exception e) {
	            e.printStackTrace();

	        }
	        return null;
	    }

	    public static String getMintVerification(String message, String sharedSecrete) {
	        StringBuilder hash = new StringBuilder();
	        try {
	            final SecretKeySpec secretKey = new SecretKeySpec(sharedSecrete.getBytes("UTF-8"), "HmacMD5");
	            Mac mac = Mac.getInstance("HmacMD5");
	            mac.init(secretKey);
	            byte[] result = mac.doFinal(message.getBytes("ASCII"));
	            for (int i = 0; i < result.length; i++) {
	                String hex = Integer.toHexString(0xFF & result[i]);
	                if (hex.length() == 1) {
	                    hash.append('0');
	                }
	                hash.append(hex);
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return hash.toString();
	    }
}
