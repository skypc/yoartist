package com.skypc.yoartist.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.skypc.yoartist.constants.YOAConstants;

public class DateUtil {

	/**
	 * 
	 * @param date1
	 * @param date2
	 * @param timeUnit
	 * @return
	 */
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
	    long diffInMillies = date2.getTime() - date1.getTime();
	    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}
	
	public static String getNotificationMessageDate(Date date){
		Date todaysDate = new Date();
		String result="";
		if(todaysDate.compareTo(date)>0){
			long diff=todaysDate.getTime()-date.getTime();
			int hours=(int)diff/(1000*60*60);
			int mins=(int)diff/(1000*60);
			int sec=(int)diff/(1000);
			if(hours>24){
				result=formatDateToString(date, "dd-MMMM-yyyy");
			}
			else if(hours>0){
				result=hours+" hours ago";
			}
			else if(mins>0)
				result=mins+" minutes ago";
			else if(sec>0){
				result=sec+" seconds ago";
			}
		}
		return result;
	}
	
	/**
	 * Format date to date.
	 *
	 * @param date the date
	 * @param formatStyle the format style
	 * @return the date
	 */
	public static Date formatDateToDate(String date, String formatStyle) {
        Date formattedDate = null;
        if (date != null) {
            SimpleDateFormat format = new SimpleDateFormat(formatStyle);
            try {
				formattedDate = format.parse(date);
			} catch (ParseException e) {
				System.out.println(e.getMessage());
			}
        }
        return formattedDate;
    }
	
	/**
	 * Format date to string.
	 *
	 * @param date the date
	 * @param formatStyle the format style
	 * @return the string
	 */
	public static String formatDateToString(Date date, String formatStyle) {
        String formattedDate = null;
        if (date != null) {
            SimpleDateFormat format = new SimpleDateFormat(formatStyle);
            formattedDate = format.format(date);
        }
        return formattedDate;
    }
	
	public static String getDateASTimeAgo(Date date) {
		String time = "";
		if (date != null) {
			Calendar todayCal = Calendar.getInstance();
			long diff = todayCal.getTime().getTime() - date.getTime();
			int day = (int) (diff / (1000 * 60 * 60 * 24));
			if (day > 0) {
				if (day > 15) {
					time = DateUtil.formatDateToString(date, YOAConstants.DATE_TIME_FORMAT);
				}
				time = day + " day's ago";
			} else {
				int hour = (int) diff / (1000 * 60 * 60);
				if (hour > 0) {
					time = hour + " hour's ago";
				} else {
					int min = (int) diff / (1000 * 60);
					if (min > 0) {
						time = min + " minute's ago";
					} else {
						int sec = (int) diff / (1000);
						time = sec + " second's ago";
					}
				}
			}
		}
		return time;
	}

	public static String dateDiffInYearAndMonth(Date startDate, Date endDate){
		StringBuilder builder=new StringBuilder();
		long diff=endDate.getTime()-startDate.getTime();
		int day = (int) (diff / (1000 * 60 * 60 * 24));
		int years=day/365;
		if(years>0){
			builder.append(years+" years ");
			int remainingDays=day%365;
			int month= remainingDays/30;
			if(month>0){
				builder.append(month+" months");
			}
		}else{
			int month= day/30;
			if(month>0){
				builder.append(month+" months");
			}
		}
		
		return builder.toString();
	}
}
