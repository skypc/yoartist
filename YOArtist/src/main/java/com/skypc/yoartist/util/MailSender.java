package com.skypc.yoartist.util;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;

import com.skypc.yoartist.mail.YOAMailApi;

public class MailSender extends Thread {
	private String recepient;
	private String from;
	private String password;
	private String body;
	private String subject;
	private YOAMailApi api;
	public MailSender(String from, String password,String recepient,String subject, String body, YOAMailApi api) {
		super();
		this.recepient = recepient;
		this.from = from;
		this.password=password;
		this.body = body;
		this.subject = subject;
		this.api=api;
	}
	@Override
	public void run() {
		try {
			api.sendMail(recepient, from, password,subject, body);
		} catch (MessagingException e) {
			System.out.println(e.getMessage());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
}
