package com.skypc.yoartist.util;

import java.io.InputStream;
import java.util.Properties;

public class PropertyUtil {

	 private static final String CLASS_NAME = PropertyUtil.class.getName();
	    

	    private PropertyUtil() {
	    }
	    
	    
	    public static Properties getProperties(String fileName) {
	        InputStream addrStream = null;
	        Properties properties = null;
	        try {
	            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
	            addrStream=classLoader.getResourceAsStream(fileName + ".properties");
	            if(addrStream!=null){
	            	properties = new Properties();
	            	properties.load(addrStream);
	            }

	        } catch (Exception e) {
	            //XeroLogger.error(CLASS_NAME, "Error in PropertyUtility Static block : ", e, true);
	        } finally {
	            try {
	                if (addrStream != null) {
	                    addrStream.close();
	                }
	            } catch (Exception e) {
	                //XeroLogger.error(CLASS_NAME, "Static Block", "error in PropertyUtility finally block");
	            }
	        }
	        return properties;
	    }
}