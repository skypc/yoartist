/*
 *
 */
package com.skypc.yoartist.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skypc.yoartist.constants.CalendarConstants;
import com.skypc.yoartist.constants.InnovisionErrorCodes;
import com.skypc.yoartist.constants.InnovisionErrorMessage;
import com.skypc.yoartist.constants.YOAConstants;
import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.logger.YOALogger;
import com.skypc.yoartist.validator.Validator;

public class Util {

	static Map<Integer, String> months = new HashMap<Integer, String>();

	static {

		months.put(CalendarConstants.JANUARY, CalendarConstants.JANUARY_MONTH);
		months.put(CalendarConstants.FEBRUARY, CalendarConstants.FEBRUARY_MONTH);
		months.put(CalendarConstants.MARCH, CalendarConstants.MARCH_MONTH);
		months.put(CalendarConstants.APRIL, CalendarConstants.APRIL_MONTH);
		months.put(CalendarConstants.MAY, CalendarConstants.MAY_MONTH);
		months.put(CalendarConstants.JUNE, CalendarConstants.JUNE_MONTH);
		months.put(CalendarConstants.JULY, CalendarConstants.JULY_MONTH);
		months.put(CalendarConstants.AUGUST, CalendarConstants.AUGUST_MONTH);
		months.put(CalendarConstants.SEPTEMBER, CalendarConstants.SEPTEMBER_MONTH);
		months.put(CalendarConstants.OCTOBER, CalendarConstants.OCTOBER_MONTH);
		months.put(CalendarConstants.NOVEMBER, CalendarConstants.NOVEMBER_MONTH);
		months.put(CalendarConstants.DECEMBER, CalendarConstants.DECEMBER_MONTH);

	}
	
	@SuppressWarnings("unchecked")
	public static Object convertJsonToObject(String jsonData, Class cls) throws YOAException {
		YOALogger.debug("InnovisionUtil", "convertJsonToObject", "json conversion start ");
		Object obj = null;
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		try {
			obj = mapper.readValue(jsonData, cls);

		} catch (IOException e) {
			throw new YOAException(e.getMessage());
		}
		return obj;
	}

	public static boolean isValidUUID(String uuid, Date createDate) {
		if (StringUtils.isNotBlank(uuid)) {
			long createdDateTime = createDate.getTime();
			long todayDateTime = new Date().getTime();
			int hour = (int) ((todayDateTime - createdDateTime) / (1000 * 60 * 60));
			if (hour <= 24) {
				return true;
			}
		}
		return false;
	}

	/**
	 * upload user file to file system directory user id will get prepended to
	 * file e.g. 1234_uploadedFileName.ext
	 */
	public static String uploadFile(MultipartFile file, String rootPath) throws YOAException {
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				File dir = new File(rootPath);
				if (!dir.exists()) {
					dir.mkdirs();
				}

				String fileName = file.getOriginalFilename();
				String type = fileName.substring(fileName.lastIndexOf("."), fileName.length());
				String fName = fileName.substring(0, fileName.lastIndexOf("."));
				// Create the file on server
				String filePath = dir.getAbsolutePath() + File.separator + fName + "_"
						+ UUID.randomUUID().toString().replaceAll("-", "") + type;
				File serverFile = new File(filePath);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				YOALogger.info("UserViewHandler2", "FileUpload",
						"Server File Location=" + serverFile.getAbsolutePath());
				YOALogger.info("UserViewHandler2", "FileUpload",
						"You successfully uploaded file=" + file.getOriginalFilename());

				return filePath;
			} catch (Exception e) {
				YOALogger.error("UserViewHandler2", "FileUpload",
						"You failed to upload " + file.getOriginalFilename() + " => " + e.getMessage());
				throw new YOAException(e);
			}
		} else {
			YOALogger.warn("UserViewHandler2", "FileUpload",
					"You failed to upload " + file.getOriginalFilename() + " because the file was empty.");
			return null;
		}
	}

	/** check valid user session */
	public static long getSessionUserId(HttpServletRequest request) throws YOAException {
		Object sessionObj = request.getSession().getAttribute(YOAConstants.SESSION_USER_ID);
		if (sessionObj != null) {
			String sessionUserId = String.valueOf(sessionObj);
			if (!Validator.isSet(sessionUserId)) {
				throw new YOAException(String.valueOf(InnovisionErrorCodes.INVALID_REQUIRED_PARAM),
						InnovisionErrorMessage.INVALID_USER, new Exception(InnovisionErrorMessage.INVALID_USER));
			}
			return StringUtils.isBlank(sessionUserId) ? 0 : Long.valueOf(sessionUserId);
		} else {
			return 0;
		}
	}

	public static String getEncodedImage(byte[] photoImage) throws YOAException {
		String base64Image = null;
		if (photoImage != null) {
			byte[] encodeBase64 = Base64.encodeBase64(photoImage);
			try {
				base64Image = new String(encodeBase64, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				base64Image = null;
			}
		}
		return base64Image;
	}

	public static String getEmploymentDuration(Date startDate, Date endDate) {

		Calendar startDateCalendar = Calendar.getInstance();
		Calendar endDateCalendar = Calendar.getInstance();

		String startStr = "";
		String endStr = "Present";
		if (startDate != null) {
			startDateCalendar.setTime(startDate);
		}
		if (endDate != null) {
			endDateCalendar.setTime(endDate);
			endStr = months.get(endDateCalendar.get(Calendar.MONTH)) + " "
					+ String.valueOf(endDateCalendar.get(Calendar.YEAR));
		}

		startStr = months.get(startDateCalendar.get(Calendar.MONTH)) + " "
				+ String.valueOf(startDateCalendar.get(Calendar.YEAR));
		return startStr + " - " + endStr;
	}
}
