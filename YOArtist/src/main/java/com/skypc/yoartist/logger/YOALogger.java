package com.skypc.yoartist.logger;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class GenericLogger.
 */
public class YOALogger extends SecurityManager {

	/**
	 * Gets the logger.
	 *
	 * @param className the class name
	 * @return the logger
	 */
	private static Logger getLogger(String className) {
		return Logger.getLogger(className);
	}

	/**
	 * Check whether debug level enabled for this log category.
	 *
	 * @param className the class name
	 * @return boolean -is debug enabled
	 */
	public static final boolean isDebugEnabled(String className) {
		return getLogger(className).isDebugEnabled();
	}

	/**
	 * Check whether info level enabled for this log category.
	 *
	 * @param className the class name
	 * @return boolean -is info enabled
	 */
	public static final boolean isInfoEnabled(String className) {
		return getLogger(className).isInfoEnabled();
	}

	/**
	 * Check whether trace level enabled for this log category.
	 *
	 * @param className the class name
	 * @return boolean -is trace enabled
	 */
	public static final boolean isTraceEnabled(String className) {
		return getLogger(className).isTraceEnabled();
	}

	/**
	 * Check whether warn level enabled for this log category.
	 *
	 * @param className the class name
	 * @return boolean -is warn enabled
	 */
	public static final boolean isWarnEnabled(String className) {
		return getLogger(className).isEnabledFor(Level.WARN);
	}

	/**
	 * Check whether error level enabled for this log category.
	 *
	 * @param className the class name
	 * @return boolean -is error enabled
	 */
	public static final boolean isErrorEnabled(String className) {
		return getLogger(className).isEnabledFor(Level.ERROR);
	}

	/**
	 * Check whether fatal level enabled for this log category.
	 *
	 * @param className the class name
	 * @return boolean -is fatal enabled
	 */
	public static final boolean isFatalEnabled(String className) {
		return getLogger(className).isEnabledFor(Level.FATAL);
	}

	/**
	 * This method logs trace messages.
	 *
	 * @param categoryName the category name
	 * @param methodName the method name
	 * @param message the message
	 */
	public static final void trace(String categoryName, String methodName,
			Object message) {
		if (null != message) {
			Logger.getLogger(categoryName).trace(
					createMessage(methodName, message.toString()));
		}
	}

	/**
	 * This method logs debug messages.
	 *
	 * @param categoryName the category name
	 * @param methodName the method name
	 * @param message the message
	 */
	public static final void debug(String categoryName, String methodName,
			Object message) {
		if (null != message) {
			Logger.getLogger(categoryName).debug(
					createMessage(methodName, message.toString()));
		}
	}

	/**
	 * This method logs info messages.
	 *
	 * @param categoryName the category name
	 * @param methodName the method name
	 * @param message the message
	 */
	public static final void info(String categoryName, String methodName,
			Object message) {
		if (null != message) {
			Logger.getLogger(categoryName).info(
					createMessage(methodName, message.toString()));
		}
	}

	/**
	 * This method logs warn messages.
	 *
	 * @param categoryName the category name
	 * @param methodName the method name
	 * @param message the message
	 */
	public static final void warn(String categoryName, String methodName,
			Object message) {
		if (null != message) {
			Logger.getLogger(categoryName).warn(
					createMessage(methodName, message.toString()));
		}
	}

	/**
	 * This will write the error message with out stack trace. Use it for
	 * writing error messages
	 *
	 * @param categoryName the category name
	 * @param methodName the method name
	 * @param message the message
	 */
	public static final void error(String categoryName, String methodName,
			Object message) {
		error(categoryName, methodName, message, false);
	}

	/**
	 * This will write the exception message with stack trace. Use it for
	 * writing Exception stack trace to log file.
	 *
	 * @param categoryName the category name
	 * @param methodName the method name
	 * @param exception the exception
	 * @param needStackTrace the need stack trace
	 */
	public static final void error(String categoryName, String methodName,
			Object exception, boolean needStackTrace) {
		if (exception instanceof Exception) {
			Logger.getLogger(categoryName).error(
					createMessage(methodName, null), (Exception) exception);
		} else {
			if (null != exception) {
				Logger.getLogger(categoryName).error(
						createMessage(methodName, exception.toString()), null);
			}
		}
	}

	/**
	 * Creates the message.
	 *
	 * @param methodName the method name
	 * @param message the message
	 * @return the string
	 */
	private static String createMessage(String methodName, String message) {
		StringBuilder sb = new StringBuilder();
		if (!methodName.isEmpty()) {
			sb.append(methodName).append(":");
		}
		if (message != null) {
			sb.append(message);
		}
		return sb.toString();
	}

}
