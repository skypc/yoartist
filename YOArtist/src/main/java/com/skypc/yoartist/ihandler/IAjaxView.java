package com.skypc.yoartist.ihandler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.skypc.yoartist.bean.ConnectionActivity;
import com.skypc.yoartist.bean.CurrentLoginUser;
import com.skypc.yoartist.bean.EducationBean;
import com.skypc.yoartist.bean.EmploymentBean;
import com.skypc.yoartist.bean.MessageUserBean;
import com.skypc.yoartist.bean.MessageUserChatsBean;
import com.skypc.yoartist.bean.NotificationBean;
import com.skypc.yoartist.bean.ResponseBean;
import com.skypc.yoartist.bean.SummaryBean;
import com.skypc.yoartist.bean.UserToConnectBean;
import com.skypc.yoartist.exception.YOAException;

public interface IAjaxView {
	
	public String uploadUserPost(HttpServletRequest request,MultipartFile multipartFile)throws YOAException;

	public List<UserToConnectBean> usersToConnect(HttpServletRequest request)throws YOAException;
	
	public List<UserToConnectBean> usersToAutoSearchBox(HttpServletRequest request)throws YOAException;

	public String commentOnPost(HttpServletRequest request, long postId, String comment)throws YOAException;

	public String sendConnectRequest(HttpServletRequest request, long userId) throws YOAException;
	
	public String acceptConnectRequest(HttpServletRequest request, long userId, boolean isAccept) throws YOAException;

	public String sendMessageToUser(HttpServletRequest request, long userId, String message)throws YOAException;

	public List<NotificationBean> getNotifications(HttpServletRequest request, boolean isLimited)throws YOAException;

	public int getNotificationOrConnectionCount(HttpServletRequest request, boolean b)throws YOAException;

	public List<NotificationBean> getAllConnectRequest(HttpServletRequest request)throws YOAException;

	public List<MessageUserBean> getMessageUsers(HttpServletRequest request) throws YOAException;

	public List<MessageUserChatsBean> getMessageUsersChat(HttpServletRequest request, long userId)throws YOAException;

	public CurrentLoginUser getCurrentLoginUser(HttpServletRequest request)throws YOAException;

	public String likePost(HttpServletRequest request, long postId)throws YOAException;

	public List<UserToConnectBean> connectedUser(HttpServletRequest request) throws YOAException;

	public List<ConnectionActivity> getConnectionActivity(HttpServletRequest request, boolean isFirst) throws YOAException;

	public ResponseBean getUserConnection(HttpServletRequest request) throws YOAException;

	public ResponseBean sharePost(HttpServletRequest request) throws YOAException;

	public ResponseBean getSessionUserProfile(HttpServletRequest request)throws YOAException;
	
	public ResponseBean saveEducationDetails(HttpServletRequest request,EducationBean education)throws YOAException;
	
	public ResponseBean updateEducationDetails(HttpServletRequest request,EducationBean education,long id)throws YOAException;
	
	public ResponseBean saveEmploymentDetails(EmploymentBean employment, HttpServletRequest request)throws YOAException;

	public ResponseBean getEmpData(HttpServletRequest request)throws YOAException;

	public ResponseBean deleteEmpData(long id, HttpServletRequest request)throws YOAException;

	public ResponseBean updateEmploymentDetails(EmploymentBean employment, HttpServletRequest request) throws YOAException;

	public ResponseBean saveSummaryDetails(SummaryBean summary, HttpServletRequest request) throws YOAException;

	public ResponseBean getSummaryData(HttpServletRequest request) throws YOAException;

	public ResponseBean deleteSummaryData(long id, HttpServletRequest request) throws YOAException;

	public ResponseBean updateSummaryDetails(SummaryBean summary, HttpServletRequest request) throws YOAException;
}
