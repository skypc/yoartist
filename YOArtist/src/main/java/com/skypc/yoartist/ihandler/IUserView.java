package com.skypc.yoartist.ihandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.skypc.yoartist.exception.YOAException;

public interface IUserView {

	ModelAndView registerUser(HttpServletRequest request, RedirectAttributes attr)throws YOAException;

	ModelAndView signInAccount(HttpServletRequest request, RedirectAttributes attr,HttpServletResponse response)throws YOAException;

	ModelAndView homePage(HttpServletRequest request, RedirectAttributes attr)throws YOAException;

	ModelAndView confirmEmail(String token, HttpServletRequest request, RedirectAttributes attr)throws YOAException;

	ModelAndView submitPost(String comments, Integer uploadedId, HttpServletRequest request, RedirectAttributes attr)throws YOAException;

	ModelAndView sharePage(HttpServletRequest request, RedirectAttributes attr)throws YOAException;
}
