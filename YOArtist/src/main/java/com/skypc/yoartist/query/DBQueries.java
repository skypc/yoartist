/*
 *
 */
package com.skypc.yoartist.query;

import com.skypc.yoartist.enums.EnumConstants.ConnectStatus;
import com.skypc.yoartist.enums.UserPostStatus;
import com.skypc.yoartist.enums.UserStatus;

public class DBQueries {
	public static final String GET_ID_BY_EMAIL_AND_STATUS="select id from User where status=:status and email=:email";
	
	public static final String GET_CURRENT_USERS_MIN_INFO="select u.id,u.profilePhoto,u.firstName, u.lastName from User u where u.id=:currentUserId and u.status='"+UserStatus.ACTIVE.name()+"'";
	
	public static final String GET_USER_POST_FOR_HOME_PAGE="select u.id, u.profilePhoto, p.id, p.postDescription, p.fileName, p.filePath, p.modifiedDate, concat(u.firstName, ' ', u.lastName) as fullName from User u inner join u.userPosts p where p.status=:status order by p.modifiedDate desc limit 0,15";
	
	public static final String GET_COMMENT_FOR_POST="select  c.user.id, c.user.profilePhoto,c.user.firstName, c.user.lastName, c.id, c.comments,c.modifiedDate from PostComments c where c.status='"+UserPostStatus.POSTED.name()+"' and c.post.id=:postId";
	
	public static final String GET_COUNT_LIKES_FOR_POST="select count(*) from LikeVO l where l.post.id=:postId";
	
	public static final String GET_IS_LIKES_FOR_POST="select l.id from LikeVO l where l.post.id=:postId and l.user.id=:userId";
	
	public static final String GET_USERS_CONNECTIONS="select connectId from Connection c where c.userId=:userId";
	
	public static final String GET_USERS_FOR_CONNECTIONS_BLOCK="select u.id,u.profilePhoto,u.firstName, u.lastName from User u where u.id!=:currentUserId and u.id in (select distinct connectId from Connection where userId in(select connectId from Connection where userId=:currentUserId) and connectId!=:currentUserId) and u.status='"+UserStatus.ACTIVE.name()+"'";
	
	public static final String GET_USERS_FOR_CONNECTIONS_IN_SEARCH_BLOCK="select u.id,u.profilePhoto,u.firstName, u.lastName from User u where u.id!=:currentUserId and u.status='"+UserStatus.ACTIVE.name()+"'";
	
	public static final String GET_USERS_FOR_CONNECTIONS_BLOCK_IN_CLAUSE=" AND id not in (:in_clause_long_connectionIds)";
	
	public static final String GET_NOTIFICATION_COUNT="select count(*) from NotificationVO n where n.toUser.id=:userId AND n.isRead="+Boolean.FALSE;
	
	public static final String GET_CONNECT_REQUEST_COUNT="select count(*) from Connection c where c.connectId=:userId AND c.status='"+ConnectStatus.PENDING.name()+"' AND c.isRead="+Boolean.FALSE;
	
	public static final String MARK_READ_NOTIFICATION="update NotificationVO set isRead="+Boolean.TRUE+" where toUser.id=:userId";
	
	public static final String MARK_READ_MESSAGES="update MessagesVO set isRead="+Boolean.TRUE+" where destinationUser.id=:userId";
	
	public static final String MARK_READ_CONNECTIONS="update Connection set isRead="+Boolean.TRUE+" where connectId=:userId";
	
	public static final String UPDATE_CONNECTION_WITH_STATUS="update Connection c set c.status=:status where c.userId=:userId AND c.connectId=:connectId";
	
	public static final String GET_ALL_PENDING_CONNECTIONS="select c.userId, u.firstName,u.lastName,u.profilePhoto,c.modifiedDate from Connection c, User u where u.id=c.userId AND c.connectId=:sessionUserId AND c.status='"+ConnectStatus.PENDING.name()+"' order by c.modifiedDate desc";

	public static final String GET_ALL_ACCEPTED_CONNECTIONS="select u.id, u.firstName,u.lastName,u.profilePhoto,c.modifiedDate from Connection c, User u where u.id=c.connectId AND c.userId=:sessionUserId AND c.status='"+ConnectStatus.ACCEPTED.name()+"' order by c.modifiedDate desc";
	
	public static final String GET_USER_CONVERSATION="from MessagesVO m where (m.sourceUser.id=:sessionId OR m.sourceUser.id=:userId) AND (m.destinationUser.id=:userId OR m.destinationUser.id=:sessionId) order by m.modifiedDateTime asc";
	
	public static final String GET_CURRENT_LOGIN_USER="select u.id, u.firstName,u.lastName,u.profilePhoto from User u where u.id=:sessionId";
	
	public static final String GET_USER_ID_FROM_POST_ID="select user.id from UserPosts where id=:postId";

	public static final String GET_ACTIVE_USER_CONNECTIONS="select connectId from Connection c where c.userId=:userId and c.status=:status";
	
	public static final String GET_USER_ACTIVITY_DATA="select u.user_id,u.second_user_id, u.description,type,u.modified_date_time,u.post_id,u.id from user_activity u join connection c on c.connect_id=u.user_id where c.user_id=:userId order by u.modified_date_time desc";
	
	public static final String GET_USER_ACTIVITY_ISNOTREAD_DATA="select u.user_id,u.second_user_id, u.description,type,u.modified_date_time,u.post_id,u.id from user_activity u join connection c on c.connect_id=u.user_id where u.is_read=0 and c.user_id=:userId order by u.modified_date_time desc";

	public static final String MARK_READ_USERACTIVITY="update UserActivityVO set isRead="+Boolean.TRUE+" where id in (:in_clause_long_actIds)";
	
	public static final String CHECK_IS_POST_BY_USER="select id from UserPosts where user.id=:userId and id=:postId";

	public static final String GET_USER_CONNECTION = "select u from User u where u.id in ("
			+ "select distinct c.connectId from Connection c where c.userId=:userId and c.status=:status)"
			+ " order by u.firstName,u.lastName";

	public static final String GET_ALL_ACTIVE_USER_ID_EXCEPT_SESSION_USER_ID = "select distinct u.id from User u where u.id!=:sessionUserId and u.status=:status";
}

