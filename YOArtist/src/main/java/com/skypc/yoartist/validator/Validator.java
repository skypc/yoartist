/*
 *
 */
package com.skypc.yoartist.validator;

import java.util.List;

public class Validator {

	public static Boolean validateAllMandatoryParameters(Object... parameters) {
		Boolean isValid = Boolean.TRUE;
		for (Object object : parameters) {
			if (object != null) {
				if (object instanceof String) {
					String val = (String) object;
					if (val.trim().isEmpty()) {
						isValid = Boolean.FALSE;
					}
				} else if (object instanceof Integer) {
					int val = (Integer) object;
					if (val < 1) {
						isValid = Boolean.FALSE;
					}
				} else if (object instanceof List) {
					@SuppressWarnings("rawtypes")
					List val = (List) object;
					if (val.isEmpty()) {
						isValid = Boolean.FALSE;
					}
				}

			} else {
				isValid = Boolean.FALSE;
			}
		}
		return isValid;
	}

	public static boolean isSet(Object value){
		return (value !=null && value.toString().length()!=0);
	}

}
