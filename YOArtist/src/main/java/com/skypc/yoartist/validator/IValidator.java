package com.skypc.yoartist.validator;

import com.skypc.yoartist.exception.YOAException;

// TODO: Auto-generated Javadoc
/**
 * The Interface IValidator.
 */
public interface IValidator {

	/**
	 * Validate.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 * @throws YOAException the innovision exception
	 */
	boolean validate(Object... objArr)throws YOAException;
}
