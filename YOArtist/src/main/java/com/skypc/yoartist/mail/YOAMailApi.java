package com.skypc.yoartist.mail;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

public class YOAMailApi {

	@Autowired
	private JavaMailSenderImpl mailSender;
	
	public void sendMail(String to,String from, String password,String subject, String body) throws MessagingException, UnsupportedEncodingException{
		MimeMessage mime = this.mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mime, true);
			
		helper.setTo(to);
		helper.setFrom(new InternetAddress(from, "YOArtist"));
		helper.setSubject(subject);
		helper.setText(body,true);
		mime.setContent(body, "text/html");
		mailSender.setUsername(from);
		mailSender.setPassword(password);
		mailSender.send(mime);
		
	}
}
