package com.skypc.yoartist.social;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.JSONObject;

import com.skypc.yoartist.constants.YOAConstants;
import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.logger.YOALogger;
import com.skypc.yoartist.util.PropertyUtil;

public class LinkedInExecutor {
	
	static Properties properties = null;
	static String clientKey;
	static String clientSecret;
	static String callBackURL;
	static String authURL;
	static String responseType = "code";
	//static String state = "786MUSA786";
	static String accessToken=null;

	static {
		properties = PropertyUtil.getProperties("social");
		if (properties != null) {
			clientKey = properties.getProperty(YOAConstants.LINKED_IN_CLEINT_KEY);
			clientSecret = properties.getProperty(YOAConstants.LINKED_IN_CLEINT_SECRET);
			callBackURL = properties.getProperty(YOAConstants.LINKED_IN_CALL_BACK_URL);
			authURL = properties.getProperty(YOAConstants.LINKED_IN_AUTH_URL);
		}
	}

	public static String authorizeLinkedIn(HttpServletRequest request,String state) {
		String authFullUrl = "";
		if(authURL!=null && !authURL.isEmpty() && responseType!=null && !responseType.isEmpty() && clientKey!=null && !clientKey.isEmpty() && callBackURL!=null && !callBackURL.isEmpty() && state!=null && !state.isEmpty())
			authFullUrl = authURL + "?" + "response_type=" + responseType + "&" + "client_id=" + clientKey + "&"
				+ "redirect_uri=" + callBackURL + "&" + "state=" + state;
		System.out.println(authFullUrl);
		return authFullUrl;
	}
	
	public static String createAccessToken(HttpServletRequest request,HttpServletResponse response)throws YOAException{
		String code=request.getParameter("code");
		String state=request.getParameter("state");
		
		HttpClient httpClient = new HttpClient();
		PostMethod postRequest = new PostMethod("https://www.linkedin.com/oauth/v2/accessToken");
		postRequest.addParameter("grant_type", "authorization_code");
		postRequest.addParameter("code", code);
		postRequest.addParameter("state", state);
		postRequest.addParameter("redirect_uri", callBackURL);
		postRequest.addParameter("client_id", clientKey);
		postRequest.addParameter("client_secret", clientSecret);
		
		try {
			httpClient.executeMethod(postRequest);
			String accessTokenJson = postRequest.getResponseBodyAsString();
			System.out.println(accessTokenJson);
			
			
				JSONObject jsonObject = new JSONObject(accessTokenJson);
				if(!jsonObject.isNull(YOAConstants.LINKED_IN_ACCESS_TOKEN))
					accessToken = jsonObject.getString(YOAConstants.LINKED_IN_ACCESS_TOKEN);
			
		} catch (IOException e) {
			YOALogger.error("LinkedInExecutor", "createAccessToken", "exception to create token: "+e.getMessage());
		}
		
        System.out.println(accessToken);
        return accessToken;
	}
	
	public static String getProfileData(String accessToken)throws YOAException{
		String result=null;
		try{
		HttpClient httpClient = new HttpClient();
		GetMethod httpGet=new GetMethod("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,location,industry,positions,picture-url,headline,email-address)?format=json");//indiviadual user
		httpGet.addRequestHeader("Accept", "application/json");
		httpGet.addRequestHeader("Connection", "Keep-Alive");
		httpGet.addRequestHeader("Authorization", "Bearer "+accessToken);
		httpClient.executeMethod(httpGet);
		result = httpGet.getResponseBodyAsString();
		System.out.println(result);
	} catch (IOException e) {
		YOALogger.error("LinkedInExecutor", "createAccessToken", "exception to access profile: "+e.getMessage());
	}
	return result;
		
	}
}
