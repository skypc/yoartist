package com.skypc.yoartist.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class SalesOptException.
 */
public class YOAException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The err code. */
	private String errCode;
	
	private Throwable throwable;
	
	/** The err msg. */
	private String errMsg;

	/**
	 * Gets the err code.
	 *
	 * @return the err code
	 */
	public String getErrCode() {
		return errCode;
	}

	/**
	 * Sets the err code.
	 *
	 * @param errCode the new err code
	 */
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	/**
	 * Gets the err msg.
	 *
	 * @return the err msg
	 */
	public String getErrMsg() {
		return errMsg;
	}

	/**
	 * Sets the err msg.
	 *
	 * @param errMsg the new err msg
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	/**
	 * Instantiates a new sales opt exception.
	 *
	 * @param errCode the err code
	 * @param errMsg the err msg
	 */
	public YOAException(String errCode, String errMsg) {
		super(errMsg);
		this.errCode = errCode;
		this.errMsg = errMsg;
	}
	
	/**
	 * Instantiates a new sales opt exception.
	 *
	 * @param errMsg the err msg
	 */
	public YOAException(String errMsg) {
		super(errMsg);
		this.errMsg = errMsg;
	}
	public YOAException(Throwable throwable) {
		super(throwable);
		this.throwable=throwable;
	}
	
	/**
	 * @param errorCode
	 * @param message
	 * @param throwable
	 * 
	 */
	public YOAException(String errCode,String message,Throwable throwable) {
		super(message,throwable);
		this.errMsg = message;
		this.throwable=throwable;
		this.errCode = errCode;
	}
	
}

