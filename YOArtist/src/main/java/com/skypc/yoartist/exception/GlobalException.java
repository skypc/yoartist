package com.skypc.yoartist.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

// TODO: Auto-generated Javadoc
/**
 * The Class GlobalException.
 */
@ControllerAdvice
public class GlobalException  {

	/**
	 * Handle custom exception.
	 *
	 * @param ex the ex
	 * @return the model and view
	 */
	@ExceptionHandler(YOAException.class)
	public ModelAndView handleCustomException(YOAException ex) {

		ModelAndView model = new ModelAndView("error/innovision-error");
		model.addObject("status", ex.getErrCode());
		model.addObject("message", ex.getErrMsg());

		return model;

	}

	/**
	 * Handle all exception.
	 *
	 * @param ex the ex
	 * @return the model and view
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView handleAllException(Exception ex) {

		ModelAndView model = new ModelAndView("/generic_error");
		model.addObject("errMsg", ex.getMessage());

		return model;

	}
	
}

