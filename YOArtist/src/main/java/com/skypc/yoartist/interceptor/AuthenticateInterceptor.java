package com.skypc.yoartist.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.skypc.yoartist.constants.YOAConstants;
import com.skypc.yoartist.enums.EnumUserType;
import com.skypc.yoartist.logger.YOALogger;

public class AuthenticateInterceptor implements HandlerInterceptor {

	final String CATEGORY_NAME = "AuthenticateInterceptor";

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {
		final String METHOD_NAME = "preHandle";
		YOALogger.debug(CATEGORY_NAME, METHOD_NAME, "start peHandle ");



		HttpSession session = request.getSession();
		Long userId = (Long) session.getAttribute(YOAConstants.SESSION_USER_ID);
		String url = request.getRequestURI();
		YOALogger.debug(CATEGORY_NAME, METHOD_NAME, "\n\n\n session id***"+userId+"\n\n\n");

		if ((userId == null || userId == 0) && (url.contains("register") || url.contains("signin") ||url.contains("confirmEmail"))) {
			return true;
		}

		String homeURL = request.getContextPath();
		if (userId == null || !(userId > 0l)) {
			String root = "";
			if (url.contains("admin")) {
				root = "/admin/";
				response.sendRedirect(homeURL + root + YOAConstants.REDIRECT_LOGIN);
				return false;
			} else if (url.contains("user")) {
				root = "/user/";
				response.sendRedirect(homeURL);
				return false;
			}

		}
		/*Object roleTypeObj = request.getSession().getAttribute(YOAConstants.SESSION_USER_ROLE);
		String role = null;
		if (roleTypeObj != null)
			role = String.valueOf(roleTypeObj);
		if (url.contains("admin")) {
			if (role != null)
				if (!(role.contains(EnumUserType.SUPER_ADMIN.name()) || role.contains(EnumUserType.SUB_ADMIN.name()))) {
					response.sendRedirect(homeURL);
					return false;
				}
		} else if (url.contains("user")) {
			if (role.contains(EnumUserType.SUPER_ADMIN.name()) || role.contains(EnumUserType.SUB_ADMIN.name())) {
				response.sendRedirect(homeURL + "/admin/" + YOAConstants.REDIRECT_LOGIN);
				return false;
			}
		}*/
				
		return true;

	}

	
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		System.out.println("checking session interceptor post handler");
	}

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		System.out.println("checking session interceptor after completion");

	}

	private boolean isAdminURL(HttpServletRequest request, String type) {
		String url = request.getRequestURI();
		if (url.contains("admin")) {
			return true;
		}
		return false;
	}

		
}
