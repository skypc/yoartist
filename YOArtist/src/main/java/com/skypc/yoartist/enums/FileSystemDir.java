/**
 *
 */
package com.skypc.yoartist.enums;

import java.io.File;
import java.util.List;
import java.util.Properties;

import org.springframework.web.multipart.MultipartFile;

import com.skypc.yoartist.constants.YOAConstants;
import com.skypc.yoartist.util.PropertyUtil;

/**
 * @author perennial Innovision file system directory location enum
 */
public enum FileSystemDir {

	USER_POST_FILES_PATH(1), PROPOSAL_FILES(2),MESSAGE_FILES(3);

	static Properties properties = null;
	static {
		properties = PropertyUtil.getProperties("fileSystem");
	}

	private FileSystemDir(int id) {
	}

	public String getPath() {
		if (this.equals(USER_POST_FILES_PATH)) {
			return properties.getProperty(YOAConstants.FILE_SYSTEM_POST_DIR_PATH);
		} else if (this.equals(PROPOSAL_FILES)) {
			return properties.getProperty("fileSystem.innovision.dir.path.proposalFiles");
		} else if (this.equals(MESSAGE_FILES)) {
			/** assumption : messages are part of project */
			return properties.getProperty("fileSystem.innovision.dir.path.projectFiles");
		}
		// default path is tomcat dir with project folder
		return System.getProperty("catalina.home") + File.separator + "Innovision";
	}

	public static boolean isValidUploadSize(List<MultipartFile> multipartFiles) {

		if (multipartFiles == null || multipartFiles.size() == 0) {
			return true;
		}

		double uploadSizeBytes = 0;
		for (MultipartFile multipartFile : multipartFiles) {
			if (!multipartFile.isEmpty()) {
				uploadSizeBytes += multipartFile.getSize();
			}
		}

		String maxSizeStr = properties.getProperty("fileSystem.innovision.upload.size.mb");
		double maxSizeMb = maxSizeStr == null ? 25 : Double.parseDouble(maxSizeStr);

		/** conversion to mb */
		if (maxSizeMb >= (uploadSizeBytes / 1024 / 1024)) {
			return true;
		}
		return false;
	}
}
