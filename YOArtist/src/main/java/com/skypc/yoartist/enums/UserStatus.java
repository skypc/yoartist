/** project statuses */
package com.skypc.yoartist.enums;
public enum UserStatus {

	ACTIVE(1), DEACTIVE(2), PENDING(3);

	private int id;

	private UserStatus(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
}
