package com.skypc.yoartist.enums;

public enum EnumUserType {
	SUPER_ADMIN(1), SUB_ADMIN(2), ADMIN(3), USER(4);
	private long id;

	private EnumUserType(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

}
