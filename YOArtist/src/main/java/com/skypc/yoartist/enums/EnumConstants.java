package com.skypc.yoartist.enums;

public class EnumConstants {

	/** project statuses */
	public static enum MilesStoneStatus {

		PENDING(1), IN_PROGRESS(2), COMPLETED(3), ACCEPTED(4), REJECTED(5);

		private int id;

		private MilesStoneStatus(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}
	}

	public static enum ConnectStatus {
		PENDING, ACCEPTED, DECLINE;
	}

	public static enum NotificationType {
		CONNECT_REQUEST, ACCEPT_CONNECT, POST_LIKE, POST_UNLIKE,POST_COMMENTS;

	}
}
