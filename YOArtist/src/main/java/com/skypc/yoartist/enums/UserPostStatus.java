/** project statuses */
package com.skypc.yoartist.enums;
public enum UserPostStatus {

	DRAFT(1), POSTED(2);

	private int id;

	private UserPostStatus(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
}
