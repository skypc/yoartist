package com.skypc.yoartist.custom.taglib;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.codec.binary.Base64;

import com.skypc.yoartist.logger.YOALogger;


public class ConvertImageBaseCode extends SimpleTagSupport{

		byte[] image;

		StringWriter sw = new StringWriter();

		public void doTag() throws JspException, IOException {

			if (image != null) {
				/* Use message from attribute */
				
					JspWriter out = getJspContext().getOut();
					String base64Encoded = "";
					byte[] encodeBase64 = Base64.encodeBase64(image);
					try {
						base64Encoded = new String(encodeBase64, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						YOALogger.error("HomeViewHandler", "processLogin", e.getMessage());
						base64Encoded="";
					}
					out.println("data:image/jpeg;base64,"+base64Encoded);
					
				

			}
			else {
				/* use message from the body */
				getJspBody().invoke(sw);
				getJspContext().getOut().println(sw.toString());
			}

		}

		/**
		 * @return the encodedImage
		 */
		public byte[] getImage() {
			return image;
		}

		/**
		 * @param encodedImage the encodedImage to set
		 */
		public void setImage(byte[] image) {
			this.image = image;
		}

		
}
