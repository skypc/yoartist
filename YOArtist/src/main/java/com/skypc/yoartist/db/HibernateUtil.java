/*
 * 
 */
package com.skypc.yoartist.db;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.skypc.yoartist.exception.YOAException;

public class HibernateUtil {

	private static SessionFactory sessionFactory=null;

	private static ApplicationContext context = null;
	static {
		try {
			context = new ClassPathXmlApplicationContext(
					"application-context.xml");
		} catch (HibernateException ex) {

			throw new ExceptionInInitializerError(ex);
		}

	}

	public static SessionFactory getSessionFactory(String sessionFactoryName) throws YOAException {
		try {
			sessionFactory = (SessionFactory) context
					.getBean(sessionFactoryName);
		} catch (HibernateException ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new YOAException(ex);
		}
		return sessionFactory;
	}
	
	public static SessionFactory getSessionFactory() throws YOAException {
		if(sessionFactory==null){
			sessionFactory=getSessionFactory("innovisionSessionFactory");
		}
		return sessionFactory;
	}

	/**
	 *
	 * @return
	 */
	/*public static Session getSession() {
		return sessionFactory.getCurrentSession();
	}*/
	
	public static void closeSession(Session session) {
		session.close();
	}
	
	
	
}
