/*
 *
 */
package com.skypc.yoartist.service;
import java.io.Serializable;
import java.util.List;

import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.util.KeyValue;
import com.skypc.yoartist.vo.IGenericVO;

	// TODO: Auto-generated Javadoc
	/**
	 * The Interface YOADao.
	 * @author musaddique
	 *
	 * @param <S> the generic type
	 */
	public interface IService {

		public IGenericVO getById(Class cls, Serializable id) throws YOAException;

		public IGenericVO getByConditions(Class cls, List<KeyValue> conditions) throws YOAException;

		public List<IGenericVO> getObjectList(Class cls,List<KeyValue> conditions,List<KeyValue> orderByColumnNames,Integer startLimit,Integer offset) throws YOAException;

		public Long save(IGenericVO s) throws YOAException;

		public void save(List<IGenericVO> objects) throws YOAException;

		public void saveOrUpdate(IGenericVO s) throws YOAException;

		
		public IGenericVO merge(IGenericVO s) throws YOAException;

		public void delete(IGenericVO s) throws YOAException;

		public void deleteById(Class cls, Serializable id) throws YOAException;

		public void deleteListOfObjects(List<IGenericVO> objects) throws YOAException;

		public void deleteByConditions(Class cls, List<KeyValue> conditions) throws YOAException;

		public List<Object[]> executeSqlQuery(String sQuery, List<KeyValue> conditions) throws YOAException;

		public List<String> executeSqlQueryForString(String sQuery, List<KeyValue> conditions) throws YOAException;

		public List<Object> executeHqlQuery(String sQuery, List<KeyValue> conditions) throws YOAException;

		public List<Object> executeHqlQueryDistinctRootEntities(String sQuery, List<KeyValue> conditions)
				throws YOAException;

		public int deleteByHqlConditions(String hqlQuery, List<KeyValue> conditions)throws YOAException;

		public Object getSingleColumnBySqlQuery(String sQuery, List<KeyValue> conditions)throws YOAException;
		
		public Object getSingleColumnByHqlQuery(String sQuery, List<KeyValue> conditions)throws YOAException;

		public int updateBySqlQuery(String dbQuery, List<KeyValue> conditions1) throws YOAException;

		public List<Object> getObjectListByHQLQuery(String getBlockedDomainList, List<KeyValue> conditions) throws YOAException;

		public List<Object[]> getColumnByHqlQuery(String hqlQuery, List<KeyValue> conditions) throws YOAException;
		
		public int updateByHQLQuery(String query, List<KeyValue> conditions)throws YOAException;
		
		public List<Object> executeHqlQueryWithLimit(String sQuery, List<KeyValue> conditions, int startIndex, int offset,
				boolean getDistinctRootEntities) throws YOAException;
		public Object getByDifferentConditions(Class cls, List<KeyValue> conditions) throws YOAException;

		public void saveSharedPost(Long postId, List<Long> connectionIds, Long userId) throws YOAException;
}
