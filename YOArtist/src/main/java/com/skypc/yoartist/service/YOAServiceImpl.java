/*
 *
 */
package com.skypc.yoartist.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import com.skypc.yoartist.dao.YOADao;
import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.mail.YOAMailApi;
import com.skypc.yoartist.util.KeyValue;
import com.skypc.yoartist.vo.IGenericVO;

public class YOAServiceImpl implements IService {

	@Autowired
	private YOADao<IGenericVO> dao;

	@Autowired
	@Lazy
	private YOAMailApi InnovisionMailApi;

	@Override
	public IGenericVO getById(Class cls, Serializable id) throws YOAException {		
		return dao.getById(cls, id);
	}

	@Override
	public IGenericVO getByConditions(Class cls, List<KeyValue> conditions) throws YOAException {
		return dao.getByConditions(cls, conditions);
	}

	@Override
	public List<IGenericVO> getObjectList(Class cls, List<KeyValue> conditions, List<KeyValue> orderByColumnNames,
			Integer startLimit, Integer offset) throws YOAException {
		return dao.getObjectList(cls, conditions, orderByColumnNames, startLimit, offset);
	}

	@Override
	public Long save(IGenericVO s) throws YOAException {
		return dao.save(s);
	}

	@Override
	public void save(List<IGenericVO> objects) throws YOAException {
		dao.save(objects);
	}

	@Override
	public void saveOrUpdate(IGenericVO s) throws YOAException {
		dao.saveOrUpdate(s);
		
	}

	@Override
	public IGenericVO merge(IGenericVO s) throws YOAException {
		return dao.merge(s);
	}

	@Override
	public void delete(IGenericVO s) throws YOAException {
		dao.delete(s);
	}

	@Override
	public void deleteById(Class cls, Serializable id) throws YOAException {
		dao.deleteById(cls, id);
	}

	@Override
	public void deleteListOfObjects(List<IGenericVO> objects) throws YOAException {
		dao.deleteListOfObjects(objects);
	}

	@Override
	public void deleteByConditions(Class cls, List<KeyValue> conditions) throws YOAException {
		dao.deleteByConditions(cls, conditions);
	}

	@Override
	public List<Object[]> executeSqlQuery(String sQuery, List<KeyValue> conditions) throws YOAException {
		return dao.executeSqlQuery(sQuery, conditions);
	}

	@Override
	public List<String> executeSqlQueryForString(String sQuery, List<KeyValue> conditions) throws YOAException {
		return dao.executeSqlQueryForString(sQuery, conditions);
	}

	@Override
	public List<Object> executeHqlQuery(String sQuery, List<KeyValue> conditions) throws YOAException {
		return dao.executeHqlQuery(sQuery, conditions);
	}

	@Override
	public List<Object> executeHqlQueryDistinctRootEntities(String sQuery, List<KeyValue> conditions)
			throws YOAException {
		return dao.executeHqlQueryDistinctRootEntities(sQuery, conditions);
	}

	@Override
	public int deleteByHqlConditions(String hqlQuery, List<KeyValue> conditions) throws YOAException {
		return dao.deleteByHqlConditions(hqlQuery, conditions);
	}

	@Override
	public Object getSingleColumnBySqlQuery(String sQuery, List<KeyValue> conditions) throws YOAException {
		return dao.getSingleColumnBySqlQuery(sQuery, conditions);
	}

	@Override
	public int updateBySqlQuery(String dbQuery, List<KeyValue> conditions1) throws YOAException {
		return dao.updateBySqlQuery(dbQuery, conditions1);
	}

	@Override
	public List<Object> getObjectListByHQLQuery(String getBlockedDomainList, List<KeyValue> conditions)
			throws YOAException {
		return dao.getObjectListByHQLQuery(getBlockedDomainList, conditions);
	}

	@Override
	public List<Object[]> getColumnByHqlQuery(String hqlQuery, List<KeyValue> conditions) throws YOAException {
		return dao.getColumnByHqlQuery(hqlQuery, conditions);
	}

	@Override
	public int updateByHQLQuery(String query, List<KeyValue> conditions) throws YOAException {
		return dao.updateByHQLQuery(query, conditions);
	}

	@Override
	public List<Object> executeHqlQueryWithLimit(String sQuery, List<KeyValue> conditions, int startIndex, int offset,
			boolean getDistinctRootEntities) throws YOAException {
		return dao.executeHqlQueryWithLimit(sQuery, conditions, startIndex, offset, getDistinctRootEntities);
	}

	@Override
	public Object getByDifferentConditions(Class cls, List<KeyValue> conditions) throws YOAException {
		return dao.getByDifferentConditions(cls, conditions);
	}

	@Override
	public Object getSingleColumnByHqlQuery(String sQuery, List<KeyValue> conditions) throws YOAException {
		return dao.getSingleColumnByHqlQuery(sQuery,conditions);
	}

	@Override
	public void saveSharedPost(Long postId, List<Long> connectionIds, Long userId) throws YOAException {
		dao.saveSharedPost(postId, connectionIds, userId);
		
	}
	
	
}
