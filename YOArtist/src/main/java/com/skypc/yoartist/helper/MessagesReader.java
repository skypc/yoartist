package com.skypc.yoartist.helper;

import java.util.Properties;

import com.skypc.yoartist.bean.MessageCodeBean;
import com.skypc.yoartist.constants.MessagesConstant;
import com.skypc.yoartist.util.PropertyUtil;

public class MessagesReader {
	static Properties properties = null;
	static {
		properties = PropertyUtil.getProperties("messages");
	}

	public static MessageCodeBean getMessage(String messageKey, boolean flag) {
		MessageCodeBean bean = null;
		if (properties != null) {
			String value = properties.getProperty(messageKey, "");
			if (!value.isEmpty()) {
				if (flag) {
					bean = new MessageCodeBean(value, MessagesConstant.SUCCESS_CODE);
				} else {
					bean = new MessageCodeBean(value, MessagesConstant.ERROR_CODE);
				}
			}
			return bean;
		} else {
			return bean;
		}
	}
}
