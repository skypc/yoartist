/*
 *
 */
package com.skypc.yoartist.helper;

import java.util.Properties;

import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.mail.YOAMailApi;
import com.skypc.yoartist.util.MailSender;
import com.skypc.yoartist.util.PropertyUtil;

public class MailHelper {
	static Properties properties = null;
	static String domainName = null;
	static String from = null;
	static String password = null;
	static {
		properties = PropertyUtil.getProperties("emailTemplates");
		if (properties != null) {
			domainName = properties.getProperty("mail.domain.name");
			if (domainName.endsWith("/")) {
				domainName = domainName.substring(0, domainName.lastIndexOf("/"));
			}
			from = properties.getProperty("sender.email.address");
			password = properties.getProperty("sender.email.password");
		}
	}

	public void sendRegistraionVerificationMail(String name, String email, String uuid, YOAMailApi mailApi)
			throws YOAException {
		if (properties != null) {
			String subject = properties.getProperty("registration.email.verification.subject");
			String body = properties.getProperty("registration.email.verification.body");
			String linkAddress = domainName + "/user/confirmEmail?token=" + uuid;
			String link = "<a href=\"" + linkAddress + "\" " + "style=\"-moz-box-shadow:inset 0px 1px 0px 0px #cf866c;"
					+ " -webkit-box-shadow:inset 0px 1px 0px 0px #cf866c;"
					+ " box-shadow:inset 0px 1px 0px 0px #cf866c;"
					+ " background:-webkit-gradient(linear, left top, left bottom, "
					+ "color-stop(0.05, #f2572c), color-stop(1, #bc3315));"
					+ " background:-moz-linear-gradient(top, #f2572c 5%, #bc3315 100%);"
					+ " background:-webkit-linear-gradient(top, #f2572c 5%, #bc3315 100%);"
					+ " background:-o-linear-gradient(top, #f2572c 5%, #bc3315 100%);"
					+ " background:-ms-linear-gradient(top, #f2572c 5%, #bc3315 100%);"
					+ " background:linear-gradient(to bottom, #f2572c 5%, #bc3315 100%);"
					+ " filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f2572c',"
					+ " endColorstr='#bc3315',GradientType=0); background-color:#f2572c; display:inline-block;"
					+ " cursor:pointer; color:#ffffff; font-family:Courier New; font-size:13px;"
					+ " padding:13px 29px; text-decoration:none; text-shadow:0px 1px 0px #854629;\">"
					+ "click here to confirm your email</a>";
			body = body.replaceAll("#name#", name);
			body = body.replaceAll("#link#", link);

			MailSender mailSender = new MailSender(from, password, email, subject, body, mailApi);
			mailSender.start();

		}
	}	
}
