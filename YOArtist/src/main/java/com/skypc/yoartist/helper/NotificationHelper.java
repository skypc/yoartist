package com.skypc.yoartist.helper;

import com.skypc.yoartist.constants.NotificationConstant;
import com.skypc.yoartist.enums.EnumConstants.NotificationType;

public class NotificationHelper {
	public static String getNotificationDescByType(NotificationType type) {
		String desc = "";
		switch (type) {
		case CONNECT_REQUEST:
			desc = NotificationConstant.SEND_CONNECT_REQUEST;
			break;
		case ACCEPT_CONNECT:
			desc = NotificationConstant.ACCEPT_CONNECT_REQUEST;
			break;
		case POST_LIKE:
			desc = NotificationConstant.POST_LIKE;
			break;
		case POST_UNLIKE:
			desc = NotificationConstant.POST_UN_LIKE;
			break;
		case POST_COMMENTS:
			desc = NotificationConstant.POST_COMMENTS;
			break;
		default:
			desc = "";
			break;
		}
		return desc;
	}
	
	public static String getNotificationLinkByType(NotificationType type,long userId) {
		String link = "";
		switch (type) {
		case CONNECT_REQUEST:
		case ACCEPT_CONNECT:
			link = NotificationConstant.CONNECT_REQUEST_LINK.replaceAll("#userId#", String.valueOf(userId));
			break;
		default:
			link = "";
			break;
		}
		return link;
	}
}
