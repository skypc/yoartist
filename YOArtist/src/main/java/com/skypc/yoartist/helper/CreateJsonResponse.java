package com.skypc.yoartist.helper;

import org.json.JSONObject;

public class CreateJsonResponse {

	public static JSONObject createJsonObject(int messageCode, String message, Object data) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("messageCode", messageCode);
            jSONObject.put("message", message);
            jSONObject.put("data", data);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return jSONObject;
    }
}
