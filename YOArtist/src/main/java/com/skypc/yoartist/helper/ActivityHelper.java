package com.skypc.yoartist.helper;

import com.skypc.yoartist.constants.ActivityConstant;
import com.skypc.yoartist.constants.NotificationConstant;
import com.skypc.yoartist.enums.EnumConstants.NotificationType;

public class ActivityHelper {
	public static String getNotificationDescByType(NotificationType type,long userId1,long userId2,String name1,String name2,long postId ) {
		String desc = "";
		switch (type) {
		case ACCEPT_CONNECT:
			desc = ActivityConstant.CONNECTION_ACCEPTED;
			desc=desc.replaceAll("#userId1#", String.valueOf(userId1));
			desc=desc.replaceAll("#userId2#", String.valueOf(userId2));
			desc=desc.replaceAll("#user_name1#", name1);
			desc=desc.replaceAll("#user_name2#", name2);
			break;
		case POST_LIKE:
			desc = ActivityConstant.POST_LIKE;
			desc=desc.replaceAll("#userId#", String.valueOf(userId1));
			desc=desc.replaceAll("#user_name#", name1);
			desc=desc.replaceAll("#postId#", String.valueOf(postId));
			break;
		case POST_UNLIKE:
			desc = ActivityConstant.POST_UNLIKE;
			desc=desc.replaceAll("#userId#", String.valueOf(userId1));
			desc=desc.replaceAll("#user_name#", name1);
			desc=desc.replaceAll("#postId#", String.valueOf(postId));
			break;
		case POST_COMMENTS:
			desc = ActivityConstant.POST_COMMENTS;
			desc=desc.replaceAll("#userId#", String.valueOf(userId1));
			desc=desc.replaceAll("#user_name#", name1);
			desc=desc.replaceAll("#postId#", String.valueOf(postId));
			break;
		default:
			desc = "";
			break;
		}
		return desc;
	}
}
