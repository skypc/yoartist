package com.skypc.yoartist.helper;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.logger.YOALogger;
import com.skypc.yoartist.service.IService;
import com.skypc.yoartist.vo.NotificationVO;

public class NotificationExecutor {

	private static NotificationExecutor notificationExecutor = null;
	private static IService service;
	private static Queue<NotificationVO> notificationQueue = new LinkedBlockingQueue<>();
	
	private NotificationExecutor(){
		
	}

	static {
		DaemonThread daemonThread = new DaemonThread();
		Thread thread = new Thread(daemonThread);
		thread.setDaemon(true);
		thread.start();
	}

	private synchronized NotificationVO deQueue() {
		return notificationQueue.poll();
	}

	private synchronized void enQueue(NotificationVO notificationVO) {
		notificationQueue.add(notificationVO);
	}

	public synchronized void processNotification(NotificationVO notificationVO) {
		enQueue(notificationVO);
	}

	public static NotificationExecutor getInstance(IService iservice) {
		service = iservice;
		if (notificationExecutor == null) {
			synchronized (NotificationExecutor.class) {
				if (notificationExecutor == null) {
					notificationExecutor = new NotificationExecutor();
				}
			}
		}
		return notificationExecutor;
	}

	private static final class DaemonThread implements Runnable {
		private static final String CATEGORY_NAME = "NotificationExecutor";

		@Override
		public void run() {
			while (true) {
				YOALogger.debug(CATEGORY_NAME, "run", "running notification thread");
				NotificationVO vo = null;
				if (notificationExecutor != null) {
					YOALogger.debug(CATEGORY_NAME, "run", "getting notification from queue");
					vo = notificationExecutor.deQueue();
					if (vo==null) {
						YOALogger.debug(CATEGORY_NAME, "run", "queue is empty");
					}
					if (vo != null) {
						YOALogger.debug(CATEGORY_NAME, "run", "saving notification in db");
						try {
							service.save(vo);
							YOALogger.debug(CATEGORY_NAME, "run", "saved notification in db");
						} catch (YOAException e) {
							YOALogger.error(CATEGORY_NAME, "run", e.getMessage());
						}
					} else {
						try {
							YOALogger.debug(CATEGORY_NAME, "run", "sleeping for 1 sec");
							Thread.sleep(5000);
						} catch (Exception e) {
							YOALogger.error(CATEGORY_NAME, "run", e.getLocalizedMessage());
						}
					}
				} else {
					try {
						YOALogger.debug(CATEGORY_NAME, "run", "sleeping for 1 sec");
						Thread.sleep(5000);
					} catch (Exception e) {
						YOALogger.error(CATEGORY_NAME, "run", e.getLocalizedMessage());
					}

				}
			}

		}

	}

}
