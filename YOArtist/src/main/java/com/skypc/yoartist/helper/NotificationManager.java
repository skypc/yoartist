package com.skypc.yoartist.helper;

import java.util.Date;

import com.skypc.yoartist.constants.NotificationDescription;
import com.skypc.yoartist.exception.YOAException;
import com.skypc.yoartist.service.IService;
import com.skypc.yoartist.vo.NotificationVO;
import com.skypc.yoartist.vo.User;

public class NotificationManager {
	private IService service;
	public NotificationManager(IService service) {
		super();
		this.service = service;
	}
	
	private NotificationVO mapNotification(User sendFromUser,User sendToUser, String desc){
		NotificationVO notification=new NotificationVO();
		notification.setCreatedDateTime(new Date());
		notification.setModifiedDateTime(new Date());
		notification.setDescription(desc);
		notification.setFromUser(sendFromUser);
		notification.setToUser(sendToUser);
		notification.setRead(Boolean.FALSE);
		return notification;
	}

	public class ResearcherToClientNotification{
		public void sendSubmitProposalOnOpenProject(User sendToUser, User sendFromUser,long researcherId,String researcherName,long proposalId,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.CLIENT_SUBMIT_PROPOSAL_ON_OPEN_PROJECT;
			desc=replaceResearcherAndProject(desc, researcherId, researcherName, projectId, projectName);
			desc=desc.replaceAll("#proposalId#", String.valueOf(proposalId));
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			////service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
			
			
		}
		
		public void sendUpdateProposalOnOpenProject(User sendToUser, User sendFromUser,long researcherId,String researcherName,long proposalId,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.CLIENT_UPDATE_PROPOSAL_ON_OPEN_PROJECT;
			desc=replaceResearcherAndProject(desc, researcherId, researcherName, projectId, projectName);
			desc=desc.replaceAll("#proposalId#", String.valueOf(proposalId));
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			////service.save(notification);
			
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		
		public void sendUpdateMilestoneOnAssignProject(User sendToUser, User sendFromUser,long researcherId,String researcherName,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.CLIENT_UPDATE_MILESTONE_ON_PROJECT;
			desc=replaceResearcherAndProject(desc, researcherId, researcherName, projectId, projectName);
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			////service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
			
		}
		public void sendDeleteMilestoneOnAssignProject(User sendToUser, User sendFromUser,long researcherId,String researcherName,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.CLIENT_DELETE_MILESTONE_ON_PROJECT;
			desc=replaceResearcherAndProject(desc, researcherId, researcherName, projectId, projectName);
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			////service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
			
		}
		
		public void sendDiputeRaisedOnAssignProject(User sendToUser, User sendFromUser,long researcherId,String researcherName,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.CLIENT_DISPUTE_RAISED_ON_PROJECT;
			desc=replaceResearcherAndProject(desc, researcherId, researcherName, projectId, projectName);
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		
		public void sendRatingOnAssignProject(User sendToUser, User sendFromUser,long researcherId,String researcherName,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.CLIENT_RATING_GIVEN_ON_PROJECT;
			desc=replaceResearcherAndProject(desc, researcherId, researcherName, projectId, projectName);
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		
		public void sendDeclineInvitationProject(User sendToUser, User sendFromUser,long researcherId,String researcherName,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.CLIENT_DECLINE_INVITATION_ON_PROJECT;
			desc=replaceResearcherAndProject(desc, researcherId, researcherName, projectId, projectName);
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		
		public String replaceResearcherAndProject(String desc,long researcherId,String researcherName,long projectId,String projectName){
			String result="";
			result=desc.replaceAll("#researcherId#", String.valueOf(researcherId));
			result=result.replaceAll("#researcherName#", researcherName);
			result=result.replaceAll("#projectId#", String.valueOf(projectId));
			result=result.replaceAll("#projectName#", projectName);
			return result;
		}
	}
	
	public class ClientToResearcherNotification{
		
		public String replaceClientAndProject(String desc,long clientId,String clientName,long projectId,String projectName){
			String result="";
			result=desc.replaceAll("#clientId#", String.valueOf(clientId));
			result=result.replaceAll("#clientName#", clientName);
			result=result.replaceAll("#projectId#", String.valueOf(projectId));
			result=result.replaceAll("#projectName#", projectName);
			
			return result;
			
		}
		public void sendInvitation(User sendToUser, User sendFromUser,long clientId,String clientName,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.RESEARCHER_INVITATION;
			desc=replaceClientAndProject(desc, clientId, clientName, projectId, projectName);
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		
		public void sendEditedOpenProjectByClient(User sendToUser, User sendFromUser,long clientId,String clientName,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.RESEARCHER_EDITED_OPEN_PROJECT_BY_CLIENT;
			desc=replaceClientAndProject(desc, clientId, clientName, projectId, projectName);
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		
		public void sendAwardedProjectToResearcherByClient(User sendToUser, User sendFromUser,long clientId,String clientName,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.RESEARCHER_AWARDED_PROJECT_BY_CLIENT;
			desc=replaceClientAndProject(desc, clientId, clientName, projectId, projectName);
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		
		public void sendAwardedProjectToOtherByClient(User sendToUser, User sendFromUser,long clientId,String clientName,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.RESEARCHER_AWARDED_PROJECT_TO_OTHER;
			desc=desc.replaceAll("#projectId#", String.valueOf(projectId));
			desc=desc.replaceAll("#projectName#", projectName);
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		
		public void sendCancelProjectOnSubmittedProposalByClient(User sendToUser, User sendFromUser,long clientId,String clientName,long projectId,String projectName,long proposalId)throws YOAException{
			String desc=NotificationDescription.RESEARCHER_CANCELLED_PROJECT_ON_SUBMITTED_PROPOSAL;
			desc=replaceClientAndProject(desc, clientId, clientName, projectId, projectName);
			desc=desc.replaceAll("#proposalId#", String.valueOf(proposalId));
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		public void sendCancelProjectOnAssignByClient(User sendToUser, User sendFromUser,long clientId,String clientName,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.RESEARCHER_CANCELLED_ON_ASSIGN_PROJECT_BY_CLIENT;
			desc=replaceClientAndProject(desc, clientId, clientName, projectId, projectName);
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		
		public void sendDisputeProjectOnAssignByClient(User sendToUser, User sendFromUser,long clientId,String clientName,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.RESEARCHER_DISPUTED_RAISED_ON_ASSIGN_PROJECT_BY_CLIENT;
			desc=replaceClientAndProject(desc, clientId, clientName, projectId, projectName);
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		public void sendRatingByClientToResearcher(User sendToUser, User sendFromUser,long clientId,String clientName,long projectId,String projectName)throws YOAException{
			String desc=NotificationDescription.RESEARCHER_REVIEW_AND_RATING_BY_CLIENT;
			desc=replaceClientAndProject(desc, clientId, clientName, projectId, projectName);
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		
		public void sendDeclineProposal(User sendToUser, User sendFromUser,long clientId,String clientName,long projectId,String projectName,long proposalId)throws YOAException{
			String desc=NotificationDescription.RESEARCHER_DECLINE_PROPOSAL_ON_OPEN_PROJECT;
			desc=replaceClientAndProject(desc, clientId, clientName, projectId, projectName);
			desc=desc.replaceAll("#proposalId#", String.valueOf(proposalId));
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		public void sendShortlistedProposal(User sendToUser, User sendFromUser,long clientId,String clientName,long projectId,String projectName,long proposalId)throws YOAException{
			String desc=NotificationDescription.RESEARCHER_SHORTLISTED_PROPOSAL_ON_OPEN_PROJECT;
			desc=replaceClientAndProject(desc, clientId, clientName, projectId, projectName);
			desc=desc.replaceAll("#proposalId#", String.valueOf(proposalId));
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
		
		public void sendWithdrawShortlistedProposal(User sendToUser, User sendFromUser,long clientId,String clientName,long projectId,String projectName,long proposalId)throws YOAException{
			String desc=NotificationDescription.RESEARCHER_WITHDRAW_SHORTLISTED_PROPOSAL_ON_OPEN_PROJECT;
			desc=replaceClientAndProject(desc, clientId, clientName, projectId, projectName);
			desc=desc.replaceAll("#proposalId#", String.valueOf(proposalId));
			NotificationVO notification = mapNotification(sendFromUser, sendToUser, desc);
			//service.save(notification);
			NotificationExecutor instance = NotificationExecutor.getInstance(service);
			instance.processNotification(notification);
		}
	}

}


