package com.skypc.yoartist.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = YOATables.TABLE_SHARED_POST)
@DynamicUpdate(value=true)
@DynamicInsert(value=true)
public class SharedPost implements Serializable, IGenericVO {

	
	
	/**
	 * @serial
	 */
	private static final long serialVersionUID = -2771648597175522687L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "shared_by")
	private Long sharedBy;
	
	@Column(name = "shared_to")
	private Long sharedTo;
	
	@Column(name = "post_id")
	private Long postId;
	
	@Column(name = "shared_type")
	private String sharedType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getSharedBy() {
		return sharedBy;
	}

	public void setSharedBy(Long sharedBy) {
		this.sharedBy = sharedBy;
	}

	public Long getSharedTo() {
		return sharedTo;
	}

	public void setSharedTo(Long sharedTo) {
		this.sharedTo = sharedTo;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	
}
