package com.skypc.yoartist.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = YOATables.TABLE_USER)
@DynamicUpdate(value=true)
@DynamicInsert(value=true)
public class User implements Serializable, IGenericVO {


	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "password", length = 500, nullable = false)
	private String password;

	@Column(name = "email", length = 500, nullable = false, unique = true)
	private String email;
	
	@Lob
	@Column(name="profile_photo",length=100000000)
	private byte[] profilePhoto;

	@Column(name = "status")
	private String status;
	
	@Column(name = "is_deleted")
	private String isDeleted;
	
	@OneToMany(mappedBy="user",cascade=CascadeType.ALL)
	private List<UserPosts> userPosts;
	
	
	@Column(name = "email_verification_uuid")
	private String emailVerificationUUID;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "base_user")
	private BaseUser baseUser;

	/*@ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinTable(name = "user_roles", joinColumns = {
			@JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "role_id", referencedColumnName = "id") })
	private Set<Role> roles;*/

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	public User() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	
	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEmailVerificationUUID() {
		return emailVerificationUUID;
	}

	public void setEmailVerificationUUID(String emailVerificationUUID) {
		this.emailVerificationUUID = emailVerificationUUID;
	}
	
	public byte[] getProfilePhoto() {
		return profilePhoto;
	}

	public void setProfilePhoto(byte[] profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	

	public List<UserPosts> getUserPosts() {
		return userPosts;
	}

	public void setUserPosts(List<UserPosts> userPosts) {
		this.userPosts = userPosts;
	}

	public String getFullName(){
		String name="";
		if(this.firstName!=null && !this.firstName.isEmpty()){
			name+=this.firstName;
		}
		if(this.lastName!=null && !this.lastName.isEmpty()){
			name+=" "+this.lastName;
		}
		return name;
	}

	public BaseUser getBaseUser() {
		return baseUser;
	}

	public void setBaseUser(BaseUser baseUser) {
		this.baseUser = baseUser;
	}

	
}
