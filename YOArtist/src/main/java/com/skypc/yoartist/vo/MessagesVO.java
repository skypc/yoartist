/*
 *
 */
package com.skypc.yoartist.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.skypc.yoartist.util.DateUtil;


// TODO: Auto-generated Javadoc
/**
 * The Class MessagesVO.
 */
@Entity
@Table(name=YOATables.TABLE_MESSAGES)
@DynamicUpdate(value=true)
@DynamicInsert(value=true)
public class MessagesVO implements Serializable, IGenericVO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 557352952434933264L;


	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private long id;

	/** The source user. */
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "source_user_id")
	private User sourceUser;

	/** The destination user. */
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "destination_user_id")
	private User destinationUser;
	
	/** The message. */
	@Column(name = "message", columnDefinition = "TEXT")
	private String message;
	
	/** The is read. */
	@Column(name="is_read")
	private boolean isRead;

	/** The created date time. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date_time")
	private Date createdDateTime;

	/** The modified date time. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="modified_date_time")
	private Date modifiedDateTime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getSourceUser() {
		return sourceUser;
	}

	public void setSourceUser(User sourceUser) {
		this.sourceUser = sourceUser;
	}

	public User getDestinationUser() {
		return destinationUser;
	}

	public void setDestinationUser(User destinationUser) {
		this.destinationUser = destinationUser;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public Date getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(Date modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	
	public String getDuration(){
		return DateUtil.getNotificationMessageDate(this.modifiedDateTime);
	}
}
