package com.skypc.yoartist.vo;

public interface YOATables {
	public static final String TABLE_USER = "user";
	public static final String TABLE_USER_POST = "user_posts";
	public static final String TABLE_POST_COMMENTS = "post_comments";
	public static final String TABLE_ADDRESS = "address";
	public static final String TABLE_BASE_USER = "base_user";
	public static final String TABLE_CONNCTION = "connection";
	
	public static final String TABLE_ORGANISATION = "organisation";
	public static final String TABLE_EDUCATIONAL_DETAILS = "education_details";
	public static final String TABLE_EMPLOYMENT_DETAILS = "employment_details";
	public static final String TABLE_PUBLICATION_DETAILS = "publication_details";

	public static final String TABLE_MESSAGES = "messages";
	public static final String TABLE_LIKES = "likes";
	public static final String TABLE_MESSAGES_ATTACHMENTS = "messages_attachments";
	public static final String TABLE_NOTIFICATION = "notification";

	public static final String TABLE_PROJECT_CATAGORY = "project_catagory";
	public static final String TABLE_PROJECT_SUB_CATAGORY = "project_sub_catagory";
	public static final String TABLE_SHARED_POST = "shared_post";
	
	public static final String TABLE_SUMMARY_DETAILS = "summary_details";
}
