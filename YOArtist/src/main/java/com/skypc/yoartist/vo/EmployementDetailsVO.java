/*
 * 
 */
package com.skypc.yoartist.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;


// TODO: Auto-generated Javadoc
/**
 * The Class EmployementDetailsVO.
 */
@Entity
@Table(name = YOATables.TABLE_EMPLOYMENT_DETAILS)
@DynamicUpdate(value = true)
@DynamicInsert(value = true)
public class EmployementDetailsVO implements Serializable, IGenericVO {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/** The job position. */
	@Column(name = "job_position")
	private String jobPosition;

	/** The company. */
	@Column(name = "org_name")
	private String orgName;

	/** The start date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_date")
	private Date startDate;

	/** The end date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_date")
	private Date endDate;

	/** The is current organisation. */
	@Column(name = "isCurrentlyWorking")
	private boolean isCurrentlyWorking;

	/** The state. */
	@Column(name = "state")
	private String state;

	/** The country. */
	@Column(name = "country")
	private String country;

	// /** The highest degree. */
	// @Column(name = "highest_degree")
	// private String highestDegree;

	/** The user. */
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "user_id")
	private User user;

	/** The is deleted. */
	@Column(name = "is_deleted")
	private boolean isDeleted;

	/** The created date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	/** The modified date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	/** The employment duration. */
	@Column(name = "employment_duration")
	private String employmentDuration;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the job position.
	 *
	 * @return the job position
	 */
	public String getJobPosition() {
		return jobPosition;
	}

	/**
	 * Sets the job position.
	 *
	 * @param jobPosition the new job position
	 */
	public void setJobPosition(String jobPosition) {
		this.jobPosition = jobPosition;
	}


	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user the new user
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Gets the checks if is deleted.
	 *
	 * @return the checks if is deleted
	 */
	public boolean getIsDeleted() {
		return isDeleted;
	}

	/**
	 * Sets the deleted.
	 *
	 * @param isDeleted the new deleted
	 */
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate the new created date
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * Gets the modified date.
	 *
	 * @return the modified date
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * Sets the modified date.
	 *
	 * @param modifiedDate the new modified date
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	// /**
	// * Gets the highest degree.
	// *
	// * @return the highestDegree
	// */
	// public String getHighestDegree() {
	// return highestDegree;
	// }
	//
	// /**
	// * Sets the highest degree.
	// *
	// * @param highestDegree the highestDegree to set
	// */
	// public void setHighestDegree(String highestDegree) {
	// this.highestDegree = highestDegree;
	// }

	/**
	 * Gets the employment duration.
	 *
	 * @return the employmentDuration
	 */
	public String getEmploymentDuration() {
		return employmentDuration;
	}

	/**
	 * Sets the employment duration.
	 *
	 * @param employmentDuration the employmentDuration to set
	 */
	public void setEmploymentDuration(String employmentDuration) {
		this.employmentDuration = employmentDuration;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public boolean isCurrentlyWorking() {
		return isCurrentlyWorking;
	}

	public void setCurrentlyWorking(boolean isCurrentlyWorking) {
		this.isCurrentlyWorking = isCurrentlyWorking;
	}

	
}
