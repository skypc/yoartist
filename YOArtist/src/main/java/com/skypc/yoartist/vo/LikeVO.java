/*
 *
 */
package com.skypc.yoartist.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.skypc.yoartist.util.DateUtil;


// TODO: Auto-generated Javadoc
/**
 * The Class MessagesVO.
 */
@Entity
@Table(name=YOATables.TABLE_LIKES)
@DynamicUpdate(value=true)
@DynamicInsert(value=true)
public class LikeVO implements Serializable, IGenericVO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 557352952434933264L;


	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private long id;

	/** The source user. */
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "user_id")
	private User user;

	/** The destination user. */
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "post_id")
	private UserPosts post;
		
	/** The created date time. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date_time")
	private Date createdDateTime;

	/** The modified date time. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="modified_date_time")
	private Date modifiedDateTime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserPosts getPost() {
		return post;
	}

	public void setPost(UserPosts post) {
		this.post = post;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public Date getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(Date modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}
	
	

}
