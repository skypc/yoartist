/*
 *
 */
package com.skypc.yoartist.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.skypc.yoartist.constants.YOATables;
import com.skypc.yoartist.enums.EnumConstants.NotificationType;

// TODO: Auto-generated Javadoc
/**
 * The Class NotificationVO.
 */
@Entity
@Table(name = YOATables.TABLE_NOTIFICATION)
@DynamicUpdate(value = true)
@DynamicInsert(value = true)
public class NotificationVO implements IGenericVO, Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1483785896574198189L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	/** The from user. */
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "from_user_id")
	private User fromUser;

	/** The to user. */
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "to_user_id")
	private User toUser;

	/** The description. */
	@Column(name = "description", columnDefinition = "TEXT")
	private String description;

	/** The status. */
	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private NotificationType type;

	/** The is read. */
	@Column(name = "is_read")
	private boolean isRead;

	/** The created date time. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date_time")
	private Date createdDateTime;

	/** The modified date time. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date_time")
	private Date modifiedDateTime;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the from user.
	 *
	 * @return the from user
	 */
	public User getFromUser() {
		return fromUser;
	}

	/**
	 * Sets the from user.
	 *
	 * @param fromUser
	 *            the new from user
	 */
	public void setFromUser(User fromUser) {
		this.fromUser = fromUser;
	}

	/**
	 * Gets the to user.
	 *
	 * @return the to user
	 */
	public User getToUser() {
		return toUser;
	}

	/**
	 * Sets the to user.
	 *
	 * @param toUser
	 *            the new to user
	 */
	public void setToUser(User toUser) {
		this.toUser = toUser;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Checks if is read.
	 *
	 * @return true, if is read
	 */
	public boolean isRead() {
		return isRead;
	}

	/**
	 * Sets the read.
	 *
	 * @param isRead
	 *            the new read
	 */
	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	/**
	 * Gets the created date time.
	 *
	 * @return the created date time
	 */
	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	/**
	 * Sets the created date time.
	 *
	 * @param createdDateTime
	 *            the new created date time
	 */
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	/**
	 * Gets the modified date time.
	 *
	 * @return the modified date time
	 */
	public Date getModifiedDateTime() {
		return modifiedDateTime;
	}

	/**
	 * Sets the modified date time.
	 *
	 * @param modifiedDateTime
	 *            the new modified date time
	 */
	public void setModifiedDateTime(Date modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

}
