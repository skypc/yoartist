/*
 *
 */
package com.skypc.yoartist.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.skypc.yoartist.constants.YOATables;
import com.skypc.yoartist.enums.EnumConstants.NotificationType;

// TODO: Auto-generated Javadoc
/**
 * The Class USERActivityVO.
 */
@Entity
@Table(name = YOATables.TABLE_USER_ACTIVITY)
@DynamicUpdate(value = true)
@DynamicInsert(value = true)
public class UserActivityVO implements IGenericVO, Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1483785896574198189L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	/** The from user. */
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "user_id")
	private User userId;
	
	/** The from user. */
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "post_id")
	private UserPosts postId;

	/** The description. */
	@Column(name = "description", columnDefinition = "TEXT")
	private String description;
	
	@Column(name = "type")
	private String type;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "second_user_id")
	private User secondUserId;

	/** The is read. */
	@Column(name = "is_read")
	private boolean isRead;

	/** The created date time. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date_time")
	private Date createdDateTime;

	/** The modified date time. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date_time")
	private Date modifiedDateTime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public Date getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(Date modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	public User getUserId() {
		return userId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public User getSecondUserId() {
		return secondUserId;
	}

	public void setSecondUserId(User secondUserId) {
		this.secondUserId = secondUserId;
	}

	public UserPosts getPostId() {
		return postId;
	}

	public void setPostId(UserPosts postId) {
		this.postId = postId;
	}

	

}
