homeApp.service('genericFactory', function($http, API_BASE, $q) {

	function getGenericResponse(url, param) {
		if (url != null && url != "") {
			if (param != null && param != "") {
				url += "?" + param;
			}
			var deferred = $q.defer();
			$http({
				method : 'GET',
				url : API_BASE + 'ajax/' + url,
				timeout : 18000
			}).then(function(data, status, headers, config) {
				var result = data;
				deferred.resolve(result);

			}, function(errorResponse) {
				deferred.reject(errorResponse);
			});

			return deferred.promise;
		}
	}
	return {
		getGenericResponse : getGenericResponse
	}
});