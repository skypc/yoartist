homeApp.service('notifyAlertService', function() {
	var notifyAlert = {
		generateNotification : generateNotification
	}
	return notifyAlert;

	function generateNotification(type,title,message) {
		var icon="";
		if(type=='success'){
			icon='glyphicon glyphicon-ok';
		}
		var messageType = type;
		var alertNotify = $.notify({
			title: "  <strong>"+title+"</strong> ",
			message : message,
			icon:icon,
		}, {
			placement : {
				align : "right",
			},
			offset : {
				x : 5,
				y : 50
			},
			animate: {
				enter: 'animated fadeInDown',
				exit: 'animated fadeOutUp'
			},
			type : messageType,
			allow_dismiss : false,
			delay : 2000,
			
		});
	}
});