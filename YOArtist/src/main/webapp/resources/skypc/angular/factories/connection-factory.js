homeApp.service('connectionFactory', function($http, API_BASE, $q) {
    var connect = {
    		getConnectionsRequestCount: getConnectionsRequestCount,
    		getAllConnectionsRequest:getAllConnectionsRequest,
    		getUserConnection : getUserConnection,
    		sharePost: sharePost
    }
    return connect;

    function getConnectionsRequestCount() {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: API_BASE + 'ajax/get-connect-request-count',
            timeout: 18000
        }).then(function(data, status, headers, config) {
            var count = data;
            deferred.resolve(count);
        }, function(errorResponse) {
            deferred.reject(errorResponse);
        })
        return deferred.promise;
    };
    
    
    function getAllConnectionsRequest(){
    	var deferred = $q.defer();
    	$http({
            method: 'GET',
            url: API_BASE + 'ajax/get-all-connect-request',
            timeout: 18000
        }).then(function(data, status, headers, config) {
        	var result = data;
        	deferred.resolve(result);
            
        }, function(errorResponse) {
        	deferred.reject(errorResponse);
        });
        
    	return deferred.promise;
    }
    
    function sharePost(postId,sharedConnection){
    	var connectIds = [];
    	if(sharedConnection.length>0){
    		for(var i=0;i<sharedConnection.length;i++){
    			connectIds.push(sharedConnection[i].userId);
    		}
    	}
    	
    	var shareData = {postId:postId,connectionIds:connectIds.join(',')};
    	
    	return $http({
            method: 'POST',
            url: API_BASE + 'ajax/sharepost',
            headers: {
                //'Authorization': 'Basic ',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {

                var str = [];
                for (var p in obj) {
                    str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                }
                
                return str.join( '&' );
            },
            data: shareData
        });
    }
    
    
    function getUserConnection(){
    	return $http({
            method: 'POST',
            url: API_BASE + 'ajax/user-connection',
            headers: {
                //'Authorization': 'Basic ',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {

                var str = [];
                for (var p in obj) {
                    str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                }
                
                return str.join( '&' );
            },
            data: ''
        });
    	
    	
    }
});