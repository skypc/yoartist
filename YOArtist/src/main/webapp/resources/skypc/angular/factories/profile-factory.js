homeApp.service('profileFactory', function($http, API_BASE, $q) {
    var connect = {
    		getSchoolList: getSchoolList,
    		getCompanyList:getCompanyList,
            getUserDetailsInfo:getUserDetailsInfo,
            saveEducationData: saveEducationData,
            saveEmploymentData: saveEmploymentData,
            getEmpData: getEmpData,
            deleteEmpData: deleteEmpData,
            updateEmploymentData: updateEmploymentData,
            saveSummary: saveSummary,
            getSummaryData: getSummaryData,
            deleteSummaryData:deleteSummaryData,
            updateSummaryDetails: updateSummaryDetails
    		//saveCompanyData: saveCompanyData
    }
    return connect;

    function getSchoolList() {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: API_BASE + 'ajax/get-school-data',
            timeout: 18000
        }).then(function(data, status, headers, config) {
            var responseData = data;
            deferred.resolve(responseData);
        }, function(errorResponse) {
            deferred.reject(errorResponse);
        })
        return deferred.promise;
    };

    function getCompanyList() {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: API_BASE + 'ajax/get-company-data',
            timeout: 18000
        }).then(function(data, status, headers, config) {
            var responseData = data;
            deferred.resolve(responseData);
        }, function(errorResponse) {
            deferred.reject(errorResponse);
        })
        return deferred.promise;
    };
    
    
    function getSummaryData(){
    	var deferred = $q.defer();
        $http({
            method: 'GET',
            url: API_BASE + 'ajax/get-summary-data',
            timeout: 18000
        }).then(function(data, status, headers, config) {
            var responseData = data;
            deferred.resolve(responseData);
        }, function(errorResponse) {
            deferred.reject(errorResponse);
        })
        return deferred.promise;
    	
    }
    
    function saveSummary(summaryObj){
    	return $http( {
            method: 'POST',
            url: API_BASE + 'ajax/summary',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function ( obj ) {
                var str = [];
                for ( var p in obj ) {
                    str.push( encodeURIComponent( p ) + '=' + encodeURIComponent( obj[ p ] ) );
                }
                return str.join( '&' );
            },
            data: summaryObj
        } );
    }
    
    function saveEmploymentData(employmentObj){
    	return $http( {
            method: 'POST',
            url: API_BASE + 'ajax/employment',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function ( obj ) {
                var str = [];
                for ( var p in obj ) {
                    str.push( encodeURIComponent( p ) + '=' + encodeURIComponent( obj[ p ] ) );
                }
                return str.join( '&' );
            },
            data: employmentObj
        } );
    }
    
    function deleteEmpData(id){
    	return $http( {
            method: 'delete',
            url: API_BASE + 'ajax/employment/' + id,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function ( obj ) {
                var str = [];
                for ( var p in obj ) {
                    str.push( encodeURIComponent( p ) + '=' + encodeURIComponent( obj[ p ] ) );
                }
                return str.join( '&' );
            },
            data: ''
        } );
    }
    
    function deleteSummaryData(id){
    	return $http( {
            method: 'delete',
            url: API_BASE + 'ajax/summary/' + id,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function ( obj ) {
                var str = [];
                for ( var p in obj ) {
                    str.push( encodeURIComponent( p ) + '=' + encodeURIComponent( obj[ p ] ) );
                }
                return str.join( '&' );
            },
            data: ''
        } );
    }
    
    function updateEmploymentData(employmentObj){
    	return $http( {
            method: 'POST',
            url: API_BASE + 'ajax/updateEmploymentDetails',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function ( obj ) {
                var str = [];
                for ( var p in obj ) {
                    str.push( encodeURIComponent( p ) + '=' + encodeURIComponent( obj[ p ] ) );
                }
                return str.join( '&' );
            },
            data: employmentObj
        } );
    }
    
    function updateSummaryDetails(summaryObj){
    	
    	return $http( {
            method: 'POST',
            url: API_BASE + 'ajax/updateSummaryDetails',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function ( obj ) {
                var str = [];
                for ( var p in obj ) {
                    str.push( encodeURIComponent( p ) + '=' + encodeURIComponent( obj[ p ] ) );
                }
                return str.join( '&' );
            },
            data: summaryObj
        } );
    }
    
    function getUserDetailsInfo() {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: API_BASE + 'ajax/get-company-data',
            timeout: 18000
        }).then(function(data, status, headers, config) {
            var responseData = data;
            deferred.resolve(responseData);
        }, function(errorResponse) {
            deferred.reject(errorResponse);
        })
        return deferred.promise;
    };

    function getEmpData(){
    	var deferred = $q.defer();
        $http({
            method: 'GET',
            url: API_BASE + 'ajax/get-emp-data',
            timeout: 18000
        }).then(function(data, status, headers, config) {
            var responseData = data;
            deferred.resolve(responseData);
        }, function(errorResponse) {
            deferred.reject(errorResponse);
        })
        return deferred.promise;
    }
    
    function saveEducationData(educationObj){
    	var deferred = $q.defer();
    	var res = $http.post(API_BASE + 'ajax/education', JSON.stringify(educationObj));
		res.success(function(data, status, headers, config) {
			deferred.resolve(data);
		});
		res.error(function(data, status, headers, config) {
			deferred.reject(data);
		});	
		return deferred.promise;
       /* return $http( {
            method: 'POST',
            url: API_BASE + 'ajax/education',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function ( obj ) {
                var str = [];
                for ( var p in obj ) {
                    str.push( encodeURIComponent( p ) + '=' + encodeURIComponent( obj[ p ] ) );
                }
                return str.join( '&' );
            },
            data: educationObj
        } );*/
    };
    
    /*function sharePost(postId,sharedConnection){
    	var connectIds = [];
    	if(sharedConnection.length>0){
    		for(var i=0;i<sharedConnection.length;i++){
    			connectIds.push(sharedConnection[i].userId);
    		}
    	}
    	
    	var shareData = {postId:postId,connectionIds:connectIds.join(',')};
    	
    	return $http({
            method: 'POST',
            url: API_BASE + 'ajax/sharepost',
            headers: {
                //'Authorization': 'Basic ',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {

                var str = [];
                for (var p in obj) {
                    str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                }
                
                return str.join( '&' );
            },
            data: shareData
        });
    }*/
    
    
});