homeApp.service('notificationFactory', function($http, API_BASE, $q) {
    var notify = {
        getNotificationCount: getNotificationCount,
        getLimitedNotification: getLimitedNotification,
        getAllNotification: getAllNotification
     
    }
    return notify;

    function getNotificationCount() {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: API_BASE + 'ajax/get-notifications-count',
            timeout: 18000
        }).then(function(data, status, headers, config) {
            var count = data;
            deferred.resolve(count);
        }, function(errorResponse) {
            deferred.reject(errorResponse);
        })
        return deferred.promise;
    };


    function getLimitedNotification() {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: API_BASE + 'ajax/get-limited-notifications',
            timeout: 18000
        }).then(function(data, status, headers, config) {
            var result = data;
            deferred.resolve(result);

        }, function(errorResponse) {
            deferred.reject(errorResponse);
        });

        return deferred.promise;
    }

    function getAllNotification() {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: API_BASE + 'ajax/get-all-notifications',
            timeout: 18000
        }).then(function(data, status, headers, config) {
            var result = data;
            deferred.resolve(result);

        }, function(errorResponse) {
            deferred.reject(errorResponse);
        });

        return deferred.promise;
    }


});