homeApp.service('messageFactory', function($http, API_BASE, $q) {
    var message = {
        getMessageUser: getMessageUser,
        getMessageUserChats: getMessageUserChats,
        sendMessageToUser: sendMessageToUser
    }
    return message;

    function getMessageUser() {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: API_BASE + 'ajax/get-message-users',
            timeout: 18000
        }).then(function(data, status, headers, config) {
            var count = data;
            deferred.resolve(count);
        }, function(errorResponse) {
            deferred.reject(errorResponse);
        })
        return deferred.promise;
    };

    function getMessageUserChats(userId) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: API_BASE + 'ajax/get-message-user-chats?userId=' + userId,
            timeout: 18000
        }).then(function(data, status, headers, config) {
            var result = data;
            deferred.resolve(result);

        }, function(errorResponse) {
            deferred.reject(errorResponse);
        });

        return deferred.promise;
    }

    function sendMessageToUser(userId, message) {
        var param = "userId=" + userId + "&message=" + message;
        $http({
            method: 'GET',
            url: API_BASE + 'ajax/send-message-to-user?' + param,
            timeout: 18000
        }).then(function(data, status, headers, config) {

        }, function(errorResponse) {

        })

    };
});