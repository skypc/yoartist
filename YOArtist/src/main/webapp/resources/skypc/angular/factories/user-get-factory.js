homeApp.service('userFactory', function($http, API_BASE, $q) {
	var user = {
		getUserToConnect : getUserToConnect,
		sendConnectRequest : sendConnectRequest,
		acceptIgnoreConnectRequest : acceptIgnoreConnectRequest,
		getUsersForSearchBox : getUsersForSearchBox
	}
	return user;

	function getUserToConnect() {
		var deferred = $q.defer();
		$http({
			method : 'GET',
			url : API_BASE + 'ajax/users-to-connect',
			timeout : 18000
		}).then(function(data, status, headers, config) {
			var users = data;
			deferred.resolve(users);
		}, function(errorResponse) {
			deferred.reject(errorResponse);
		})
		return deferred.promise;
	}
	;

	function getUsersForSearchBox() {
		var deferred = $q.defer();
		$http({
			method : 'GET',
			url : API_BASE + 'ajax/users-for-search-box',
			timeout : 18000
		}).then(function(data, status, headers, config) {
			var users = data;
			deferred.resolve(users);
		}, function(errorResponse) {
			deferred.reject(errorResponse);
		})
		return deferred.promise;
	}
	;

	function sendConnectRequest(userId) {
		var deferred = $q.defer();
		$http({
			method : 'GET',
			url : API_BASE + 'ajax/send-connect-request?userId=' + userId,
			timeout : 18000
		}).then(function(data, status, headers, config) {
			var result = data;
			deferred.resolve(result);

		}, function(errorResponse) {
			deferred.reject(errorResponse);
		});

		return deferred.promise;
	}

	function acceptIgnoreConnectRequest(userId, flag) {
		var deferred = $q.defer();
		$http(
				{
					method : 'GET',
					url : API_BASE + 'ajax/accept-connect-request?userId='
							+ userId + '&isAccept=' + flag,
					timeout : 18000
				}).then(function(data, status, headers, config) {
			var result = data;
			deferred.resolve(result);

		}, function(errorResponse) {
			deferred.reject(errorResponse);
		});

		return deferred.promise;
	}

	function getCurrentLoginUser() {
		var deferred = $q.defer();
		$http({
			method : 'GET',
			url : API_BASE + 'ajax/get-current-login-user',
			timeout : 18000
		}).then(function(data, status, headers, config) {
			var result = data;
			deferred.resolve(result);

		}, function(errorResponse) {
			deferred.reject(errorResponse);
		});

		return deferred.promise;
	}

	});