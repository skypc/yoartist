/**
 * 
 */
var homeApp = angular.module('homeApp')
    .controller('message-controller', function($rootScope, messageFactory,$location, $anchorScroll) {
        var self = this;
        self.inputMessage;
        getMessageUsers();
        self.currentChatId = 0;
        self.firstChatUserId = 0;
        
        function getMessageUsers() {
            messageFactory.getMessageUser().then(
                function(response) {
                    self.messageUsers = response.data;
                    var firstUser = self.messageUsers[0];
                    self.firstChatUserId = firstUser.userId;
                    if (self.firstChatUserId > 0) {
                        self.getMessageByUser(self.firstChatUserId);
                    }

                });
        }
        
        	self.getMessageByUser = function(userId) {
            self.currentChatId = userId;
            messageFactory.getMessageUserChats(userId).then(function(response) {
                if (response.data != null && response.data != "") {
                    self.conversation = response.data;                    
                } else {
                    self.conversation = [];
                }
            });
        }
        self.sendMessageToUser = function() {
            var userId = self.currentChatId;
            var currentUser = $rootScope.currentLoginUser;
            var message = self.inputMessage;
            if (userId > 0) {
                var user = {
                    userId: currentUser.userId,
                    fullName: currentUser.fullName,
                    userImage: currentUser.userImage,
                    message: message,
                    time: "1 second ago",
                    loginUser: true
                }
                self.conversation.push(user);
                self.inputMessage = '';
                messageFactory.sendMessageToUser(userId, message);

            }
        }
    });


homeApp.directive('chatMessageList', function($timeout) {
	return {
		scope: {
			list: '=chatMessageList'
		},
		link: function(scope, element) {
			scope.$watchCollection('list', function() {
		    var $list = $(element).find('.chat-message');
			var scrollHeight = $list.prop('scrollHeight');
			$list.animate({scrollTop: scrollHeight+10000}, 500);			    
			});
		}
	};
});