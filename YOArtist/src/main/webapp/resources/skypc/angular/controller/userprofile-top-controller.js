/**
 * 
 */
var homeApp = angular
    .module('homeApp')
    .controller('userProfileTopController', function($scope, genericFactory) {
        $scope.usersProfileTopData;
        $scope.getUsersTopProfile = function() {
            genericFactory.getGenericResponse('get-user-top-profile', null).then(function(response) {
                if (response.status == 200) {
                    var result = response.data.result;

                    if (result != null) {
                        console.log(JSON.stringify(result));
                        $scope.usersProfileTopData = result;
                    }
                }
                if (response.status == 204) {
                    $scope.usersProfileTopData = null;
                }
            });
        }
        $scope.getUsersTopProfile();
    });