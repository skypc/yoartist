/**
 * 
 */

var homeApp = angular.module('homeApp');
homeApp.requires.push('720kb.datepicker');
homeApp.requires.push('ngMessages');


var profileLoadController = homeApp
.controller('profileLoadController',
		function($filter, $timeout, $mdDialog, profileFactory, PubSub) {

			var scope = this;
			scope.isEditedMode = false;
			scope.empDetails = [];
			scope.employment = {
					jobPosition: '',
					orgName: '',
					country: '',
					state: '',
					isCurrentlyWorking: true,
					startDate: '',
					endDate: ''
		
			};
	scope.deleteEmpData = function(ev, item) {
		// Appending dialog to document.body to cover sidenav in docs app
		var deleteEmpData = $mdDialog.confirm()
		.title('Confirmation')
		.textContent('Would you like to delete the entry?')
		.ariaLabel('Lucky day')
		.targetEvent(ev)
		.ok('Yes')
		.cancel('No');

		$mdDialog.show(deleteEmpData).then(function(response) {
			profileFactory.deleteEmpData(item.id).then(function(response){
				PubSub.publish('empData-modified');
			});
		}, function() {
		});
	};
	scope.updateEmpData = function(){
		scope.getEmpData();
	}

	scope.getEmpData = function(){
		profileFactory.getEmpData().then(function(response){
			scope.empDetails = response.data.result;
		});
	}

	scope.editEmpData = function(empData, ev){
		scope.employment = angular.copy(empData);
		scope.employment.isCurrentlyWorking = scope.employment.currentlyWorking;
		var dialogDetails = {
				buttonName: 'Update',
				title: 'Edit Employment Details'
		};

		$mdDialog.show({
			controller: DialogController,
			templateUrl: 'dialog1.tmpl.html',
			locals:{
				employment: scope.employment,
				dialogDetails: dialogDetails
			},
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
			fullscreen: scope.customFullscreen // Only for -xs, -sm breakpoints.
		})
		.then(function(answer) {
			scope.status = 'You said the information was "' + answer + '".';
		}, function() {
			scope.status = 'You cancelled the dialog.';
		});
	}



	scope.getEmpData();
	/* scope.minDateOfBirth = $filter('date')(
									new Date("01/01/1970"), "MM/dd/yyyy"); */

	PubSub.subscribe('empData-modified',scope.updateEmpData);


	scope.showPrompt = function(ev) {
		scope.employment = {
				jobPosition: '',
				orgName: '',
				country: '',
				state: '',
				isCurrentlyWorking: true,
				startDate: '',
				endDate: ''

		};
		var dialogDetails = {
				buttonName: 'Save',
				title: 'Add Employment Details'
		};

		$mdDialog.show({
			controller: DialogController,
			templateUrl: 'dialog1.tmpl.html',
			locals:{
				employment: scope.employment,
				dialogDetails: dialogDetails
			},
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
			fullscreen: scope.customFullscreen // Only for -xs, -sm breakpoints.
		})
		.then(function(answer) {
			scope.status = 'You said the information was "' + answer + '".';
		}, function() {
			scope.status = 'You cancelled the dialog.';
		});
	};			

	function DialogController(PubSub, $scope, $mdDialog, profileFactory, employment, dialogDetails) {
		$scope.employment = employment;
		$scope.dialogDetails = dialogDetails;
		$scope.dialogTitle = "Add Employment Details";
		$scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
				'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
		'WY').split(' ').map(function(state) {
			return {abbrev: state};
		});


		$scope.hide = function() {
			$mdDialog.hide();
		};

		$scope.cancel = function() {
			$mdDialog.cancel();
		};

		$scope.answer = function(answer) {
			$mdDialog.hide(answer);
		};
		if(employment.id != undefined && employment.id != ''){

			$scope.startDate = moment(employment.startDate,'DD-MM-YYYY').toDate();
			if(!employment.isCurrentlyWorking){
				$scope.endDate = moment(employment.endDate,'DD-MM-YYYY').toDate();
			}else{
				$scope.endDate='';
			}
		}else{
			$scope.startDate = '';
			$scope.endDate = '';

		}

		$scope.saveOrUpdate = function(){


			$scope.employment.startDate = moment($scope.startDate).format("MM/DD/YYYY");
			if(!employment.isCurrentlyWorking){
				$scope.employment.endDate = moment($scope.endDate).format("MM/DD/YYYY");
			}



			if(dialogDetails.buttonName == 'save'){
				profileFactory.saveEmploymentData($scope.employment).then(function(response){
					PubSub.publish('empData-modified');
				});	
			}else{
				profileFactory.updateEmploymentData($scope.employment).then(function(response){
					PubSub.publish('empData-modified');
				});
			}
			$mdDialog.hide(); 
		}
	}

	scope.addAboutDetails = function() {
		scope.isEditedMode = true;
	}
	scope.cancelAboutEditing = function() {
		scope.isEditedMode = false;
	}
	scope.bodyOpacity = true;
	$timeout(function() {
		scope.bodyOpacity = false
	}, 2000);

	scope.getNotifier = function(messageType) {
		var messageType = messageType;
		var profileReady = $
		.notify(
				{
					// options
					message : "<strong>Getting</strong> your profile ready.",
				}, {
					placement : {
						align : "right",
					},
					offset : {
						x : 200,
						y : 70
					},
					type : messageType,
					allow_dismiss : false,
					showProgressbar : true,
					delay : 2000,
				});

		setTimeout(
				function() {
					profileReady
					.update('message',
					'<strong>Getting</strong> Profile data.');
				}, 1000);

		setTimeout(
				function() {
					profileReady
					.update('message',
					'<strong>Getting</strong> User Data.');
				}, 2000);

	}
	scope.getNotifier('info');
});