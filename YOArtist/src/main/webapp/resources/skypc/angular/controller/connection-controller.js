/**
 * 
 */
var homeApp = angular.module('homeApp').controller(
    "connection-controller",
    function(connectionFactory, userFactory, $interval,$rootScope) {
        var self = this;
        self.currentSelectedUserId = 0;
        self.clickFlag = false;
        getConnectionsRequestCount();
        $rootScope.connections = [];
        getUsersConnection();
        
        function getUsersConnection(){
        	connectionFactory.getUserConnection().then(
        		function(response){
        			$rootScope.connections = response.data.result;
       			}
        	)
        }
        
        function getConnectionsRequestCount() {
            connectionFactory.getConnectionsRequestCount().then(
                function(response) {
                    var count = response.data;
                    if (count > 0) {
                        self.connectionCount = count;
                    }

                });
        }
        $interval(getConnectionsRequestCount, 1000 * 30);
        self.isEmpty = false;
        self.getAllConnectionRequest = function() {
            if (!self.clickFlag ||
                (self.connectionCount && self.connectionCount > 0)) {
                getConnectionRequests();
            }

        }

        function getConnectionRequests() {
            self.isLoading = true;
            connectionFactory.getAllConnectionsRequest().then(
                function(response) {
                    if (response.data == "") {
                        self.isEmpty = true;
                    } else {
                        self.isEmpty = false;
                        self.connections = response.data;
                    }
                    self.connectionCount = '';
                    self.clickFlag = true;
                    self.isLoading = false;
                });
        }

        self.acceptIgnoreConnectRequest = function(userId, flag) {
            self.isLoading = true;
            $("#connectionDropdown").dropdown("toggle");
            userFactory.acceptIgnoreConnectRequest(userId, flag).then(
                function(response) {
                    if (response.data == "1") {
                        self.currentSelectedUserId = userId;
                        self.isLoading = false;
                    } else if (response.data == "2") {
                        self.currentSelectedUserId = userId;
                        self.isLoading = false;
                    } else {
                        self.isLoading = false;
                    }
                });
        }
    });