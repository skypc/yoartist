/**
 * 
 */
var homeApp = angular
    .module('homeApp')
    .controller('profileController', function(genericFactory,$uibModal, $log, $document) {
    	//$self.userDetailsData;
        var self=this;
       self.education;
       self.employement;

       self.items = ['item1', 'item2', 'item3'];

  self.animationsEnabled = true;

  self.openEduModal = function (type,eduObj,size, parentSelector) {
    if(type=='add'){
      self.education;
    }
    else{
     self.education=eduObj;
    }
    var parentElem = parentSelector ?
    angular.element($document[0].querySelector('.edu-modal-demo' + parentSelector)) : undefined;
    var modalInstance = $uibModal.open({
      animation: self.animationsEnabled,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'eduModal.html',
      controller: 'ModalInstanceCtrl',
      controllerAs: '$ctrl',
      appendTo: parentElem,
      resolve: {
      items: function () {
          return self.education;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      self.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  self.toggleAnimation = function () {
    self.animationsEnabled = !self.animationsEnabled;
  };

        self.getUserDetailsData = function() {
            genericFactory.getGenericResponse('user-details-data',null).then(function(response) {
            	var result=response.data;
            	if(result==null || result.length>0){
            		$self.usersActivity=result;
            	}
                 
            });
        }

       self.addEducation=function(){
            $self.education=null;
       }
       self.submitEducation=function(obj){
           console.log(obj);
           $self.dismissModel();
       }

    });

angular.module('homeApp').controller('ModalInstanceCtrl', function ($uibModalInstance, items,profileFactory) {
  var $ctrl = this;
  

  $ctrl.ok = function (education) {
    console.log(education);
    profileFactory.saveEducationData(education)
    	.then(function(response){
    		if(response.status==200)
    			$uibModalInstance.close("item");
    		else
    			console.log("error");
    	});
    
  };

  $ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
