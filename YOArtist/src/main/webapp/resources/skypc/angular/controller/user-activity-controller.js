/**
 * 
 */
var homeApp = angular
    .module('homeApp')
    .controller('userActivityController', function($scope, genericFactory) {
    	$scope.usersActivity;
        $scope.getUsersActivity = function() {
        	var param='isFirst=true';
            genericFactory.getGenericResponse('connection-activity',param).then(function(response) {
            	var result=response.data;
            	if(result==null || result.length>0){
            		$scope.usersActivity=result;
            	}
                 
            });
        }
        $scope.getUsersActivity();
    });