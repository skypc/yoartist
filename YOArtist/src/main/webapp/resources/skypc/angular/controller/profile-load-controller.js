/**
 * 
 */
/**
 * 
 */

var homeApp = angular.module('homeApp');
homeApp.requires.push('720kb.datepicker');
homeApp.requires.push('ngMessages');


var profileLoadController = homeApp
.controller('profileLoadController',
		function($filter, $timeout, $mdDialog, profileFactory, PubSub) {

			var scope = this;
			scope.isEditedMode = false;
			scope.empDetails = [];
			scope.employment = {
					jobPosition: '',
					orgName: '',
					country: '',
					state: '',
					isCurrentlyWorking: true,
					startDate: '',
					endDate: ''
		
			};
			
			scope.deleteSummaryData = function(ev, item){
				var deleteSummaryData = $mdDialog.confirm()
				.title('Confirmation')
				.textContent('Would you like to delete the entry?')
				.ariaLabel('Delete')
				.targetEvent(ev)
				.ok('Yes')
				.cancel('No');
		
				$mdDialog.show(deleteSummaryData).then(function(response) {
					profileFactory.deleteSummaryData(item.id).then(function(response){
						PubSub.publish('summary-modified');
					});
				}, function() {
				});
			}
			
			scope.deleteEmpData = function(ev, item) {
				// Appending dialog to document.body to cover sidenav in docs app
				var deleteEmpData = $mdDialog.confirm()
				.title('Confirmation')
				.textContent('Would you like to delete the entry?')
				.ariaLabel('Lucky day')
				.targetEvent(ev)
				.ok('Yes')
				.cancel('No');
		
				$mdDialog.show(deleteEmpData).then(function(response) {
					profileFactory.deleteEmpData(item.id).then(function(response){
						PubSub.publish('empData-modified');
					});
				}, function() {
				});
			};
	
			scope.updateEmpData = function(){
				scope.getEmpData();
			}
			
			scope.updateSummaryData = function(){
				scope.getSummaryData();
			}
			
			scope.getEmpData = function(){
				profileFactory.getEmpData().then(function(response){
					scope.empDetails = response.data.result;
				});
			}
			
			scope.getSummaryData = function(){
				profileFactory.getSummaryData().then(function(response){
					scope.summaryDataDetails = response.data.result;
				});
			}
			
			scope.editSummaryData = function(summaryData, ev){
				
				scope.profile = angular.copy(summaryData);
				var summaryDetails = {
						buttonName: 'Update',
						title: 'Edit Summary Details'
				};
		
				$mdDialog.show({
					controller: SummaryDialogController,
					templateUrl: 'summary.dialog.html',
					locals:{
						profile: scope.profile,
						dialogDetails: summaryDetails
					},
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose:true,
					fullscreen: scope.customFullscreen // Only for -xs, -sm breakpoints.
				})
				.then(function(answer) {
				}, function() {
				});
			}
			
			scope.editEmpData = function(empData, ev){
				scope.employment = angular.copy(empData);
				scope.employment.isCurrentlyWorking = scope.employment.currentlyWorking;
				var dialogDetails = {
						buttonName: 'Update',
						title: 'Edit Employment Details'
				};
		
				$mdDialog.show({
					controller: DialogController,
					templateUrl: 'dialog1.tmpl.html',
					locals:{
						employment: scope.employment,
						dialogDetails: dialogDetails
					},
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose:true,
					fullscreen: scope.customFullscreen // Only for -xs, -sm breakpoints.
				})
				.then(function(answer) {
				}, function() {
				});
			}



	scope.getEmpData();
	scope.getSummaryData();

	PubSub.subscribe('empData-modified',scope.updateEmpData);
	PubSub.subscribe('summary-modified',scope.updateSummaryData);
	
	
	scope.saveUpdateSummaryModal = function(ev){
		
		var summaryDetails = {
				buttonName: 'Save',
				title: 'Add Summary Details'
		};
		
		scope.profile = {summary:''};
		
		$mdDialog.show({
			controller: SummaryDialogController,
			templateUrl: 'summary.dialog.html',
			locals:{
				dialogDetails: summaryDetails,
				profile: scope.profile
			},
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
			fullscreen: scope.customFullscreen // Only for -xs, -sm breakpoints.
		})
		.then(function(answer) {
		}, function() {
		});
	}
	
	scope.saveUpdateModal = function(ev) {
		
		scope.employment = {
				jobPosition: '',
				orgName: '',
				country: '',
				state: '',
				isCurrentlyWorking: true,
				startDate: '',
				endDate: ''

		};
		var dialogDetails = {
				buttonName: 'Save',
				title: 'Add Employment Details'
		};

		$mdDialog.show({
			controller: DialogController,
			templateUrl: 'dialog1.tmpl.html',
			locals:{
				employment: scope.employment,
				dialogDetails: dialogDetails
			},
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
			fullscreen: scope.customFullscreen // Only for -xs, -sm breakpoints.
		})
		.then(function(answer) {
		}, function() {
		});
	};			
	
	function SummaryDialogController(PubSub, $scope, $mdDialog, profileFactory, dialogDetails, profile){
		
		$scope.profile = profile;
		$scope.summaryDetails = dialogDetails;
		$scope.hide = function() {
			$mdDialog.hide();
		};

		$scope.cancel = function() {
			$mdDialog.cancel();
		};

		$scope.answer = function(answer) {
			$mdDialog.hide(answer);
		};
		
		$scope.saveOrUpdateSummary = function(){

			if(dialogDetails.buttonName == 'Save'){
				profileFactory.saveSummary($scope.profile).then(function(response){
					PubSub.publish('summary-modified');
				});	
			}else{
				profileFactory.updateSummaryDetails($scope.profile).then(function(response){
					PubSub.publish('summary-modified');
				});
			}
			$mdDialog.hide(); 
		}
	}
	
	function DialogController(PubSub, $scope, $mdDialog, profileFactory, employment, dialogDetails) {
		$scope.employment = employment;
		$scope.dialogDetails = dialogDetails;
		$scope.dialogTitle = "Add Employment Details";
		
		$scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
				'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
		'WY').split(' ').map(function(state) {
			return {abbrev: state};
		});


		$scope.hide = function() {
			$mdDialog.hide();
		};

		$scope.cancel = function() {
			$mdDialog.cancel();
		};

		$scope.answer = function(answer) {
			$mdDialog.hide(answer);
		};
		
		if(employment.id != undefined && employment.id != ''){

			$scope.startDate = moment(employment.startDate,'DD-MM-YYYY').toDate();
			if(!employment.isCurrentlyWorking){
				$scope.endDate = moment(employment.endDate,'DD-MM-YYYY').toDate();
			}else{
				$scope.endDate='';
			}
		}else{
			$scope.startDate = '';
			$scope.endDate = '';

		}

		$scope.saveOrUpdate = function(){


			$scope.employment.startDate = moment($scope.startDate).format("MM/DD/YYYY");
			if(!employment.isCurrentlyWorking){
				$scope.employment.endDate = moment($scope.endDate).format("MM/DD/YYYY");
			}

			if(dialogDetails.buttonName == 'Save'){
				profileFactory.saveEmploymentData($scope.employment).then(function(response){
					PubSub.publish('empData-modified');
				});	
			}else{
				profileFactory.updateEmploymentData($scope.employment).then(function(response){
					PubSub.publish('empData-modified');
				});
			}
			$mdDialog.hide(); 
		}
	}

	scope.addAboutDetails = function() {
		scope.isEditedMode = true;
	}
	scope.cancelAboutEditing = function() {
		scope.isEditedMode = false;
	}
	scope.bodyOpacity = true;
	$timeout(function() {
		scope.bodyOpacity = false
	}, 2000);

	scope.getNotifier = function(messageType) {
		var messageType = messageType;
		var profileReady = $
		.notify(
				{
					// options
					message : "<strong>Getting</strong> your profile ready.",
				}, {
					placement : {
						align : "right",
					},
					offset : {
						x : 200,
						y : 70
					},
					type : messageType,
					allow_dismiss : false,
					showProgressbar : true,
					delay : 2000,
				});

		setTimeout(
				function() {
					profileReady
					.update('message',
					'<strong>Getting</strong> Profile data.');
				}, 1000);

		setTimeout(
				function() {
					profileReady
					.update('message',
					'<strong>Getting</strong> User Data.');
				}, 2000);

	}
	scope.getNotifier('info');
});