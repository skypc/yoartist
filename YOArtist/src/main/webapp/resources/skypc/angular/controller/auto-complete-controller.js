angular.module('homeApp')
.controller('searchBarCtrl', function($scope, $http,userFactory,API_BASE,$log) {
  var _selected;
  usersForSearchBox();
  function usersForSearchBox() {
		userFactory.getUsersForSearchBox().then(function(response) {
			$scope.usersForSearchBox = response.data;
		});
	}
  $scope.onSelect=function($item){
	  	$log.debug($item.userId);
	    $scope.autoSelectInput=$item.userName;
	    
  };
	  
  
});