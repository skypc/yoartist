/**
 * 
 */
var homeApp = angular.module("homeApp").controller('share-modal-controller',
    function($scope, ModalService, $attrs, $rootScope, connectionFactory, notifyAlertService,API_BASE) {
        var instance = this;
        instance.openShareModal = function(item) {
            var itemPostId = item.target.attributes.postid.value;
            var postDesc = item.target.attributes.postDesc.value;
            var postFilePath = item.target.attributes.postFilePath.value;
            ModalService.showModal({
                templateUrl: 'share-modal.html',
                controller: "share-post-controller",
                inputs: {
                    connections: $rootScope.connections,
                    postId: itemPostId,
                    postDesc: postDesc,
                    postFilePath: postFilePath,
                }
            }).then(function(modal) {

                modal.element.modal();
                modal.close.then(function(result) {
                    if (!angular.isUndefined(result.postId)) {
                        connectionFactory.sharePost(result.postId, result.sharedConnections);
                        notifyAlertService.generateNotification('success', '', "post shared successfully");
                    }
                });
            });
        };
    });
homeApp.controller('share-post-controller', function(postId, $scope, close, connections, $q, $timeout, postDesc, postFilePath,API_BASE) {

    var pendingSearch, cancelSearch = angular.noop;
    var cachedQuery, lastSearch;

    for (var i = 0; i < connections.length; i++) {
        if (connections[i].userImage == null || connections[i].userImage == undefined && connections[i].userImage == '') {
            connections[i].userImage = DEFAULT_USER_IMAGE;
        }
    }
    $scope.allContacts = connections;
    $scope.postDesc = postDesc;
    if (postFilePath != "") {
        $scope.postFilePath = API_BASE+postFilePath;
    }

    $scope.isShareDisabled = false;


    $scope.share = { type: 'public' };

    $scope.connectionToShare = [];
    $scope.filterSelected = false;

    $scope.querySearch = querySearch;
    $scope.delayedQuerySearch = delayedQuerySearch;
    $scope.userConnections = connections;

    $scope.changeShareType = function() {
        if ($scope.share.type == 'custom') {
            $scope.isShareDisabled = true;
        } else {
            $scope.isShareDisabled = false;
        }
    }

    $scope.sharePost = function(shareType) {
        close({
            postId: postId,
            sharedConnections: $scope.connectionToShare,
        }, 500);
    }

    /**
     * Search for contacts; use a random delay to simulate a remote call
     */
    function querySearch(criteria) {
        cachedQuery = cachedQuery || criteria;
        return cachedQuery ? $scope.allContacts.filter(createFilterFor(cachedQuery)) : [];
    }

    /**
     * Async search for contacts
     * Also debounce the queries; since the md-contact-chips does not support this
     */
    function delayedQuerySearch(criteria) {
        cachedQuery = criteria;
        if (!pendingSearch || !debounceSearch()) {
            cancelSearch();

            return pendingSearch = $q(function(resolve, reject) {
                // Simulate async search... (after debouncing)
                cancelSearch = reject;
                $timeout(function() {

                    resolve($scope.querySearch());

                    refreshDebounce();
                }, Math.random() * 500, true)
            });
        }

        return pendingSearch;
    }

    function refreshDebounce() {
        lastSearch = 0;
        pendingSearch = null;
        cancelSearch = angular.noop;
    }

    /**
     * Debounce if querying faster than 300ms
     */
    function debounceSearch() {
        var now = new Date().getMilliseconds();
        lastSearch = lastSearch || now;

        return ((now - lastSearch) < 300);
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);

        return function filterFn(contact) {
            return (contact.userName.toLowerCase().indexOf(lowercaseQuery) != -1);
        };

    }





    $scope.close = function(result) {
        close(result, 500); // close, but give 500ms for bootstrap to animate
    };

});