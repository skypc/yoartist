/**
 * 
 */
var homeApp = angular
    .module('homeApp')
    .controller('connectController', function($scope, $http, userFactory, $timeout, notifyAlertService) {
        $scope.opacitiyStyle = "opacity:0.2;";
        $scope.sentFriendReq = false;
        $scope.currentSelectedUserId = 0;
        getUsersForConnections();
        $scope.sendConnectRequest = function(userId) {
            userFactory.sendConnectRequest(userId)
                .then(function(response) {
                    if (response.data == "1") {
                        //alert notify
                        notifyAlertService.generateNotification('success', '', "connect request sent successfully");
                        $scope.opacitiyStyle = "opacity:0.2;"
                        $scope.currentSelectedUserId = userId;
                        //getUsersForConnections();

                    } else {

                    }
                });
        }

        function getUsersForConnections() {
            userFactory.getUserToConnect().then(function(response) {
                $scope.users = response.data;
                $scope.opacitiyStyle = "opacity:1;"
            });
        }




    });