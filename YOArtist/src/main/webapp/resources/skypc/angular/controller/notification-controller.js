/**
 * 
 */
var homeApp = angular
    .module('homeApp')
    .controller(
        "notification-controller",
        function($rootScope,notificationFactory, userFactory, $interval, $http, API_BASE) {
            var self = this;
            self.clickFlag = false;
            getNotificationCount();
            getcurrentLoginUser();

            function getcurrentLoginUser() {
                $http.get(API_BASE + "ajax/get-current-login-user")
                    .success(function(data) {
                        var currentLoginUser = data;
                        $rootScope.currentLoginUser=currentLoginUser;

                    });
            }

            function getNotificationCount() {
                notificationFactory.getNotificationCount().then(
                    function(response) {
                        var count = response.data;
                        if (count > 0) {
                            self.notificationCount = count;
                        }

                    });
            }
            $interval(getNotificationCount, 1000 * 30);
            self.isEmpty = false;
            self.getLimitedNotification = function() {
                if (!self.clickFlag ||
                    (self.notificationCount && self.notificationCount > 0)) {
                    self.isLoading = true;
                    notificationFactory.getLimitedNotification().then(
                        function(response) {
                            if (response.data == "") {
                                self.isEmpty = true;
                            } else {
                                self.isEmpty = false;
                                self.notifications = response.data;

                            }
                            self.notificationCount = '';
                            self.clickFlag = true;
                            self.isLoading = false;

                        });
                }
            }

            self.acceptIgnoreConnectRequest = function(userId, flag) {
                self.isLoading = true;
                userFactory
                    .acceptIgnoreConnectRequest(userId, flag)
                    .then(
                        function(response) {
                            if (response.data == "1") {
                                var connectToastMsg = ngToast
                                    .success({
                                        content: '<a href="#" class="">request accepted successfully</a>'
                                    });
                                self.getLimitedNotification
                                self.isLoading = false;
                                $timeout(
                                    function() {
                                        ngToast
                                            .dismiss(connectToastMsg);
                                    }, 2000);
                            } else if (response.data == "2") {
                                var connectToastMsg = ngToast
                                    .success({
                                        content: '<a href="#" class="">request decline successfully</a>'
                                    });
                                self.getLimitedNotification
                                self.isLoading = false;
                                $timeout(
                                    function() {
                                        ngToast
                                            .dismiss(connectToastMsg);
                                    }, 2000);
                            } else {
                                var connectToastMsg = ngToast
                                    .success({
                                        content: '<a href="#" class="">Error occured while processing connect...</a>'
                                    });
                                self.isLoading = false;
                                $timeout(
                                    function() {
                                        ngToast
                                            .dismiss(connectToastMsg);
                                    }, 2000);
                            }
                        });
            }

        });