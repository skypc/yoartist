var homeApp = angular
    .module('homeApp')
    .filter('contextPathFilter', function(API_BASE) {
        return function(description) {
            if (description.indexOf('#contextPath#') > -1) {
                var result=description.replace(new RegExp('#contextPath#/', 'g'), API_BASE);
                if((result.indexOf("like")>-1) && (result.indexOf("post")>-1)){
                	result=result.replace('>post', '><b style="color:#2dc3e8;">post</b>');
                }
                return result;
            } else {
                return description;
            }
        }
    });