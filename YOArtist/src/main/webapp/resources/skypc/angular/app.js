/**
 * 
 */

var homeApp = angular.module("homeApp", ['ngAnimate', 'ngSanitize', 'ui.bootstrap','ngMaterial','angularModalService', 'PubSub']);

homeApp.constant('API_BASE', '/yoartist/');
homeApp.run(function($rootScope, API_BASE) {
    $rootScope.API_BASE = API_BASE;
});


homeApp.config(function($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function(date) {
    	if(date!=''){
    		return moment(date).format('DD-MM-YYYY');
    	}
    	else{
    		return '';
    	}
       
    };
    
    /*$mdDateLocaleProvider.parseDate = function(dateString) {
    	if(dateString!=''){
    		var m = moment(dateString, 'DD-MM-YYYY', true);
      	  	return m.isValid() ? m.toDate() : new Date(NaN);
    	}
    	  
    	};*/
});
/*
 * headerApp.constant('API_BASE', '/yoartist/');
 * 
 * angular.bootstrap(document.getElementById('homeApplication',['homeApp']));
 */