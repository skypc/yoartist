/**
 * 
 */
// Validation messages
const FIRST_NAME_REQUIRED = "Please enter your full name.";
const EMAIL_ADDRESS_REQUIRED = "Please enter your email address.";
const EMAIL_EXISTS = "Email address already exists in the system.";
const INVALID_EMAIL_ADDRESS = "Please enter valid email address.";
const MAX_LENGTH_EMAIL_REQUIRED = "Please enter a value less than or equal to 64 characters long.";
const PASSWORD_REQUIRED = "Please enter the password.";
const PASSWORD_MISMATCHED = "Your password and confirmation password do not match.";
const CONFIRM_PASSWORD_REQUIRED = "Please re-enter the password.";
const SPACE_NOT_ALLOWED = "Space not allowed";
const TRUE = true;
const FALSE = false;
// regex
const EMAIL_REGEX = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
const REGEX_WITHOUT_SPACE = /^\S*$/;

// form ids
const USER_REGISTRATION_FORM_ID = "#registerUser-form";

function validateLogin(){
	
	var emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	
	var email=$("#loginEmail").prop('value');
	var loginPassword=$("#loginPassword").prop('value');
	
	if(email=="" && loginPassword==""){
		$("#loginAlert").css('color','red')
		$("#loginAlert").text("Please enter email address and password");
		return false;
	}

	else if(email==""){
		$("#loginAlert").css('color','red')
		$("#loginAlert").text("Please enter email address");
		return false;
	}

	else if(!emailRegex.test(email)){
		$("#loginAlert").css('color','red')
		$("#loginAlert").text("invalid email address");
		
		return false;
	}

	else if(loginPassword==""){
		$("#loginAlert").css('color','red')
		$("#loginAlert").text("Please enter password");
		return false;
	}	

	else if(loginPassword.length<6){
		$("#loginAlert").css('color','red')
		$("#loginAlert").text("Password length should be min 6 character");
		return false;
	}
	return true;
}


$(function() {

	$(USER_REGISTRATION_FORM_ID).validate({
		/** **defines rules to validate the form**** */
		rules : {

			fullName : {
				required : true,
				regex : /^[a-zA-Z ]*$/,
				rangelength : [ 1, 64 ],
			},

			email : {
				required : true,
				emailAddressValidator : true,
				//isEmailExistsValidator : true,
				maxLengthEmailAddressValidator : true,
			},
			password : {
				required : true,
				regex : REGEX_WITHOUT_SPACE,
				rangelength : [ 6, 64 ],
			},
			confirmPassword : {
				required : true,
				regex : REGEX_WITHOUT_SPACE,
				equalTo : "#password",
				rangelength : [ 6, 64 ],
			},

		},

		messages : {
			fullName : {
				required : FIRST_NAME_REQUIRED,
				regex : "Allowed alphabets only"
			},
			email : {
				required : EMAIL_ADDRESS_REQUIRED,
				emailAddressValidator : INVALID_EMAIL_ADDRESS,
				maxLengthEmailAddressValidator : MAX_LENGTH_EMAIL_REQUIRED,
				isEmailExistsValidator : EMAIL_EXISTS,
			},
			password : {
				required : PASSWORD_REQUIRED,
				regex : SPACE_NOT_ALLOWED,
			},
			confirmPassword : {
				required : CONFIRM_PASSWORD_REQUIRED,
				regex : SPACE_NOT_ALLOWED,
				equalTo : PASSWORD_MISMATCHED,
			}

		},
		/** **to place error to custom div ** */
		errorPlacement : function(error, element) {
			if (element.attr("name") == "fullName") {
				error.appendTo($("#fullNameErrorDiv")); // append
				// error to
				// user
				// define
				// div
				// #nameError
			}

			if (element.attr("name") == "email") {
				error.appendTo($("#emailErrorDiv")); // append
				// error
				// to user
				// define div
				// #nameError
			}
			if (element.attr("name") == "password") {
				error.appendTo($("#passwordErrorDiv")); // append
				// error
				// to user
				// define div
				// #nameError
			}
			if (element.attr("name") == "confirmPassword") {
				error.appendTo($("#confirmPasswordErrorDiv")); // append
				// error
				// to
				// user
				// define
				// div
				// #nameError
			}

		},

		submitHandler : function(form) {

			$("#user-register-button").attr("disabled", "disabled");
			form.submit();
		}
	});

});

$(document).ready(function() {
	$.validator.addMethod("emailAddressValidator", function(value, element,
			isValid) {
		var email = $("#email").val().trim();
		
		if (email != "") {
			return EMAIL_REGEX.test(email);
		}
		return TRUE;

	}, "");

	$.validator.addMethod("maxLengthEmailAddressValidator", function(value,
			element, isValid) {
		var email = $("#email").val().trim();
		if (email.length <= 64) {
			return TRUE;
		}
		return FALSE;

	}, "");

	$.validator.addMethod("isEmailExistsValidator", function(value, element,
			isValid) {
		var email = $("#email").val().trim();
		rootPath = $("#rootPath").val().trim();
		serverStatus = "0";

		$.ajax({
			type : "POST",
			url : rootPath + "/checkEmailExistence",
			async : false,
			timeout : 100000,
			data : {
				"emailAddress" : email,
			},
			success : function(data) {
				serverStatus = data;
			},
			error : function(e) {
				console.log("ERROR: ", e);

			},
			done : function(e) {
				console.log("DONE");
			}
		});

		if (serverStatus == "0" || serverStatus == 0) {
			return false;
		} else {
			return true;
		}

	}, "");

	$.validator.addMethod("regex", function(value, element, regexpr) {

		return regexpr.test(value);

	}, "");
});
