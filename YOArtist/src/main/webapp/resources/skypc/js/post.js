/*search box*/
$('.search_button').click(function() {
if ($('.search_input').val() == '') 
    {
    $('.search_box').toggleClass("start");
    if (!$(this).is(":focus")){
      setTimeout(function(){
        $('.search_input').focus();
      }, 920);
    }
}
else {
  $('.search_box').addClass("finish");
}
});
/*search box*/

$("#uploadFile").fileinput({
		uploadAsync : true,
		allowedFileExtensions : [ 'jpg', 'png', 'gif', 'mp4' ],
		showUploadedThumbs : false,

	});
	var files;
	$('#uploadFile').on('change', prepareUpload);
	// Grab the files and set them to our variable
	function prepareUpload(event) {
		files = event.target.files;
	}
	$('.fileinput-upload-button').on('click', function(event) {
		event.preventDefault();
		var upload = $(".fileinput-upload-button");

		if (upload.prop('disabled') == false) {

			var fd = new FormData();
			var uploadFile = files[0];
			var fileName = uploadFile.name;
			fd.append("uploadFile", uploadFile);
			fd.append("type", "uploadFromHomePost");
			$.ajax({
				url : $("#rootPath").val() + "/ajax/uerPostMedia",
				data : fd,
				async : false,
				processData : false,
				contentType : false,
				type : 'POST',
				success : function(data) {
					var value = parseInt(data);

					if (value > 0) {

						$("#uploadModal").modal('hide');
						$("#uploadDiv").css("display", 'none');
						$("#uploadedDataUI").css("display", 'block');
						$("#uploadedData").text(fileName);
						$("#removeLink").prop("href", value);
						$("#uploadedId").val(value);
						$("#errorDiv").css("display", 'none');

					} else {
						$("#uploadModal").model('hide');
						$("#errorDiv").css("display", 'block');
						$("#errorMsg").text("Media not uploaded");

					}
				}
			});

		}
		return false;
	});
	function deleteMedia() {
		var id = $("#removeLink").prop("href");
		$("#removeLink").css('pointer-events', 'none');
		$.ajax({
			url : $("#rootPath").val() + "/ajax/deleteUploadedMedia?id=" + id,
			type : 'POST',
			success : function(data) {
				var value = parseInt(data);
				if (value > 0) {
					$("#uploadedData").text("");
					$("#removeLink").prop("href", "");
					$("#uploadedDataUI").css("display", 'none');
					$("#removeLink").css('pointer-events', 'all');
					$("#uploadedId").val("0");
					$("#uploadDiv").css("display", 'block');

				} else {
					$("#errorDiv").css("display", 'block');
					$("#errorMsg").text("Media not removed");
					$("#removeLink").css('pointer-events', 'all');

				}
			}
		});

		$("#removeLink").css('pointer-events', 'all');
		return false;
	}
	function uploadPost() {

		var flag = false;
		var comments = $("#comments").val();
		var uploadedId1 = $("#uploadedId").val();
		if (comments != "" || uploadedId1 != "0") {
			$("#errorMsg").text("");
			flag = true;
			;
		} else {

			$("#errorMsg").text("Please enter text in post box or upload photo/video");
			$("#errorDiv").css("display", 'block');
		}
		return flag;
	}
	
	function like(postId){
		var className=$("#likeIcon_"+postId).prop('class');
		var likeCount;
		if($("#likeCount_"+postId).length>0)
			likeCount=parseInt($("#likeCount_"+postId).text());
		
		if(className.indexOf('fa-thumbs-o-up')>-1){
			$("#likeIcon_"+postId).removeClass('fa-thumbs-o-up');
			$("#likeIcon_"+postId).addClass('fa-thumbs-o-down');
			$("#likeText_"+postId).text('Unlike');
			$("#likeStatus_"+postId).text('you like this post');
			
			
			if(likeCount!=null){
				$("#likeCount_"+postId).text((likeCount+1)+' Likes -');
			}else{
				$("#likeCount_"+postId).text(1);
			}
		}else{
			$("#likeIcon_"+postId).removeClass('fa-thumbs-o-down');
			$("#likeIcon_"+postId).addClass('fa-thumbs-o-up');
			$("#likeCount_"+postId).text((likeCount-1)+' Likes -');
			$("#likeText_"+postId).text('Like');
			$("#likeStatus_"+postId).text('');
		}
		$.ajax({
			url : $("#rootPath").val() + "/ajax/likePost?postId=" + postId,
			type : 'POST',
			success : function(data) {
			
			}
		});
	}