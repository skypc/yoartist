<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="musaddique">
<link rel="icon" href="img/favicon.png">
<title>Login</title>
<!-- Bootstrap core CSS -->

<link rel="stylesheet"
	href="<c:url value='/resources/support/bootstrap.3.3.6/css/bootstrap.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/font-awesome.4.6.1/css/font-awesome.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/animate.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/timeline.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/login_register.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/forms.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/buttons.css'/>">
<link href="<c:url value='/resources/skypc/css/common-user.css'/>"
	rel="stylesheet" />

<style type="text/css">
.alert{
text-align: center;
}
</style>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
</head>
<body>
	<!-- Fixed navbar -->
	<nav class="navbar navbar-fixed-top navbar-transparent"
		role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button id="menu-toggle" type="button" class="navbar-toggle">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar bar1"></span> <span class="icon-bar bar2"></span> <span
					class="icon-bar bar3"></span>
			</button>
			<!-- <a class="navbar-brand" href="profile.html">Day-Day</a> -->
		</div>
	</div>
	</nav>
	<div class="wrapper">
		<div class="parallax filter-black">
			<div class="parallax-image"></div>
			<div class="small-info">
				
				<div
					class="col-sm-10 col-sm-push-1 col-md-6 col-md-push-3 col-lg-6 col-lg-push-3">
					<c:if test="${not empty MESSAGE}">
					<c:if test="${CODE eq 1001}">
						<%-- <div class="form-group alertMessage">
							<div class="alert alert-danger">
								<a href="#" class="close" data-dismiss="alert"
									aria-label="close">&times;</a> <strong>Alert!</strong>
								${MESSAGE}
							</div>
						</div> --%>
					</c:if>
					<input type="hidden" value="${CODE }" id="errorCode"/>
					<input type="hidden" value="${MESSAGE }" id="errorMessage"/>
					
					<c:if test="${CODE eq 1000}">
						<%-- <div class="form-group alertMessage">
							<div class="alert alert-success">
								<a href="#" class="close" data-dismiss="alert"
									aria-label="close">&times;</a> <strong>Success!</strong>
								${MESSAGE}
							</div>
						</div> --%>
					</c:if>
				</c:if>
					<div class="card-group animated flipInX">
						<div class="card">
							<div class="card-block">
								<div class="center">
									<h4 class="m-b-0">
										<span class="icon-text">Login</span>
									</h4>
									<c:if test="${not empty loginErrorMsg}">
										<p class="text-muted" style="color: red;" id="loginAlert">
											<c:out value="${loginErrorMsg}"></c:out>
										</p>
									</c:if>
									<c:if test="${not empty resetPassSuccessMsg}">
										<p class="text-muted" style="color: green;" id="loginAlert">${resetPassSuccessMsg}</p>
									</c:if>

									<c:if test="${empty loginErrorMsg}">
										<p class="text-muted" id="loginAlert">Sign in to start
											your session</p>
									</c:if>

								</div>
								<form action="${pageContext.request.contextPath}/user/signin"
									method="post" novalidate>
									<div class="form-group">
										<input type="email" class="form-control"
											placeholder="Email Address" id="loginEmail" name="loginEmail"
											value="${cookie.loginName.value }">
									</div>
									<div class="form-group">
										<input type="password" class="form-control"
											placeholder="Password" id="loginPassword"
											name="loginPassword" value="${cookie.loginPass.value}">
										<a href="#" class="pull-xs-right"> <small>Forgot
												password?</small>
										</a>
										<div class="clearfix"></div>
									</div>
									<div class="row">
										<div class="col-xs-8">
											<div class="checkbox">
												<c:set var="checkBoxStatus" value=""></c:set>
												<c:if test="${(cookie.checkbox.value==1)}">
													<c:set var="checkBoxStatus" value="checked"></c:set>
												</c:if>
												<label> <input type="checkbox" class="colored-blue"
													name="rememberMe" value="on" ${checkBoxStatus}> <span
													class="text">Remember Me</span>
												</label>
											</div>
										</div>

										<!-- /.col -->
										<div class="col-xs-4">
											<button type="submit" class="btn  btn-azure pull-right"
												onclick="return validateLogin();">Login</button>
										</div>

									</div>
								</form>
							</div>
						</div>
						<div class="card">
							<div class="card-block center">
								<h4 class="m-b-0">
									<span class="icon-text">Sign Up</span>
								</h4>

								<p class="text-muted">Create a new account</p>
								<form action="user/register" id="registerUser-form"
									method="post" novalidate>
									<div class="form-group">
										<input type="text" class="form-control"
											placeholder="Full Name" id="fullName" name="fullName">
										<div id="fullNameErrorDiv" class="errorDiv"></div>
									</div>
									<div class="form-group">
										<input type="email" class="form-control" placeholder="Email"
											id="email" name="email">
										<div id="emailErrorDiv" class="errorDiv"></div>
									</div>
									<div class="form-group">
										<input type="password" class="form-control"
											placeholder="Password" id="password" name="password">
										<div id="passwordErrorDiv" class="errorDiv"></div>
									</div>
									<div class="form-group">
										<input type="password" class="form-control"
											placeholder="Confirm Password" id="confirmPassword"
											name="confirmPassword">
										<div id="confirmPasswordErrorDiv" class="errorDiv"></div>
									</div>
									<button type="submit" id="user-register-button"
										class="btn btn-azure">Register</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<footer class="footer">
		<div class="container">
			<p class="text-muted">Copyright &copy; Company - All rights
				reserved</p>
		</div>
		</footer>
	</div>
</body>
<script
	src="<c:url value='/resources/support/assets/js/jquery-2.1.3.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/bootstrap.3.3.6/js/bootstrap.min.js'/>"></script>
<script 
	src="<c:url value='/resources/support/js/bootstrap-notify.js'/>"></script>
<script
	src="<c:url value='/resources/support/js/jquery.validate.min.js'/>"></script>
<script src="<c:url value='/resources/support/assets/js/custom.js'/>"></script>
<script src="<c:url value='/resources/skypc/js/registration-login.js'/>"></script>

<script type="text/javascript">

$(document).ready(function(){
	var errorCode = $("#errorCode");
	var errorMessage = $("#errorMessage");
	var messageType = "success";
	if(errorCode.length>0){
	
		if(errorCode.val()==1001){
			messageType = "danger";
		}
		
		$.notify({
			// options
			message: errorMessage.val(),
		},
		{
			placement : {
				align : "center",
			},
			type : messageType,
			delay : 4500,
		}); 
	}
});


/* var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
	allow_dismiss: false,
	showProgressbar: true
});

setTimeout(function() {
	notify.update({'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25});
}, 4500); */
</script>

</html>