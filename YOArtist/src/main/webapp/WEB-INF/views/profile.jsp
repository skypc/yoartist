<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<link rel="icon" href="img/favicon.png">
<title>Profile</title>
<!-- Bootstrap core CSS -->
<%-- <link rel="stylesheet"
	href="<c:url value='/resources/support/bootstrap.3.3.6/css/bootstrap.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/angular/css/angular-material.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/support/font-awesome.4.6.1/css/font-awesome.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/animate.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/timeline.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/profile2.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/cover.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/forms.css'/>">
<link href="<c:url value='/resources/support/assets/css/friends.css'/>"
	rel="stylesheet">
<link rel="stylesheet"
	href="<c:url value='/resources/support/angular-datepicker/css/angular-datepicker.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/buttons.css'/>">
<link href="<c:url value='/resources/skypc/css/header.css'/>"
	rel="stylesheet" />
<link href="<c:url value='/resources/skypc/css/common-user.css'/>"
	rel="stylesheet" />
<link href="http://vjs.zencdn.net/5.8.8/video-js.css" rel="stylesheet">
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="http://vjs.zencdn.net/5.8.8/video.js"></script>
 --%>
<link rel="stylesheet"
	href="<c:url value='/resources/support/bootstrap.3.3.6/css/bootstrap.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/font-awesome.4.6.1/css/font-awesome.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/animate.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/profile2.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/friends.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/timeline.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/login_register.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/forms.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/buttons.css'/>">

<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/cover.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/css/fileinput.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/support/css/bootstrap-edit.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/support/angular/css/angular-material.min.css'/>" />
<link href="<c:url value='/resources/skypc/css/header.css'/>"
	rel="stylesheet" />
<link href="<c:url value='/resources/support/video/video-js.css'/>"
	rel="stylesheet" />
<link href="<c:url value='/resources/skypc/css/common-user.css'/>"
	rel="stylesheet" />
<link href="<c:url value='/resources/skypc/css/common-user.css'/>"
	rel="stylesheet" />
<script
	src="<c:url value='/resources/support/video/videojs-ie8.min.js'/>"></script>
<script src="<c:url value='/resources/support/video/video.js'/>"></script>
<style type="text/css">
[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak,
	.x-ng-cloak {
	display: none !important;
}

input[type="radio"] {
	-webkit-appearance: radio !important;
}

.dialog-content{
	padding:4px;
}

.hideBody {
	opacity: 0;
}

.showBody {
	opacity: 1;
}

.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}
.my-md-dialog{
	width:55%;
}
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(images/loader-64x/Preloader_2.gif) center no-repeat #fff;
}

.cover.profile .wrapper .image {
	padding-right: 0px;
}

body.md-dialog-is-showing {
    overflow: visible !important;
}

.dialogdemoBasicUsage #popupContainer {
  position: relative; }

.dialogdemoBasicUsage .footer {
  width: 60%;
  text-align: center;
  margin-left: 20px; }

.dialogdemoBasicUsage .footer, .dialogdemoBasicUsage .footer > code {
  font-size: 0.8em;
  margin-top: 50px; }

.dialogdemoBasicUsage button {
  width: 200px; }

.dialogdemoBasicUsage div#status {
  color: #c60008; }

.dialogdemoBasicUsage .dialog-demo-prerendered md-checkbox {
  margin-bottom: 0; }
</style>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="animated fadeIn" ng-app="homeApp" ng-cloak>
	<jsp:include page="include/header.jsp" />
	<div style="height: 10px;"></div>
	<div ng-controller="profileLoadController as vm">
		<div class="{{vm.bodyOpacity?'hideBody':'showBody'}}">
			<!-- Fixed navbar -->

			<!-- Begin page content -->

			<!-- Begin page content -->
			<div class="container page-content">
				<div class="row" id="user-profile">
					<div class="col-md-4 col-xs-12">
						<div class="row-xs">
							<div class="main-box clearfix">
								<h2>John Breakgrow jr.</h2>
								<div class="profile-status">
									<i class="fa fa-check-circle"></i> Online
								</div>
								<img src="img/Friends/guy-3.jpg" alt=""
									class="profile-img img-responsive center-block show-in-modal">

								<div class="profile-details">
									<ul class="fa-ul">
										<li><i class="fa-li fa fa-user"></i>Following: <span>456</span></li>
										<li><i class="fa-li fa fa-users"></i>Followers: <span>828</span></li>
										<li><i class="fa-li fa fa-comments"></i>Posts: <span>1024</span></li>
									</ul>
								</div>

								<div class="profile-message-btn center-block text-center">
									<a href="#" class="btn btn-azure"> <i
										class="fa fa-envelope"></i> Send message
									</a>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-8 col-xs-12">
						<div class="row-xs">
							<div class="main-box clearfix">
								<div class="row profile-user-info"
									ng-controller="userProfileTopController as userTopProfile">
									<div class="col-sm-8">
										<div class="profile-user-details clearfix">
											<div class="profile-user-details-label">First Name</div>
											<div class="profile-user-details-value">
												{{usersProfileTopData.firstName}}</div>
										</div>
										<div class="profile-user-details clearfix">
											<div class="profile-user-details-label">Last Name</div>
											<div class="profile-user-details-value">{{usersProfileTopData.lastName}}</div>
										</div>
										<br
											ng-if="usersProfileTopData.lastName=='' || usersProfileTopData.lastName==null">
										<div class="profile-user-details clearfix">
											<div class="profile-user-details-label">Email</div>
											<div class="profile-user-details-value">
												{{usersProfileTopData.email}}</div>
										</div>
										<div class="profile-user-details clearfix">
											<div class="profile-user-details-label">Phone number</div>
											<div class="profile-user-details-value">011 223 344 556
												677</div>
										</div>
										<div class="profile-user-details clearfix">
											<div class="profile-user-details-label">Address</div>
											<div class="profile-user-details-value">
												{{usersProfileTopData.address}}</div>
										</div>
									</div>
									<div class="col-sm-4 profile-social">
										<ul class="fa-ul">
											<li><i class="fa-li fa fa-twitter-square"></i><a
												href="#">@johnbrewk</a></li>
											<li><i class="fa-li fa fa-linkedin-square"></i><a
												href="#">John Breakgrow jr.</a></li>
											<li><i class="fa-li fa fa-facebook-square"></i><a
												href="#">John Breakgrow jr.</a></li>
											<li><i class="fa-li fa fa-skype"></i><a href="#">john_smart</a></li>
											<li><i class="fa-li fa fa-instagram"></i><a href="#">john_smart</a></li>
										</ul>
									</div>
								</div>

								<div class="tabs-wrapper profile-tabs">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab-profile"
											data-toggle="tab"> <i class="fa fa-user"></i>Profile
										</a></li>
										<li><a href="#tab-timeline" data-toggle="tab"><i
												class="fa fa-life-ring"></i>Timeline</a></li>
										<li><a href="#tab-media" data-toggle="tab"><i
												class="fa fa-film"></i>Photos/Videos</a></li>
									</ul>

									<div class="tab-content">
										<div class="tab-pane fade in active " id="tab-profile" ng-controller="profileController as pfc">
											<ul class="widget-users row">
												<div class="row" style="margin-top: 20px;">
													<div class="col-md-12">
														<label>Work/Occupassion</label> <label ng-click="vm.saveUpdateModal($event)"
															style="float: right;"><md-button class="md-fab md-mini md-warn" aria-label="Eat cake">
            													<md-icon md-svg-src="/yoartist/resources/support/svg/plus.svg"></md-icon>
        													</md-button></label><br>
												<div class="row">
													<md-content ng-if="!vm.empDetails.length>0">
														<p class="text-center">you don't have any working history</p>
													</md-content>
													<md-content ng-if="vm.empDetails.length>0">
													    <md-list>
													      <md-list-item class="md-3-line" ng-repeat="item in vm.empDetails">
													        <div class="md-list-item-text">
													          <h3>{{item.jobPosition}} | {{item.orgName}} </h3>
													          <h4>{{item.employmentDuration}}</h4>
													          <p>{{item.country}}, {{item.state}}</p>
													        </div>
													        <md-button class="md-fab md-mini md-primary" ng-click="vm.editEmpData(item, $event)" aria-label="Eat cake">
            													<md-icon md-svg-src="/yoartist/resources/support/svg/pencil-black-tool-interface-symbol.svg"></md-icon>
        													</md-button>
        													<md-button ng-click="vm.deleteEmpData($event, item)" class="md-fab md-mini" aria-label="Eat cake">
            													<md-icon md-svg-src="/yoartist/resources/support/svg/rubbish-bin-delete-button.svg"></md-icon>
        													</md-button>
													        <md-divider ng-if="!$last"></md-divider>
													      </md-list-item>
													    </md-list>
													 </md-content>
														</div>
													</div>
													
												</div>

												<div class="row" style="margin-top: 20px;">
													<div class="col-md-12">
														<label>Education Details</label> <label
															style="float: right;"><i
															class="fa fa-plus fa-2x icon-color"
															 ng-click="pfc.openEduModal('add')"></i></label><br>
														<div class="row">
															<div class="col-md-10 col-xs-10">
																<div class="row">
																	<div class="col-md-3">
																		<img alt="" class="company_image"
																			ng-src="<c:url value="/resources/images/school_default.png"></c:url>">
																	</div>
																	<div class="col-md-9">
																		<p class="company_lables">Company Name</p>
																		<p class="company_lables">type</p>
																		<p class="company_lables">duration</p>
																	</div>

																</div>
															</div>
															<div class="col-md-2 col-xs-2">
																<i ng-click="pfc.openEduModal('add')" class="fa fa-pencil-square-o fa-lg icon-color"
																	aria-hidden="true"></i>
															</div>
														</div>
													</div>
												</div>
												<!--Educational Modal -->
												<div class="edu-modal-demo">
												 <script type="text/ng-template" id="eduModal.html">
																<div class="modal-header">
																	<button type="button" class="close"
																		data-dismiss="modal">&times;</button>
																	<h4 class="modal-title">Educational Details</h4>
																</div>
																<div class="modal-body">
																	<table style="width: 100%;">
																	<input type="hidden" ng-model="pfc.education.id">
																		<tr class="modal-profile-tr">
																			<td><lable>School/College</lable></td>
																			<td><input 
																				type="text"
																				class="modal-profile-input"
																				Placeholder="School/College" 
																				name="schoolName"
																				ng-model="pfc.education.schoolName" 
																				minlength="3"
																				required />
																				</td>
																				<td></td>
																				<td>
																					<div ng-messages="eduForm.schoolName.$error" 
																					ng-if="eduForm.schoolName.$dirty"
																					style="color: maroon" role="alert">
																						<div ng-message="required">School name is required</div>
																						<div ng-message="minlength">School name is too short</div>
																					</div>
																				</td>
																		</tr>
																		<tr class="modal-profile-tr">
																			<td><lable>Degree</lable></td>
																			<td><input type="text"
																				class="modal-profile-input" 
																				name="degree"
																				ng-model="pfc.education.degree"
																				Placeholder="Degree"
																				required />
																				</td>
																				<td></td>
																				<td>
																					<div ng-messages="eduForm.degree.$error" 
																					ng-if="eduForm.degree.$dirty"
																					style="color: maroon" role="alert">
																						<div ng-message="required">Degree is required</div>
																					</div>
																				</td>
																		</tr>
																		<tr class="modal-profile-tr">
																			<td><lable>Field Of Study</lable></td>
																			<td><input type="text"
																				class="modal-profile-input"
																				Placeholder="Field Of Study"
																				name="fieldOfStudy"
																				ng-model="pfc.education.fieldOfStudy">
																			</td>
																		</tr>
																		<tr class="modal-profile-tr">
																			<td><lable>Grade</lable></td>
																			<td><input type="text"
																			name="grade"
																			ng-model="pfc.education.grade"
																				class="modal-profile-input" Placeholder="Grade"></td>
																		</tr>
																		<tr class="modal-profile-tr">
																			<td><lable>From Year</lable></td>
																			<td><input
																				class="modal-profile-input modal-profile-input-width"
																				type="text" name="fromYear" ng-model="pfc.education.fromYear" Placeholder="yyyy" required></td>
																			<td><lable>To Year</lable></td>
																			<td><input
																				class="modal-profile-input modal-profile-input-width"
																				name="toYear" ng-model="pfc.education.toYear"
																				type="text" Placeholder="yyyy" required></td>
																		</tr>
																		<tr class="modal-profile-tr">
																			<td><lable>State</lable></td>
																			<td><input
																				class="modal-profile-input modal-profile-input-width"
																				type="text" name="state" ng-model="pfc.education.state" Placeholder="state" required></td>
																			<td><lable>Country</lable></td>
																			<td><input
																				class="modal-profile-input modal-profile-input-width"
																				name="toYear" ng-model="pfc.education.country"
																				type="text" Placeholder="Country" required></td>
																		</tr>
																		<tr class="modal-profile-tr">
																			<td><lable>Activities</lable></td>
																			<td><textarea  row="4" name="activities" ng-model="pfc.education.activities"
																					class="modal-profile-input"></textarea></td>
																		</tr>
																	</table>
																</div>
																<div class="modal-footer">
																	<button class="btn btn-primary" type="button" ng-click="$ctrl.ok(pfc.education)">Save</button>
            														<button class="btn btn-warning" type="button" ng-click="$ctrl.cancel()">Cancel</button>
																</div>
														</script></div>
													</div>
												</div>
									<div class="row" style="margin-top: 20px;">
										<div class="col-md-12">
											<label>Summary</label> <label ng-if="!vm.summaryDataDetails.summary"
												ng-click="vm.saveUpdateSummaryModal($event)" style="float: right;"><md-button
													class="md-fab md-mini md-warn" aria-label="Eat cake">
												<md-icon
													md-svg-src="/yoartist/resources/support/svg/plus.svg"></md-icon>
												</md-button></label><br>
										</div>
										<div class="col-md-12">
											<md-content ng-if="!vm.summaryDataDetails.summary">
											<p class="text-center">you don't have any summary</p>
											</md-content>
											<md-content ng-if="vm.summaryDataDetails.summary"> <md-list>
											<md-list-item class="md-3-line">
											<div class="md-list-item-text">
												<p>{{vm.summaryDataDetails.summary}}</p>
											</div>
											<md-button class="md-fab md-mini md-primary"
												ng-click="vm.editSummaryData(vm.summaryDataDetails, $event)"
												aria-label="Eat cake"> <md-icon
												md-svg-src="/yoartist/resources/support/svg/pencil-black-tool-interface-symbol.svg"></md-icon>
											</md-button> <md-button ng-click="vm.deleteSummaryData($event, vm.summaryDataDetails)"
												class="md-fab md-mini" aria-label="Eat cake"> <md-icon
												md-svg-src="/yoartist/resources/support/svg/rubbish-bin-delete-button.svg"></md-icon>
											</md-button> <md-divider ng-if="!$last"></md-divider> </md-list-item> </md-list> </md-content>
										</div>
									</div>


									<div class="row" style="margin-top: 20px;">
													<div class="col-md-12">
														<label>Award/Recognition</label> <label
															style="float: right;"><i
															class="fa fa-pencil-square-o fa-lg icon-color"></i></label><br>
														<div class="row">
															<div class="col-md-10">
																<div class="row">
																	<div class="col-md-2"></div>
																	<div class="col-md-10">This is my firt art</div>
																</div>
															</div>
														</div>
													</div>
												</div>


											</ul>
										</div>
										<div class="tab-pane fade" id="tab-timeline">
											<div class="row">
												<div class="col-md-12">
													<!--   posts -->
													<div class="box box-widget">
														<div class="box-header with-border">
															<div class="user-block">
																<img class="img-circle" src="img/Friends/guy-3.jpg"
																	alt="User Image"> <span class="username"><a
																	href="#">John Breakgrow jr.</a></span> <span
																	class="description">Shared publicly - 7:30 PM
																	Today</span>
															</div>
														</div>

														<div class="box-body" style="display: block;">
															<img class="img-responsive show-in-modal"
																src="img/Post/young-couple-in-love.jpg" alt="Photo">
															<p>I took this photo this morning. What do you guys
																think?</p>
															<button type="button" class="btn btn-default btn-xs">
																<i class="fa fa-share"></i> Share
															</button>
															<button type="button" class="btn btn-default btn-xs">
																<i class="fa fa-thumbs-o-up"></i> Like
															</button>
															<span class="pull-right text-muted">127 likes - 3
																comments</span>
														</div>
														<div class="box-footer box-comments"
															style="display: block;">
															<div class="box-comment">
																<img class="img-circle img-sm"
																	src="img/Friends/guy-2.jpg" alt="User Image">
																<div class="comment-text">
																	<span class="username"> Maria Gonzales <span
																		class="text-muted pull-right">8:03 PM Today</span>
																	</span> It is a long established fact that a reader will be
																	distracted by the readable content of a page when
																	looking at its layout.
																</div>
															</div>

															<div class="box-comment">
																<img class="img-circle img-sm"
																	src="img/Friends/guy-3.jpg" alt="User Image">
																<div class="comment-text">
																	<span class="username"> Luna Stark <span
																		class="text-muted pull-right">8:03 PM Today</span>
																	</span> It is a long established fact that a reader will be
																	distracted by the readable content of a page when
																	looking at its layout.
																</div>
															</div>
														</div>
														<div class="box-footer" style="display: block;">
															<form action="#" method="post">
																<img class="img-responsive img-circle img-sm"
																	src="img/Friends/guy-3.jpg" alt="Alt Text">
																<div class="img-push">
																	<input type="text" class="form-control input-sm"
																		placeholder="Press enter to post comment">
																</div>
															</form>
														</div>
													</div>
													<!--  end posts-->



													<!-- post -->
													<div class="box box-widget">
														<div class="box-header with-border">
															<div class="user-block">
																<img class="img-circle" src="img/Friends/guy-3.jpg"
																	alt="User Image"> <span class="username"><a
																	href="#">Jonathan Burke Jr.</a></span> <span
																	class="description">Shared publicly - 7:30 PM
																	Today</span>
															</div>
														</div>
														<div class="box-body">
															<p>Far far away, behind the word mountains, far from
																the countries Vokalia and Consonantia, there live the
																blind texts. Separated they live in Bookmarksgrove right
																at</p>

															<p>the coast of the Semantics, a large language
																ocean. A small river named Duden flows by their place
																and supplies it with the necessary regelialia. It is a
																paradisematic country, in which roasted parts of
																sentences fly into your mouth.</p>

															<div class="attachment-block clearfix">
																<img class="attachment-img show-in-modal"
																	src="img/Photos/2.jpg" alt="Attachment Image">
																<div class="attachment-pushed">
																	<h4 class="attachment-heading">
																		<a href="http://www.bootdey.com/">Lorem ipsum text
																			generator</a>
																	</h4>
																	<div class="attachment-text">
																		Description about the attachment can be placed here.
																		Lorem Ipsum is simply dummy text of the printing and
																		typesetting industry... <a href="#">more</a>
																	</div>
																</div>
															</div>
															<button type="button" class="btn btn-default btn-xs">
																<i class="fa fa-share"></i> Share
															</button>
															<button type="button" class="btn btn-default btn-xs">
																<i class="fa fa-thumbs-o-up"></i> Like
															</button>
															<span class="pull-right text-muted">45 likes - 2
																comments</span>
														</div>
														<div class="box-footer box-comments">
															<div class="box-comment">
																<img class="img-circle img-sm"
																	src="img/Friends/guy-5.jpg" alt="User Image">
																<div class="comment-text">
																	<span class="username"> Maria Gonzales <span
																		class="text-muted pull-right">8:03 PM Today</span>
																	</span> It is a long established fact that a reader will be
																	distracted by the readable content of a page when
																	looking at its layout.
																</div>
															</div>
															<div class="box-comment">
																<img class="img-circle img-sm"
																	src="img/Friends/guy-6.jpg" alt="User Image">
																<div class="comment-text">
																	<span class="username"> Nora Havisham <span
																		class="text-muted pull-right">8:03 PM Today</span>
																	</span> The point of using Lorem Ipsum is that it has a
																	more-or-less normal distribution of letters, as opposed
																	to using 'Content here, content here', making it look
																	like readable English.
																</div>
															</div>
														</div>
														<div class="box-footer">
															<form action="#" method="post">
																<img class="img-responsive img-circle img-sm"
																	src="img/Friends/guy-3.jpg" alt="Alt Text">
																<div class="img-push">
																	<input type="text" class="form-control input-sm"
																		placeholder="Press enter to post comment">
																</div>
															</form>
														</div>
													</div>
													<!-- end post -->


													<!--   posts -->
													<div class="box box-widget">
														<div class="box-header with-border">
															<div class="user-block">
																<img class="img-circle" src="img/Friends/guy-3.jpg"
																	alt="User Image"> <span class="username"><a
																	href="#">John Breakgrow jr.</a></span> <span
																	class="description">Shared publicly - 7:30 PM
																	Today</span>
															</div>
														</div>

														<div class="box-body" style="display: block;">
															<img class="img-responsive pad show-in-modal"
																src="img/Photos/3.jpg" alt="Photo">
															<p>I took this photo this morning. What do you guys
																think?</p>
															<button type="button" class="btn btn-default btn-xs">
																<i class="fa fa-share"></i> Share
															</button>
															<button type="button" class="btn btn-default btn-xs">
																<i class="fa fa-thumbs-o-up"></i> Like
															</button>
															<span class="pull-right text-muted">127 likes - 3
																comments</span>
														</div>
														<div class="box-footer box-comments"
															style="display: block;">
															<div class="box-comment">
																<img class="img-circle img-sm"
																	src="img/Friends/guy-2.jpg" alt="User Image">
																<div class="comment-text">
																	<span class="username"> Maria Gonzales <span
																		class="text-muted pull-right">8:03 PM Today</span>
																	</span> It is a long established fact that a reader will be
																	distracted by the readable content of a page when
																	looking at its layout.
																</div>
															</div>

															<div class="box-comment">
																<img class="img-circle img-sm"
																	src="img/Friends/guy-3.jpg" alt="User Image">
																<div class="comment-text">
																	<span class="username"> Luna Stark <span
																		class="text-muted pull-right">8:03 PM Today</span>
																	</span> It is a long established fact that a reader will be
																	distracted by the readable content of a page when
																	looking at its layout.
																</div>
															</div>
														</div>
														<div class="box-footer" style="display: block;">
															<form action="#" method="post">
																<img class="img-responsive img-circle img-sm"
																	src="img/Friends/guy-3.jpg" alt="Alt Text">
																<div class="img-push">
																	<input type="text" class="form-control input-sm"
																		placeholder="Press enter to post comment">
																</div>
															</form>
														</div>
													</div>
													<!--  end posts -->
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="tab-connection">
											<div class="row">
												<div class="col-md-3">
													<div class="contact-box center-version">
														<a href="#"> <img alt="image" class="img-circle"
															src="<c:url value='/resources/img/Friends/guy-1.jpg'/>">
															<h3>
																<strong>John Doe</strong>
															</h3>

															<div class="font-bold">Graphics designer</div>
														</a>
														<div class="contact-box-footer">
															<div class="m-t-xs btn-group">
																<a href="messages1.html" class="btn btn-xs btn-white"><i
																	class="fa fa-envelope"></i>Send Messages</a> <a
																	class="btn btn-xs btn-white"><i
																	class="fa fa-user-plus"></i> Follow</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="tab-pane fade" id="tab-media">
											<ul class="widget-users row">
												<li class="col-md-6">
													<div class="img">
														<img src="img/Friends/woman-6.jpg" alt="">
													</div>
													<div class="details">
														<div class="name">
															<a href="#">Danielath grande</a>
														</div>
														<div class="time">
															<i class="fa fa-clock-o"></i> Last online: 5 minutes ago
														</div>
													</div>
												</li>
												<li class="col-md-6">
													<div class="img">
														<img src="img/Friends/woman-4.jpg" alt="">
													</div>
													<div class="details">
														<div class="name">
															<a href="#">Fernanda Hytrod</a>
														</div>
														<div class="time online">
															<i class="fa fa-check-circle"></i> Online
														</div>
													</div>
												</li>
												<li class="col-md-6">
													<div class="img">
														<img src="img/Friends/guy-1.jpg" alt="">
													</div>
													<div class="details">
														<div class="name">
															<a href="#">Arnold Thossling</a>
														</div>
														<div class="time online">
															<i class="fa fa-check-circle"></i> Online
														</div>
													</div>
												</li>
												<li class="col-md-6">
													<div class="img">
														<img src="img/Friends/guy-2.jpg" alt="">
													</div>
													<div class="details">
														<div class="name">
															<a href="#">Peter Downey</a>
														</div>
														<div class="time">
															<i class="fa fa-clock-o"></i> Last online: Thursday
														</div>
													</div>
												</li>
												<li class="col-md-6">
													<div class="img">
														<img src="img/Friends/woman-3.jpg" alt="">
													</div>
													<div class="details">
														<div class="name">
															<a href="#">Emiliath Suansont</a>
														</div>
														<div class="time">
															<i class="fa fa-clock-o"></i> Last online: 1 week ago
														</div>
													</div>
												</li>
												<li class="col-md-6">
													<div class="img">
														<img src="img/Friends/guy-6.jpg" alt="">
													</div>
													<div class="details">
														<div class="name">
															<a href="#">Briatng bowingy</a>
														</div>
														<div class="time">
															<i class="fa fa-clock-o"></i> Last online: 1 month ago
														</div>
													</div>
												</li>
												<li class="col-md-6">
													<div class="img">
														<img src="img/Friends/woman-4.jpg" alt="">
													</div>
													<div class="details">
														<div class="name">
															<a href="#">Milanith Grotyu</a>
														</div>
														<div class="time online">
															<i class="fa fa-check-circle"></i> Online
														</div>
													</div>
												</li>
												<li class="col-md-6">
													<div class="img">
														<img src="img/Friends/guy-5.jpg" alt="">
													</div>
													<div class="details">
														<div class="name">
															<a href="#">Trwort Htrew</a>
														</div>
														<div class="time online">
															<i class="fa fa-check-circle"></i> Online
														</div>
													</div>
												</li>
											</ul>
											<br> <a href="#" class="btn btn-azure pull-right"> <i
												class="fa fa-refresh"></i> Load more
											</a>
										</div>

									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="modalShow" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Modal title</h4>
						</div>
						<div class="modal-body text-centers">...</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
			<div class="container">
				<p class="text-muted">Copyright &copy; Company - All rights
					reserved</p>
			</div>
			</footer>
		</div>
	</div>
	
	<jsp:include page="dialogs/employment-dialog.jsp"></jsp:include>
	<jsp:include page="dialogs/summary-dialog.jsp"></jsp:include>
	
	
	
	<script
		src="<c:url value='/resources/support/assets/js/jquery-2.1.3.min.js'/>"></script>
	<script
		src="<c:url value='/resources/support/bootstrap.3.3.6/js/bootstrap.min.js'/>"></script>
	<script
		src="<c:url value='/resources/support/js/bootstrap-notify.js'/>"></script>
	<script
		src="<c:url value='/resources/support/js/jquery.validate.min.js'/>"></script>
	<script src="<c:url value='/resources/support/assets/js/custom.js'/>"></script>

	<script
		src="<c:url value='/resources/support/angular/js/angular.min.js'/>"></script>
	<script
		src="<c:url value='/resources/support/angular-datepicker/js/angular-datepicker.js'/>"></script>
	<script
		src="<c:url value='/resources/support/angular/js/angular-animate.min.js'/>"></script>
	<script
		src="<c:url value='/resources/support/angular/js/angular-sanitize.min.js'/>"></script>
	<script
		src="<c:url value='/resources/support/angular/js/angular-aria.min.js'/>"></script>
	<script
		src="<c:url value='/resources/support/angular/js/angular-modal-service.min.js'/>"></script>
	<script
		src="<c:url value='/resources/support/angular/js/angular-messages.min.js'/>"></script>
	<script
		src="<c:url value='/resources/support/angular/js/angular-material.min.js'/>"></script>
		
	<script
		src="<c:url value='/resources/support/angular/js/angular-pubsub.js'/>"></script>
	
	<script
		src="<c:url value='/resources/support/angular/js/ui-bootstrap-tpls-2.3.0.js'/>"></script>
	<script src="<c:url value='/resources/support/js/bootstrap-edit.js'/>"></script>
	<script src="<c:url value='/resources/support/angular/js/moment.js'/>"></script>
	
	<script src="<c:url value='/resources/support/js/textAngular-1.5.16/dist/textAngular.min.js'/>"></script>

	<script src="<c:url value='/resources/skypc/angular/app.js'/>"></script>
	<script src="<c:url value='/resources/skypc/angular/directives/model-dismiss-directive.js'/>"></script>
	<script
		src="<c:url value='/resources/skypc/angular/filters/yo-filters.js'/>"></script>
	<script
		src="<c:url value='/resources/skypc/angular/factories/generic-factory.js'/>"></script>
	<script
		src="<c:url value='/resources/skypc/angular/factories/user-get-factory.js'/>"></script>
	<script
		src="<c:url value='/resources/skypc/angular/factories/notification-factory.js'/>"></script>
	<script
		src="<c:url value='/resources/skypc/angular/factories/connection-factory.js'/>"></script>
	<script
		src="<c:url value='/resources/skypc/angular/factories/profile-factory.js'/>"></script>
	<script
		src="<c:url value='/resources/skypc/angular/controller/connection-controller.js'/>"></script>
	<script
		src="<c:url value='/resources/skypc/angular/controller/userprofile-top-controller.js'/>"></script>
		<script
		src="<c:url value='/resources/skypc/angular/controller/profile-controller.js'/>"></script>
	<script
		src="<c:url value='/resources/skypc/angular/controller/notification-controller.js'/>"></script>
	<script
		src="<c:url value='/resources/skypc/angular/controller/auto-complete-controller.js'/>"></script>
	<script
		src="<c:url value='/resources/skypc/angular/controller/connect-controller.js'/>"></script>
	<script
		src="<c:url value='/resources/skypc/angular/controller/profile-load-controller.js'/>"></script>

	<script type="text/javascript">
		
	</script>
	<script type="text/javascript">
		$(document).on("click", "ul.dropdown-menu", function(event) {
			event.stopPropagation();
		});

		$(document).ready(function() {
			$('.user-profile-dropdown').css('background-color', '#067894');
		});
	</script>
</body>
</html>
