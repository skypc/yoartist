/**
 * 
 */
<script type="text/ng-template" id="summary.dialog.html">
<md-dialog aria-label="{{summaryDetails.title}}" class="my-md-dialog">
	<md-toolbar>
		<div class="md-toolbar-tools">
			<h2>{{summaryDetails.title}}</h2>
			<span flex></span>
			<md-button class="md-icon-button" ng-click="cancel()"> x </md-button>
		</div>
	</md-toolbar> 
	<md-dialog-content>
<div class="dialog-content">
	<form name="summaryForm" ng-submit="saveOrUpdateSummary()" novalidate>
		<md-input-container class="md-block"> <label>Summary</label>
		<textarea ng-model="profile.summary" md-maxlength="500" rows="5"
			md-select-on-focus required name="summary"></textarea>
			<div ng-messages="summaryForm.summary.$error" role="alert" multiple>
              	<div ng-message="required" class="my-message">Please enter the summary.</div>
            </div>
		</md-input-container>
		<div layout-gt-sm="row" layout-align="center center">
			<md-button type="submit" class="md-raised md-primary"
				ng-disabled="summaryForm.$invalid">{{summaryDetails.buttonName}}</md-button>

		</div>
	</form>
</div>
</md-content>
</md-dialog>
</script>
