<script type="text/ng-template" id="dialog1.tmpl.html">
<md-dialog aria-label="{{dialogDetails.title}}" class="my-md-dialog">
	<md-toolbar>
		<div class="md-toolbar-tools">
			<h2>{{dialogDetails.title}}</h2>
			<span flex></span>
			<md-button class="md-icon-button" ng-click="cancel()"> x </md-button>
		</div>
	</md-toolbar> 
	<md-dialog-content>
		<div class="dialog-content">
			<form name="employmentForm" ng-submit="saveOrUpdate()" novalidate>
				<div layout-gt-sm="row">
					<md-input-container class="md-block" flex-gt-sm="45">
					<label>Job Position</label> <input name="jobPosition" ng-model="employment.jobPosition"
						required> 
					<div ng-messages="employmentForm.jobPosition.$error" role="alert" multiple>
              			<div ng-message="required" class="my-message">Please enter job position.</div>
              			
              		</div>
					</md-input-container>
					<md-input-container class="md-block" flex-gt-sm="10"></md-input-container>
					<md-input-container class="md-block" flex-gt-sm="45">
					<label>Organization</label> <input ng-model="employment.orgName" name="orgName"
						required>
					<div ng-messages="employmentForm.orgName.$error" role="alert" multiple>
              			<div ng-message="required" class="my-message">Please enter Organization name.</div>
              			
              		</div>
					 </md-input-container>
				</div>
		
				<div layout-gt-sm="row">
					<md-input-container class="md-block" flex-gt-sm="45">
					<label>Country</label> <md-select ng-model="employment.country"> <md-option
						ng-repeat="state in states" value="{{state.abbrev}}">
					{{state.abbrev}} </md-option> </md-select> </md-input-container>
					<md-input-container class="md-block" flex-gt-sm="10"></md-input-container>
					<md-input-container class="md-block" flex-gt-sm="45">
					<label>State</label> <md-select ng-model="employment.state"> <md-option
						ng-repeat="state in states" value="{{state.abbrev}}">
					{{state.abbrev}} </md-option> </md-select> </md-input-container>
				</div>
		
				<div layout-gt-sm="row">
					<md-input-container class="md-block" flex-gt-sm="45">
					<label>Start date</label> <md-datepicker
						ng-model="startDate"></md-datepicker> </md-input-container>
					<md-input-container class="md-block" flex-gt-sm="10"></md-input-container>
					<md-input-container class="md-block" flex-gt-sm="45">
						<md-switch class="md-primary" name="special" ng-model="employment.isCurrentlyWorking">
          				Enable special options.
        			</md-switch>
					 </md-input-container>
				</div>
		
				<div layout-gt-sm="row" ng-if="!employment.isCurrentlyWorking">
					<md-input-container class="md-block" flex-gt-sm="45">
					<label>End Date</label> <md-datepicker ng-model="endDate"></md-datepicker>
					</md-input-container>
				</div>
		
				<div layout-gt-sm="row" layout-align="center center">
					<md-button type="submit" 
						class="md-raised md-primary" ng-disabled="employmentForm.$invalid">{{dialogDetails.buttonName}}</md-button>

				</div>
			</form>
		</div>
	</md-content>
</md-dialog>
</script>
