<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="img/favicon.png">


<link rel="stylesheet"
	href="<c:url value='/resources/support/bootstrap.3.3.6/css/bootstrap.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/font-awesome.4.6.1/css/font-awesome.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/animate.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/timeline.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/cover.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/forms.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/buttons.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/friends.css'/>">
<link href="<c:url value='/resources/skypc/css/common-user.css'/>"
	rel="stylesheet" />
<link rel="stylesheet"
	href="<c:url value='/resources/support/angular/css/angular-material.min.css'/>" />
<link href="<c:url value='/resources/skypc/css/header.css'/>"
	rel="stylesheet" />

<link href="http://vjs.zencdn.net/5.8.8/video-js.css" rel="stylesheet">



<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="http://vjs.zencdn.net/5.8.8/video.js"></script>



<title>Home-YOArtist</title>
<style>
.row {
    margin-right: -60px;
    margin-left: -60px;
    margin-top: 40px;
 }
 
 
 
  .m-b-xs{
  margin-top: 0px;
  margin-bottom: 5px;
  }
 	
 
</style>
</head>

<body class="animated fadeIn" ng-app="homeApp">
	<input type="hidden" value="${pageContext.request.contextPath}"
		id="rootPath">

	<!-- Fixed navbar header-->
	<jsp:include page="include/header.jsp" />
	<toast></toast>

	<!-- Begin page content -->
	<div class="row page-content">
      <div class="col-md-12 col-md-offset-2">
        <div class="row">
          <div class="col-md-2">
              <div class="contact-box center-version">
	              	              
              
                <a href="#">
                  <img alt="image" class="img-circle" src="img/Friends/guy-1.jpg">
                  <h3 class="m-b-xs"><strong>John Doe</strong></h3>
    
                  <div class="font-bold">Graphics designer</div>
                </a>
                <div class="contact-box-footer">
                  <div class="m-t-xs btn-group">
                    <a href="messages1.html" class="btn btn-xs btn-white"><i class="fa fa-envelope"></i>Send Messages</a>
                    <a class="btn btn-xs btn-white"><i class="fa fa-user-plus"></i> Follow</a>
                  </div>
                </div>
              </div>
          </div>

          <div class="col-md-2">
              <div class="contact-box center-version">
                <a href="#">
                  <img alt="image" class="img-circle" src="img/Friends/guy-2.jpg">
                  <h3 class="m-b-xs"><strong>John Doe</strong></h3>
    
                  <div class="font-bold">Graphics designer</div>
                </a>
                <div class="contact-box-footer">
                  <div class="m-t-xs btn-group">
                    <a href="messages1.html" class="btn btn-xs btn-white"><i class="fa fa-envelope"></i>Send Messages</a>
                    <a class="btn btn-xs btn-white"><i class="fa fa-user-plus"></i> Follow</a>
                  </div>
                </div>
              </div>
          </div>
		  <div class="col-md-2">
              <div class="contact-box center-version">
                <a href="#">
                  <img alt="image" class="img-circle" src="img/Friends/guy-2.jpg">
                  <h3 class="m-b-xs"><strong>John Doe</strong></h3>
    
                  <div class="font-bold">Graphics designer</div>
                </a>
                <div class="contact-box-footer">
                  <div class="m-t-xs btn-group">
                    <a href="messages1.html" class="btn btn-xs btn-white"><i class="fa fa-envelope"></i>Send Messages</a>
                    <a class="btn btn-xs btn-white"><i class="fa fa-user-plus"></i> Follow</a>
                  </div>
                </div>
              </div>
          </div>
          <div class="col-md-2">
              <div class="contact-box center-version">
                <a href="#">
                  <img alt="image" class="img-circle" src="img/Friends/guy-3.jpg">
                  <h3 class="m-b-xs"><strong>John Doe</strong></h3>
    
                  <div class="font-bold">Graphics designer</div>
                </a>
                <div class="contact-box-footer">
                  <div class="m-t-xs btn-group">
                    <a href="messages1.html" class="btn btn-xs btn-white"><i class="fa fa-envelope"></i>Send Messages</a>
                    <a class="btn btn-xs btn-white"><i class="fa fa-user-plus"></i> Follow</a>
                  </div>
                </div>
              </div>
          </div>
		</div>
		<div class="row">
          <div class="col-md-2">
              <div class="contact-box center-version">
                <a href="#">
                  <img alt="image" class="img-circle" src="img/Friends/woman-1.jpg">
                  <h3 class="m-b-xs"><strong>John Doe</strong></h3>
    
                  <div class="font-bold">Graphics designer</div>
                </a>
                <div class="contact-box-footer">
                  <div class="m-t-xs btn-group">
                    <a href="messages1.html" class="btn btn-xs btn-white"><i class="fa fa-envelope"></i>Send Messages</a>
                    <a class="btn btn-xs btn-white"><i class="fa fa-user-plus"></i> Follow</a>
                  </div>
                </div>
              </div>
          </div>
		  <div class="col-md-2">
              <div class="contact-box center-version">
                <a href="#">
                  <img alt="image" class="img-circle" src="img/Friends/woman-1.jpg">
                  <h3 class="m-b-xs"><strong>John Doe</strong></h3>
    
                  <div class="font-bold">Graphics designer</div>
                </a>
                <div class="contact-box-footer">
                  <div class="m-t-xs btn-group">
                    <a href="messages1.html" class="btn btn-xs btn-white"><i class="fa fa-envelope"></i>Send Messages</a>
                    <a class="btn btn-xs btn-white"><i class="fa fa-user-plus"></i> Follow</a>
                  </div>
                </div>
              </div>
          </div>
          <div class="col-md-2">
              <div class="contact-box center-version">
                <a href="#">
                  <img alt="image" class="img-circle" src="img/Friends/woman-2.jpg">
                  <h3 class="m-b-xs"><strong>John Doe</strong></h3>
    
                  <div class="font-bold">Graphics designer</div>
                </a>
                <div class="contact-box-footer">
                  <div class="m-t-xs btn-group">
                    <a href="messages1.html" class="btn btn-xs btn-white"><i class="fa fa-envelope"></i>Send Messages</a>
                    <a class="btn btn-xs btn-white"><i class="fa fa-user-plus"></i> Follow</a>
                  </div>
                </div>
              </div>
          </div>

          <div class="col-md-2">
              <div class="contact-box center-version">
                <a href="#">
                  <img alt="image" class="img-circle" src="img/Friends/woman-3.jpg">
                  <h3 class="m-b-xs"><strong>John Doe</strong></h3>
    
                  <div class="font-bold">Graphics designer</div>
                </a>
                <div class="contact-box-footer">
                  <div class="m-t-xs btn-group">
                    <a href="messages1.html" class="btn btn-xs btn-white"><i class="fa fa-envelope"></i>Send Messages</a>
                    <a class="btn btn-xs btn-white"><i class="fa fa-user-plus"></i> Follow</a>
                  </div>
                </div>
              </div>
          </div>
		</div>
		<div class="row">
          <div class="col-md-2">
              <div class="contact-box center-version">
                <a href="#">
                  <img alt="image" class="img-circle" src="img/Friends/guy-5.jpg">
                  <h3 class="m-b-xs"><strong>John Doe</strong></h3>
    
                  <div class="font-bold">Graphics designer</div>
                </a>
                <div class="contact-box-footer">
                  <div class="m-t-xs btn-group">
                    <a href="messages1.html" class="btn btn-xs btn-white"><i class="fa fa-envelope"></i>Send Messages</a>
                    <a class="btn btn-xs btn-white"><i class="fa fa-user-plus"></i> Follow</a>
                  </div>
                </div>
              </div>
          </div>

          <div class="col-md-2">
              <div class="contact-box center-version">
                <a href="#">
                  <img alt="image" class="img-circle" src="img/Friends/woman-1.jpg">
                  <h3 class="m-b-xs"><strong>John Doe</strong></h3>
    
                  <div class="font-bold">Graphics designer</div>
                </a>
                <div class="contact-box-footer">
                  <div class="m-t-xs btn-group">
                    <a href="messages1.html" class="btn btn-xs btn-white"><i class="fa fa-envelope"></i>Send Messages</a>
                    <a class="btn btn-xs btn-white"><i class="fa fa-user-plus"></i> Follow</a>
                  </div>
                </div>
              </div>
          </div>
          <div class="col-md-2">
              <div class="contact-box center-version">
                <a href="#">
                  <img alt="image" class="img-circle" src="img/Friends/woman-1.jpg">
                  <h3 class="m-b-xs"><strong>John Doe</strong></h3>
    
                  <div class="font-bold">Graphics designer</div>
                </a>
                <div class="contact-box-footer">
                  <div class="m-t-xs btn-group">
                    <a href="messages1.html" class="btn btn-xs btn-white"><i class="fa fa-envelope"></i>Send Messages</a>
                    <a class="btn btn-xs btn-white"><i class="fa fa-user-plus"></i> Follow</a>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
	<!-- end page content -->



	<footer class="footer">
	<div class="container">
		<p class="text-muted">Copyright &copy; Company - All rights
			reserved</p>
	</div>
	</footer>
</body>
<script
	src="<c:url value='/resources/support/assets/js/jquery-2.1.3.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/bootstrap.3.3.6/js/bootstrap.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/js/jquery.validate.min.js'/>"></script>
<script src="<c:url value='/resources/support/assets/js/custom.js'/>"></script>

<script
	src="<c:url value='/resources/support/angular/js/angular.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-animate.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-sanitize.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-modal-service.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-aria.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-messages.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-material.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/ui-bootstrap-tpls-2.3.0.js'/>"></script>

<script src="<c:url value='/resources/skypc/angular/app.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/filters/yo-filters.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/factories/user-get-factory.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/factories/notification-factory.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/factories/connection-factory.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/factories/message-factory.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/connection-controller.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/notification-controller.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/auto-complete-controller.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/connect-controller.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/message-controller.js'/>"></script>
<script type="text/javascript">
$(document).ready(function() {
		$('.fa-users').css('color','#067894');
	});
</script>
</html>