<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<a>
<div class="notify-dropdown"
	ng-controller="notification-controller as notify" ng-cloak>
	<a id="dLabel" role="button" data-toggle="dropdown" data-target="#"
		href="/page.html"> <span class="glyphicon glyphicon-bell"
		ng-click="notify.getLimitedNotification();"
		style="font-size: 20px; line-height: 0px;color: #2dc3e8;font-size: 2em !important;"></span>
		<span class="badge badge-notify">{{notify.notificationCount}}</span>
	</a>
	<ul class="dropdown-menu notifications notifcus" role="menu"
		style="margin-top: 20px;" aria-labelledby="dLabel">
		<div class="notification-heading" ng-cloak="">
			<h4 class="menu-title">Notifications</h4>
			<h4 ng-if="!notify.isEmpty" class="menu-title pull-right">
				View all<i class="glyphicon glyphicon-circle-arrow-right"></i>
			</h4>
		</div>
		
		<div flex-gt-sm="100" flex>

			<md-content> <md-list flex>
			<div
				class="{{notify.notifications.length>0?'scroll-notification':''}}">
				<md-divider></md-divider>
				<md-list-item class="md-2-line hoverDiv"
					ng-repeat="notification in notify.notifications">
				<a ng-href="${pageContext.request.contextPath}{{notification.link}}">
					<img class="img-rounded" ng-if="notification.userImage"
					data-ng-src="data:image/jpeg;base64,{{notification.userImage}}"
					alt=""> <img class="md-avatar"
					ng-if="!notification.userImage"
					data-ng-src="${pageContext.request.contextPath}/resources/images/profile-pic.png"
					alt="">
				</a>

				<div class="md-list-item-text" layout="column">
					<h4>
						<a
							ng-href="${pageContext.request.contextPath}{{notification.link}}"><Strong style="color:#2dc3e8">{{notification.fullName}}</Strong></a>
						<span ng-bind-html="notification.description | contextPathFilter"></span>
					</h4>
					<p><i class="fa fa-clock-o" aria-hidden="true"></i> {{notification.time}}</p>
				</div>
				<md-divider></md-divider>
				</md-list-item>
			</div>
			<md-divider></md-divider> </md-list></md-content>
		</div>
		<p ng-if="notify.isEmpty" class="text-center">No records found</p>

		<div class="notification-footer">
			<h4 ng-if="!notify.isEmpty" class="menu-title">
				View all<i class="glyphicon glyphicon-circle-arrow-right"></i>
			</h4>
		</div>
	</ul>

</div></a>
