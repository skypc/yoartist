<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
    <a>
    <div class="notify-dropdown" ng-controller="connection-controller as conCtrl" ng-cloak>
        <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html"> 
        <i title="Connection request" ng-click="conCtrl.getAllConnectionRequest()" class="fa fa-user-plus" aria-hidden="true" style="font-size: 2em !important;line-height: 0px; font-size: 20px; color: #2dc3e8;"></i> 
        <span class="badge badge-notify" style="left:-17px;">{{conCtrl.connectionCount}}</span>
        </a>
	<ul class="dropdown-menu notifications" role="menu"
		id="connectionDropdown" style="margin-top: 23px;"
		aria-labelledby="dLabel">
		<div class="notification-heading" ng-cloak="">
			<h4 class="menu-title">Connection Request</h4>
			<!-- <h4 ng-if="!conCtrl.isEmpty" class="menu-title pull-right">
				View all<i class="glyphicon glyphicon-circle-arrow-right"></i>
			</h4> -->
		</div>


		<div flex-gt-sm="100" flex>

			<md-content> <md-list flex>
			<div
				class="{{notify.notifications.length>0?'scroll-notification':''}}">
				<md-divider></md-divider>
				<md-list-item class="md-2-line hoverDiv"
					ng-repeat="con in conCtrl.connections" ng-hide="conCtrl.currentSelectedUserId==con.userId"> <a
					ng-href="${pageContext.request.contextPath}{{notification.link}}">
					<img class="img-rounded" ng-if="con.userImage"
					data-ng-src="data:image/jpeg;base64,{{con.userImage}}" alt="">
					<img class="md-avatar" ng-if="!con.userImage"
					data-ng-src="${pageContext.request.contextPath}/resources/images/profile-pic.png"
					alt="">
				</a>

				<div class="md-list-item-text" layout="column">
					<h4>
						<a ng-href="${pageContext.request.contextPath}{{con.link}}"><Strong>{{con.fullName}}</Strong></a>
						{{con.description}}
					</h4>
					<p>
						<i class="fa fa-clock-o" aria-hidden="true"></i>{{con.time}}
						<md-button class="md-raised md-primary"
							ng-click="conCtrl.acceptIgnoreConnectRequest(con.userId,true);">Connect</md-button>
						<md-button class="md-raised md-warn"
							ng-click="conCtrl.acceptIgnoreConnectRequest(con.userId,false);">Ignore</md-button>
					</p>
				</div>
				<md-divider></md-divider> </md-list-item>
			</div>
			<md-divider></md-divider> </md-list></md-content>
		</div>
		<p class="text-center" ng-if="conCtrl.isEmpty">No Connection
			request</p>

		<!-- <li class="divider"></li>
		<div class="notification-footer">
			<h4 ng-if="!conCtrl.isEmpty" class="menu-title">
				View all<i class="glyphicon glyphicon-circle-arrow-right"></i>
			</h4>
		</div> -->
	</ul>

</div></a>