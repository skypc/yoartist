<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<div class="notifications-wrapper"
			ng-repeat="notification in notify.notifications">
			<a class="content"
				href="${pageContext.request.contextPath}/user/notifications">
				<div class="notification-item">
					<div class="notif-image">
						<a href="${pageContext.request.contextPath}{{notification.link}}">
							<img class="img-rounded" ng-if="notification.userImage"
							data-ng-src="data:image/jpeg;base64,{{notification.userImage}}" alt=""
							>
							<img
							class="img-rounded" ng-if="!notification.userImage"
							data-ng-src="${pageContext.request.contextPath}/resources/images/profile-pic.png"
							alt="">
						</a>
						
					</div>
					<div class='notif-content'>
						<h4 class="item-title">
							<a
								style="color: #0c424a; font-size: 14px; margin-right: 2px; font-family: Verdana, Geneva, sans-serif;"
								href="${pageContext.request.contextPath}{{notification.link}}">{{notification.fullName}}</a>
							{{notification.description}}
						</h4>
						<p class="item-info">
							<i class="fa fa-clock-o" aria-hidden="true"></i>
							{{notification.time}}
							<!-- <button class="connectBtn" ng-click="notify.acceptIgnoreConnectRequest(notification.userId,true);">Connect</button>
							<button class="ignoreBtn" ng-click="notify.acceptIgnoreConnectRequest(notification.userId,false);">Ignore</button> -->
						</p>
					</div>
				</div>
			</a>

		</div>

</body>
</html>