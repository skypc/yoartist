<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="col-md-3">
				<div class="profile-nav">
					<div class="widget">
						<div class="widget-body">
							<%-- <div class="user-heading round">
								<a href="#"> <img
									src='<c:url value="/resources/images/profile-pic.png"></c:url>'
									" alt="">
								</a>
								<h1>${sessionScope.SessionFullName}</h1>

							</div>
 --%>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="${pageContext.request.contextPath}/user/messages"> <i class="fa fa-envelope"></i>
										Messages <span class="label label-info pull-right r-activity"></span>
								</a></li>
								<li><a href="#"> <i class="fa fa-calendar"></i> Events
								</a></li>
							</ul>
						</div>
					</div>

					<div class="widget">
						<div class="widget-body">
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#"> <i class="fa fa-globe"></i> Pages
								</a></li>
								<li><a href="#"> <i class="fa fa-gamepad"></i> Games
								</a></li>
								<li><a href="#"> <i class="fa fa-puzzle-piece"></i> Ads
								</a></li>
								<li><a href="#"> <i class="fa fa-users"></i> Groups
								</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- end left links -->
    