<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<nav class="navbar navbar-white navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.html"><b>YOArtist</b></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">

			<ul class="nav navbar-nav navbar-right">
				<li>
				<script type="text/ng-template" id="autoCompleteTemplate.html">
  					<a style="cursor:pointer;"> 
						<img ng-if="match.model.image==false" class="auto-image" src="/yoartist/resources/images/profile-pic.png">
						<img ng-if="match.model.image==true" class="auto-image" src="data:image/jpeg;base64,{{match.model.userImage}}">
      					<span class="auto-name" ng-bind-html="match.model.userName | uibTypeaheadHighlight:query"></span>
  					</a>
				</script>
					<div class="form-group container-fluid typeahead-demo" ng-controller="searchBarCtrl">
						<span class="input-icon inverted">						 
						<input type="text" style="margin-left: 32px;"
						ng-model="autoSelectInput" 
						placeholder="Enter name to search" 
						uib-typeahead="userName as user.userName for user in usersForSearchBox | filter:{userName:$viewValue}" 
						typeahead-template-url="autoCompleteTemplate.html" 
						class="form-control input-sm" 
						typeahead-show-hint="true" 
						typeahead-min-length="1"
						typeahead-on-select="onSelect($item)"> 
						<i class="glyphicon glyphicon-search bg-blue"></i>
						</span>
					</div></li>
				<li><a title="Home" href="${pageContext.request.contextPath}/user/home"><i class="fa fa-home fa-2x" aria-hidden="true" style="color: #2dc3e8;"></i></a></li>
				<li><a title="Shared To Me" href="${pageContext.request.contextPath}/user/home"><i class="fa fa-share-alt fa-2x" aria-hidden="true" style="color: #2dc3e8;"></i></a></li>
				<li><a title="Connections" href="${pageContext.request.contextPath}/user/connections"><i class="fa fa-users fa-2x" aria-hidden="true" style="color: #2dc3e8;"></i></a></li>
				<li><jsp:include page="connectionRequestHeader.jsp" /></li>
				<li><jsp:include page="notificationHeader.jsp" /></li>
				<li><jsp:include page="profileHeader.jsp" /></li>
				
			</ul>
		</div>
	</div>
</nav>