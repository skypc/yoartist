<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- <li class="dropdown"><a href="#" class="dropdown-toggle"
	data-toggle="dropdown" role="button" aria-haspopup="true"
	aria-expanded="false"> Profile <span class="caret"></span>
</a>
	<ul class="dropdown-menu">
		<li><a href="profile2.html">${sessionScope.SessionFullName}</a></li>
		<li><a href="profile3.html">Account Setting</a></li>
		<li><a href="profile3.html">Privacy Setting</a></li>
		<li><a href="${pageContext.request.contextPath}/user/logout">Logout</a></li>
		<li><a href="profile4.html">Profile 4</a></li>
		<li><a href="sidebar_profile.html">Sidebar profile</a></li>
		<li><a href="user_detail.html">User detail</a></li>
		<li><a href="edit_profile.html">Edit profile</a></li>
		<li><a href="about.html">About</a></li>
		<li><a href="friends.html">Friends</a></li>
		<li><a href="friends2.html">Friends 2</a></li>
		<li><a href="profile_wall.html">Profile wall</a></li>
		<li><a href="photos1.html">Photos 1</a></li>
		<li><a href="photos2.html">Photos 2</a></li>
		<li><a href="view_photo.html">View photo</a></li>
		<li><a href="messages1.html">Messages 1</a></li>
		<li><a href="messages2.html">Messages 2</a></li>
		<li><a href="group.html">Group</a></li>
		<li><a href="list_users.html">List users</a></li>
		<li><a href="file_manager.html">File manager</a></li>
		<li><a href="people_directory.html">People directory</a></li>
		<li><a href="list_posts.html">List posts</a></li>
		<li><a href="grid_posts.html">Grid posts</a></li>
		<li><a href="forms.html">Forms</a></li>
		<li><a href="buttons.html">Buttons</a></li>
		<li><a href="error404.html">Error 404</a></li>
		<li><a href="error500.html">Error 500</a></li>
		<li><a href="recover_password.html">Recover password</a></li>
		<li><a href="registration_mail.html">Registration mail</a></li>
	</ul></li> --%>
<a>
<div class="btn-group user-profile-box">
	<a class="btn user-profile-dropdown" href="${pageContext.request.contextPath}/user/profile"><i class="fa fa-user fa-fw"></i>
		${sessionScope.SessionFullName}</a> <a class="btn dropdown-toggle" style="background: #e4dfdf;" data-toggle="dropdown"
		href="#"> <span class="fa fa-caret-down user-dropdown-icon"
		title="Toggle dropdown menu"></span>
	</a>
	<ul class="dropdown-menu">
		<li><a href="#"><i class="fa fa-user fa-fw"></i> view profile</a></li>
		<li><a href="#"><i class="fa fa-pencil fa-fw"></i> Edit profile</a></li>
		<li class="divider"></li>
		<li><a href="#"><i class="fa fa-cog"></i> account setting<br>
		<li><a href="#"><i class="fa fa-shield"></i> privacy setting<br>
		<li class="divider"></li>
		<li><a href="${pageContext.request.contextPath}/user/logout"><i class="fa fa-power-off"></i> Logout</a></li>
	</ul>
</div>
</a>