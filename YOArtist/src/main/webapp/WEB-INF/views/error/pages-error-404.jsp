<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Error 404</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <link rel="stylesheet" type="text/css" id="theme"
			href="<c:url value='/resources/support/css/theme-default.css'/>">  
			
		<style type="text/css">
			#go-dashboard{
				text-decoration: none;
			}
		</style>                       
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        
            <!-- START PAGE SIDEBAR -->
            
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">

                            <div class="error-container">
                                <div class="error-code">404</div>
                                <div class="error-text">page not found</div>
                                <div class="error-subtext">Unfortunately we're having trouble loading the page you are looking for. Please wait a moment and try again or use action below.</div>
                                <div class="error-actions">                                
                                    <div class="row">
                                    	<div class="col-md-12">
                                        <div class="col-md-6" style="display: inline;">
                                            <a href="${pageContext.request.contextPath }/" id="go-dashboard"><button class="btn btn-info btn-lg">Back to dashboard</button></a>
                                        </div>
                                        <div class="col-md-6" style="display: inline;">
                                            <button class="btn btn-primary btn-lg" onClick="history.back();" style="background-color: #30a008;color: white;">Previous page</button>
                                        </div>
                                        </div>
                                    </div>                                
                                </div>
                                
                            </div>

                        </div>
                    </div>
                    
                                                                        
                </div>                
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
       
        <!-- END PAGE CONTAINER -->

        
    </body>
</html>






