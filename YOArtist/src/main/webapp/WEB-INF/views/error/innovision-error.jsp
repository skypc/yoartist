<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>Innovision Exception</title>

<link
	href="https://a1.muscache.com/airbnb/static/packages/common_o2.1-pretzel-63569659624b0120e962749578cae707.css"
	media="all" rel="stylesheet" type="text/css">


<!-- Replace by loading from stylesheet -->
<style type="text/css">
.text-ginormous {
	font-size: 145px;
}

.navbar .container.fixed-height {
	height: 46px;
}

.brand.airbnb.center {
	float: none;
	margin: 7px auto 0 auto;
}

#back-div {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	padding: 10px;
}

#back {
	background: #950006;
	background: -moz-linear-gradient(top, #de0009 0%, #950006 100%);
	background: -o-linear-gradient(top, #de0009 0%, #950006 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #de0009),
		color-stop(100%, #950006));
	background: -webkit-linear-gradient(top, #de0009 0%, #950006 100%);
	background: linear-gradient(top, #de0009 0%, #950006 100%);
	border: solid #dd0009;
	border-width: 1px 1px 1px 0;
	-moz-border-radius: 0 5px 5px 0;
	-webkit-border-radius: 0 5px 5px 0;
	border-radius: 0 5px 5px 0;
	display: inline-block;
	height: 28px;
	line-height: 28px;
	margin-left: 20px;
	margin-right: 2px;
	padding: 0 7px 0 3px;
	position: relative;
	text-decoration: none;
}

#back:before {
	background: #950006;
	background: -moz-linear-gradient(-45deg, #de0009 0%, #950006 100%);
	background: -o-linear-gradient(-45deg, #de0009 0%, #950006 100%);
	background: -webkit-gradient(linear, left top, right bottom, color-stop(0%, #de0009),
		color-stop(100%, #950006));
	background: -webkit-linear-gradient(-45deg, #de0009 0%, #950006 100%);
	background: linear-gradient(-45deg, #de0009 0%, #950006 100%);
	border: solid #dd0009;
	border-width: 0 0 1px 1px;
	border-radius: 2px 0 2px 2px;
	content: '';
	display: block;
	height: 21px;
	left: -11px;
	position: absolute;
	top: 3px;
	-moz-transform: rotate(45deg);
	-webkit-transform: rotate(45deg);
	-o-transform: rotate(45deg);
	transform: rotate(45deg);
	width: 21px;
}

#back:hover, #back:focus {
	border-color: #b50007;
	background: #7e0004;
	background: -moz-linear-gradient(top, #b50007 0%, #7e0004 100%);
	background: -o-linear-gradient(top, #b50007 0%, #7e0004 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #b50007),
		color-stop(100%, #7e0004));
	background: -webkit-linear-gradient(top, #b50007 0%, #7e0004 100%);
	background: linear-gradient(top, #b50007 0%, #7e0004 100%);
}

#back:hover:before, #back:focus:before {
	border-color: #b50007;
	background: #7e0004;
	background: -moz-linear-gradient(-45deg, #b50007 0%, #7e0004 100%);
	background: -o-linear-gradient(-45deg, #b50007 0%, #7e0004 100%);
	background: -webkit-gradient(linear, left top, right bottom, color-stop(0%, #b50007),
		color-stop(100%, #7e0004));
	background: -webkit-linear-gradient(-45deg, #b50007 0%, #7e0004 100%);
	background: linear-gradient(-45deg, #b50007 0%, #7e0004 100%);
}

#back #span-back {
	color: #fff;
	font-weight: bold;
	position: relative;
}
</style>
</head>

<body>
	<div id="header" class="navbar navbar-top">
		<div class="navbar-inner">
			<div
				class="fixed-height container container-full-width page-container page-container-responsive navbar-container">

			</div>
		</div>
	</div>

	<div class="page-container page-container-responsive">
		<div class="row row-space-top-8 row-space-8 row-table">
			<div class="col-5 col-middle">
				<h1 class="text-jumbo text-ginormous">Oops!</h1>
				<h2>Error code: ${status }</h2>
				<h6>Error message: ${message }</h6>
				<div id="back-div">
					<a href="#" id="back" onclick="goBack(event);"><span id="span-back"><h3>Back</h3></span></a>
				</div>
			</div>

			<div class="col-5 col-middle text-center">
				<img
					src="https://a0.muscache.com/airbnb/static/error_pages/404-Airbnb_final-554adc9e2190445fea6d7f0e3a30e67e.gif"
					width="313" height="428" alt="Girl has dropped her ice cream.">
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="<c:url value='/resources/js/plugins/jquery/jquery.min.js'/>"></script>
		
		<script type="text/javascript">
		
			function goBack(e)
			{
				e.preventDefault();
				window.history.back();
				
			}
		</script>
</body>
</html>
