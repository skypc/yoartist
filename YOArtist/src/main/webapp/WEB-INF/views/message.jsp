<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="img/favicon.png">


<link rel="stylesheet"
	href="<c:url value='/resources/support/bootstrap.3.3.6/css/bootstrap.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/font-awesome.4.6.1/css/font-awesome.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/animate.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/timeline.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/login_register.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/forms.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/buttons.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/messages1.css'/>">
<link href="<c:url value='/resources/skypc/css/common-user.css'/>"
	rel="stylesheet" />
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/cover.css'/>">

<link rel="stylesheet"
	href="<c:url value='/resources/support/angular/css/ngToast.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/support/angular/css/angular-material.min.css'/>" />
<link href="<c:url value='/resources/skypc/css/header.css'/>"
	rel="stylesheet" />

<link href="http://vjs.zencdn.net/5.8.8/video-js.css" rel="stylesheet">



<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="http://vjs.zencdn.net/5.8.8/video.js"></script>



<title>Home-YOArtist</title>
<style>
.postdesc {
	margin-top: 8px;
	font-size: 16px;
}


</style>
</head>

<body class="animated fadeIn" ng-app="homeApp">
	<input type="hidden" value="${pageContext.request.contextPath}"
		id="rootPath">

	<!-- Fixed navbar header-->
	<jsp:include page="include/header.jsp" />
	<toast></toast>

	<!-- Begin page content -->
	<div class="container page-content" id="homeApplication"
		ng-controller="message-controller as mc">
		<div class="row">
			<div class="col-md-4 bg-white">
				<div class=" row border-bottom padding-sm" style="height: 40px;margin-left:5px;">
					<strong>Messages</strong></div>
				<!-- member list -->
				<ul class="friend-list" ng-repeat="user in mc.messageUsers">
					
					<li ng-class="mc.currentChatId==user.userId?'active animated':''"><a href=""
						ng-click="mc.getMessageByUser(user.userId);" class="clearfix" ng-cloak>
							<img class="img-circle" ng-if="user.userImage"
							ng-src="data:image/jpeg;base64,{{user.userImage}}" alt=""> <img
							class="img-circle" ng-if="!user.userImage"
							ng-src="{{API_BASE}}/resources/images/profile-pic.png" alt="">
							<div class="friend-name">
								<strong>{{user.fullName}}</strong>
							</div>
					</a></li>
				</ul>
			</div>

			<!--=========================================================-->
			<!-- selected chat -->
			<div chat-message-list="mc.conversation" class="col-md-8 bg-white"  ng-if="mc.currentChatId>0">
				<div class="chat-message"
					style="max-height: 480px; overflow-y: auto">
					<ul class="chat" ng-repeat="chat in mc.conversation" ng-cloak>

						<li ng-class="chat.loginUser?'right clearfix':'left clearfix'">
							<span
							ng-class="chat.loginUser?'chat-img pull-right':'chat-img pull-left'">
								<img ng-if="user.userImage"
								src="data:image/jpeg;base64,{{user.userImage}}" alt=""> <img
								ng-if="!user.userImage"
								ng-src="{{API_BASE}}/resources/images/profile-pic.png" alt="">

						</span>
							<div class="chat-body clearfix">
								<div class="header">
									<strong class="primary-font">{{chat.fullName}}</strong> <small
										class="pull-right text-muted"><i class="fa fa-clock-o"></i>
										{{chat.time}}</small>
								</div>
								<p style="white-space:pre-wrap;">{{chat.message}}</p>
							</div>
						</li>						
					</ul>
				</div>
				
				<div class="chat-box_ bg-white" ng-if="mc.currentChatId>0">
				<form ng-submit="mc.sendMessageToUser();">
					<div class="input-group" style="display: flex;    height: 80px;" >
						
							<textarea ng-model="mc.inputMessage"
								class="form-control border no-shadow no-rounded"
								placeholder="Type your message here"></textarea> 
								<button class="btn btn-success no-rounded" type="submit">Send</button>
							
					</div>
						</form>
					<!-- /input-group -->
				</div>
			</div>
		</div>
	</div>



	<footer class="footer">
      <div class="container">
        <p class="text-muted"> Copyright &copy; Company - All rights reserved </p>
      </div>
    </footer>
</body>
<script
	src="<c:url value='/resources/support/assets/js/jquery-2.1.3.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/bootstrap.3.3.6/js/bootstrap.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/js/jquery.validate.min.js'/>"></script>
<script src="<c:url value='/resources/support/assets/js/custom.js'/>"></script>

<script
	src="<c:url value='/resources/support/angular/js/angular.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-animate.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-sanitize.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-modal-service.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-aria.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-messages.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-material.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/ui-bootstrap-tpls-2.3.0.js'/>"></script>

<script src="<c:url value='/resources/skypc/angular/app.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/filters/yo-filters.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/factories/user-get-factory.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/factories/notification-factory.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/factories/connection-factory.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/factories/message-factory.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/connection-controller.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/notification-controller.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/auto-complete-controller.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/connect-controller.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/message-controller.js'/>"></script>

</html>