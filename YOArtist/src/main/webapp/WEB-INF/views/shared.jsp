<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="img/favicon.png">


<link rel="stylesheet"
	href="<c:url value='/resources/support/bootstrap.3.3.6/css/bootstrap.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/font-awesome.4.6.1/css/font-awesome.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/animate.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/timeline.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/login_register.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/forms.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/buttons.css'/>">

<link rel="stylesheet"
	href="<c:url value='/resources/support/assets/css/cover.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/support/css/fileinput.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/support/angular/css/ngToast.min.css'/>" />
<link rel="stylesheet"
	href="<c:url value='/resources/support/angular/css/angular-material.min.css'/>" />
<link href="<c:url value='/resources/skypc/css/header.css'/>"
	rel="stylesheet" />
<link href="<c:url value='/resources/support/video/video-js.css'/>"
	rel="stylesheet" />
<link href="<c:url value='/resources/skypc/css/common-user.css'/>"
	rel="stylesheet" />
<script
	src="<c:url value='/resources/support/video/videojs-ie8.min.js'/>"></script>
<script src="<c:url value='/resources/support/video/video.js'/>"></script>


<title>Home-YOArtist</title>


</head>

<body class="animated fadeIn" ng-app="homeApp">
	<input type="hidden" value="${pageContext.request.contextPath}"
		id="rootPath">


	<!-- Fixed navbar header-->
	<jsp:include page="include/header.jsp" />
	<div style="height: 10px;"></div>

	<!-- Begin page content -->
	<div class="container page-content" id="homeApplication">
		<div class="row">
			<!-- left links -->
			<jsp:include page="include/leftmenu.jsp" />

			<!-- center posts -->
			<div class="col-md-6">

				<div class="row">
					<!-- left posts-->
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<!-- post state form -->
								<div class="box profile-info n-border-top">
									<div class="alert alert-danger" style="display: none;"
										id="errorDiv">
										<strong>Error!</strong> <span id="errorMsg"></span>
									</div>
									<form
										action="${pageContext.request.contextPath}/user/submitPost"
										method="post">
										<textarea class="form-control input-lg p-text-area"
											name="comments" id="comments" rows="4"
											placeholder="Whats in your mind today?"></textarea>
										<input type="hidden" name="uploadedId" id="uploadedId"
											value="0" />
										<div class="box-footer box-form">
											<button type="submit" class="btn btn-azure pull-right"
												onclick="return uploadPost();">Post</button>
											<ul class="nav nav-pills" id="uploadDiv">
												<li><a href="#" data-toggle="modal"
													data-target="#uploadModal"><i class="fa fa-camera"></i></a></li>
												<li><a href="#" data-toggle="modal"
													data-target="#uploadModal"><i class=" fa fa-film"></i></a></li>

											</ul>
											<ul class="nav" id="uploadedDataUI"
												style="display: none; width: 80%;">
												<li><span id="uploadedData"
													style="display: inline; color: #337ab7"></span><a
													id="removeLink" href="#" onclick="return deleteMedia();"
													style="display: inline; color: #ef0910;">remove</a></li>
											</ul>
										</div>
									</form>
								</div>
								<!-- end post state form -->

								<!--   posts -->
								<c:if test="${not empty posts}">
									<c:forEach var="post" items="${posts}">
										<div class="box box-widget">
											<div class="box-header with-border">
												<div class="user-block">
													<c:choose>
														<c:when test="${not empty post.userImage}">
															<img class="img-circle"
																src='<c:url value="data:image/jpeg;base64,${post.userImage}"></c:url>'
																alt="User Image">
														</c:when>
														<c:otherwise>
															<img class="img-circle"
																src='<c:url value="/resources/images/profile-pic.png"></c:url>'
																alt="User Image">
														</c:otherwise>
													</c:choose>


													<span class="username"><a href="#">${post.userName}</a></span>
													<span class="description">posted ${post.time }</span>
												</div>
											</div>

											<div class="box-body" style="display: block;">
												<c:set var="filePathValue" value=""></c:set>
												<c:if test="${not empty post.filePath}">
												<c:set var="filePathValue" value="/images/${post.filePath}"></c:set>
													<c:set var="fName" value="${fn:toLowerCase(post.fileName)}"></c:set>
													<c:if
														test="${fn:endsWith(fName,'jpg') || fn:endsWith(fName,'jpeg') || fn:endsWith(fName,'png') || fn:endsWith(fName,'gif')}">
														<img class="img-responsive show-in-modal"
															src="<c:url value="/images/${post.filePath}"></c:url>"
															alt="Photo">
													</c:if>
													<c:if
														test="${fn:endsWith(fName,'mp4') || fn:endsWith(fName,'flv') || fn:endsWith(post.fileName,'Wmv') || fn:endsWith(post.fileName,'avi')}">
														<c:set value="${fn:split('fName','.')}"
															var="separatorPosition" />
														<c:set
															value="${separatorPosition[fn:length(separatorPosition)-1]}"
															var="vidType" />

														<%-- 														<c:set var="vidType" value=""></c:set> --%>
														<video
															style="width: 100% !important;height: 300px !important;"
															id="my-video" class="video-js" controls preload="auto"
															poster="<c:url value="/resources/images/music.png "></c:url>"
															data-setup="{}"> <source
															src="<c:url value="/images/${post.filePath}"></c:url>"
															type='video/mp4'>

														<p class="vjs-no-js">
															To view this video please enable JavaScript, and consider
															upgrading to a web browser that <a
																href="http://videojs.com/html5-video-support/"
																target="_blank">supports HTML5 video</a>
														</p></video>
													</c:if>

												</c:if>
												<p class="postdesc">${post.postDesc}</p>
												<c:if test="${post.like}">
													<p id="likeStatus_${post.postId}">you like this post</p>
												</c:if>
												<c:if test="${!post.like}">
													<p id="likeStatus_${post.postId}"></p>
												</c:if>
												<div style="display: inline;"
													ng-controller="share-modal-controller as vm">
													<button type="button" class="btn btn-default btn-xs"
														postid="${post.postId}" postDesc="${post.postDesc}" postFilePath="${filePathValue}" 
														ng-click="vm.openShareModal($event)">
														<i class="fa fa-share"></i> Share
													</button>
	<script type="text/ng-template" id="share-modal.html">
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" ng-click="close('Cancel')" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Share post</h4>
                </div>
                <form name='share-form'>
                    <div class="modal-body">
						<img ng-if='postFilePath!="" ' src="{{postFilePath}}" class="share-model-image" >
                        <p class='text-muted'>{{postDesc}}</p>
                        <md-content md-autofocus
							class="md-padding autocomplete" 
							layout="column" 
							ng-if='share.type!="public"'>
                            	<md-contact-chips 
									ng-model="connectionToShare" 
									md-contacts="delayedQuerySearch($query)" 
									md-contact-name="userName" 
									md-contact-image="userImage" 
									md-require-match="true" 
									md-highlight-flags="i" 
									filter-selected="filterSelected" 
									placeholder="To" md-select-on-focus
									required>
                            	</md-contact-chips>
                        </md-content>
                    </div>
                    <div class="modal-footer">
                        <div class="form-group col-md-4 col-sm-4">
                            <select class="form-control" ng-model="share.type" ng-change='changeShareType()'>
                                <option value='public'><i class="fa fa-home fa-fw" aria-hidden="true"></i>public</option>
                                <option value='custom'>custom</option>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <button type="button" ng-click="close('No')" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" ng-click="sharePost(share.type)" ng-disabled='isShareDisabled && !connectionToShare.length>0' class="btn btn-primary" data-dismiss="modal">Share</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
	</script>												</div>
												<button type="button" class="btn btn-default btn-xs"
													onclick="like('${post.postId}');">

													<c:if test="${!post.like}">
														<i id="likeIcon_${post.postId}" class="fa fa-thumbs-o-up"></i>
														<font id="likeText_${post.postId}"> Like</font>
													</c:if>
													<c:if test="${post.like}">
														<i id="likeIcon_${post.postId}"
															class="fa fa-thumbs-o-down"></i>
														<font id="likeText_${post.postId}"> Unlike</font>
													</c:if>
												</button>
												<c:if test="${not empty post.commentList}">
													<c:set var="noComment"
														value="${fn:length(post.commentList)}"></c:set>
												</c:if>
												<c:if
													test="${post.commentList==null || empty post.commentList}">
													<c:set var="noComment" value="0"></c:set>
												</c:if>
												<span class="pull-right text-muted"> <c:if
														test="${ post.likeCount!=0}">
														<font id="likeCount_${post.postId}">${post.likeCount}Likes
															-</font>
													</c:if> <c:if test="${ noComment> 0}">
												${noComment} comment
												</c:if>
												</span>
											</div>
											<div class="box-footer box-comments" style="display: block;">
												<div id="commentBox_${post.postId}">
													<c:if test="${not empty post.commentList}">
														<c:forEach items="${post.commentList}" var="com">
															<div class="box-comment">
																<c:choose>
																	<c:when test="${not empty com.userImage}">
																		<img class="img-circle img-sm"
																			src='<c:url value="data:image/jpeg;base64,${com.userImage}"></c:url>'
																			alt="User Image">
																	</c:when>
																	<c:otherwise>
																		<img class="img-circle img-sm"
																			src='<c:url value="/resources/images/profile-pic.png"></c:url>'
																			alt="User Image">
																	</c:otherwise>
																</c:choose>

																<div class="comment-text">
																	<span class="username"> ${com.userName} <span
																		class="text-muted pull-right">${com.commentDate }</span>
																	</span> ${com.comment }
																</div>
															</div>


														</c:forEach>


													</c:if>
												</div>

												<div class="box-footer" style="display: block;">
													<form action="#"
														onsubmit="return addComment('${post.postId}','${sessionScope.SessionFullName}');"
														method="post">
														<c:choose>
															<c:when test="${not empty sessionScope.profileImage}">
																<img id="img_${post.postId}"
																	class="img-responsive img-circle img-sm"
																	src='<c:url value="data:image/jpeg;base64,${sessionScope.profileImage}"></c:url>'
																	alt="User Image">
															</c:when>
															<c:otherwise>
																<img id="img_${post.postId}"
																	class="img-responsive img-circle img-sm"
																	src='<c:url value="/resources/images/profile-pic.png"></c:url>'
																	alt="User Image">
															</c:otherwise>
														</c:choose>
														<div class="img-push">
															<input id="comment_${post.postId}" type="text"
																class="form-control input-sm"
																placeholder="Add a comment">
															<button type="submit" class="btn btn-azure"
																style="height: 25px; margin-top: 2px; padding-top: 1px; padding-left: 5px; padding-right: 5px">Comment</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</c:forEach>
								</c:if>
								<!--  end posts-->


							</div>
						</div>
					</div>
					<!-- end left posts-->
				</div>
			</div>
			<!-- end  center posts -->




			<!-- right posts -->
			<div class="col-md-3">
				<!-- Friends activity -->
				<div class="widget" ng-controller="userActivityController">
					<div class="widget-header">
						<h3 class="widget-caption">Connections activity</h3>
					</div>
					<div class="widget-body bordered-top bordered-sky">
						<div class="card">
							<div class="content">
								<ul class="list-unstyled team-members">
									<li ng-repeat="act in usersActivity" ng-cloak>
										<div class="row"
											ng-if="act.type=='POST_LIKE' || act.type=='POST_LIKE' || act.type=='POST_UNLIKE'">
											<div class="col-xs-3">
												<div class="avatar">
													<img ng-if="!act.userImage1 || act.userImage1==null"
														src="<c:url value="/resources/images/profile-pic.png"></c:url>"
														alt="img" class="img-circle img-no-padding img-responsive">
													<img ng-if="act.userImage1!=null"
														src="data:image/jpeg;base64,{{act.userImage1}}" alt="img"
														class="img-circle img-no-padding img-responsive">
												</div>
											</div>
											<div class="col-xs-9">
												<!-- <b><a href="#">{{act.connName1}}</a></b> -->
												<p style="margin: 0 0 0px;"
													ng-bind-html="act.desc | contextPathFilter"></p>
												<span class="timeago">{{act.time}}</span>
											</div>
										</div>
										<div class="row" ng-if="act.type=='ACCEPT_CONNECT'">
											<div class="col-xs-3">
												<div class="avatar">
													<img ng-if="!act.userImage1 || act.userImage1==null"
														src="<c:url value="/resources/images/profile-pic.png"></c:url>"
														alt="img" class="img-circle img-no-padding img-responsive">
													<img ng-if="act.userImage1!=null"
														src="data:image/jpeg;base64,{{act.userImage1}}" alt="img"
														class="img-circle img-no-padding img-responsive">
												</div>
											</div>
											<div class="col-xs-9">
												<!-- <b><a href="#">{{act.connName1}}</a></b> -->
												<p style="margin: 0 0 0px;word-break: break-word;"
													ng-bind-html="act.desc | contextPathFilter"></p>
												<span class="timeago">{{act.time}}</span>
											</div>
										</div>
									</li>


								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- End Friends activity -->
				<!-- People You May Know -->
				<div class="widget" ng-controller="connectController" ng-cloak>

					<div class="widget-header">
						<h3 class="widget-caption">People You May Know</h3>
					</div>
					<div class="widget-body bordered-top bordered-sky" style="">
						<div class="card">
							<div class="content">
								<p ng-if="sentFriendReq" style="">friend reuqest sent</p>
								<ul class="list-unstyled team-members">
									<li ng-repeat="u in users"
										ng-hide="u.userId==currentSelectedUserId">
										<div class="row">
											<div class="col-xs-3">
												<div class="avatar">
													<img ng-if="u.isImage"
														src="data:image/jpeg;base64,{{u.userImage}}"
														alt="Circle Image"
														class="img-circle img-no-padding img-responsive"> <img
														ng-if="!u.isImage"
														src="<c:url value="/resources/images/profile-pic.png
                                                                        "></c:url>"
														alt="Circle Image"
														class="img-circle img-no-padding img-responsive">
												</div>
											</div>
											<a
												href="${pageContext.request.contextPath}/user/profile/{{u.userId}}">
												<div class="col-xs-6">{{u.userName}}</div>
											</a>

											<div class="col-xs-3 text-right">
												<btn class="btn btn-sm btn-azure btn-icon"
													ng-click="sendConnectRequest(u.userId);"> <i
													class="fa fa-user-plus"></i> </btn>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- End people yout may know -->
			</div>
			<!-- end right posts -->
		</div>
	</div>

	<!-- upload model -->
	<div id="uploadModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Upload Media</h4>
				</div>
				<div class="modal-body">
					<form action="" id="uploadFileForm">
						<div class="form-group">
							<input type="file" id="uploadFile" class="file" name="uploadFile">
							<!-- <div id="kv-error-1" style="margin-top: 10px; display: none"></div>
						<div id="kv-success-1" class="alert alert-success fade in"
							style="margin-top: 10px; display: none"></div> -->

						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- upload modal -->
	<jsp:include page="include/footer.jsp" />
</body>
<script
	src="<c:url value='/resources/support/assets/js/jquery-2.1.3.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/bootstrap.3.3.6/js/bootstrap.min.js'/>"></script>
<script src="<c:url value='/resources/support/js/bootstrap-notify.js'/>"></script>
<script
	src="<c:url value='/resources/support/js/jquery.validate.min.js'/>"></script>
<script src="<c:url value='/resources/support/assets/js/custom.js'/>"></script>
<script src="<c:url value='/resources/support/js/fileinput.js'/>"></script>
<script src="<c:url value='/resources/skypc/js/post.js'/>"></script>
<script src="<c:url value='/resources/skypc/js/constants.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-animate.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-sanitize.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-aria.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-messages.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-modal-service.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-material.min.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/ui-bootstrap-tpls-2.3.0.js'/>"></script>
<script
	src="<c:url value='/resources/support/angular/js/angular-cookie.js'/>"></script>
<script src="<c:url value='/resources/skypc/angular/app.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/filters/yo-filters.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/factories/generic-factory.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/factories/user-get-factory.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/factories/notification-factory.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/factories/notify-alert-service.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/factories/connection-factory.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/connection-controller.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/notification-controller.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/auto-complete-controller.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/user-activity-controller.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/connect-controller.js'/>"></script>
<script
	src="<c:url value='/resources/skypc/angular/controller/share-modal-controller.js'/>"></script>



<script type="text/javascript">
	/* $("#file-4").fileinput({

		});
		$("#uploadFile").fileinput({
			uploadExtraData : {
				kvId : '10'
			},
			allowedFileExtensions : [ 'jpg', 'png', 'gif' ],
			showUploadedThumbs : false,
			uploadUrl : $("rootpath").val() + "/ajax/uerPostMedia", // server upload action
			uploadAsync : true,
		}); */

	function addComment(id, name) {
		var comment = $("#comment_" + id).val();
		var postId = id;
		$
				.post(
						$("#rootPath").val() + "/ajax/comment-on-post",
						{
							postId : postId,
							comment : comment
						},
						function(result) {
							var commentBox = '<div class="box-comment">';
							var source = $("#img_" + postId).prop('src');
							var source = $("#img_" + postId).prop('src');
							commentBox += '<img class="img-circle img-sm" src="' + source + '">';
							commentBox += '<div class="comment-text">';
							commentBox += '<span class="username">'
									+ name
									+ '<span class="text-muted pull-right">2 second ago</span></span>'
									+ comment;
							commentBox += '</div></div>';
							$("#comment_" + id).val('');
							$("#commentBox_" + postId).append(commentBox);

						});
		return false;
	}

	$('ul.notifications').on('click', function(event) {
		event.stopPropagation();
	});
	$(document).ready(function() {
		$('.fa-home').css('color','#067894');
	});
	$(document).ready(function() {
		var win = $(window);
		// Each time the user scrolls
		win.scroll(function() {
			// End of the document reached?
			if ($(document).height() - win.height() == win.scrollTop()) {
				$('#loading').show();
				/* $.ajax({
					url: 'get-post.php',
					dataType: 'html',
					success: function(html) {
						$('#posts').append(html);
						$('#loading').hide();
					}
				}); */
			}
		});
	});
</script>

</html>
