--Roles entry--
INSERT INTO roles (id,role_name,created_date,modified_date) values(1,'SUPER_ADMIN',NOW(),NOW());
INSERT INTO roles (id,role_name,created_date,modified_date) values(2,'SUB_ADMIN',NOW(),NOW());
INSERT INTO roles (id,role_name,created_date,modified_date) values(3,'ADMIN',NOW(),NOW());
INSERT INTO roles (id,role_name,created_date,modified_date) values(4,'USER',NOW(),NOW());
INSERT INTO roles (id,role_name,created_date,modified_date) values(5,'GUEST',NOW(),NOW());

--Admin user entry--
insert into user values (1 , 'mahavir park' , NULL , '8345645444' , 'india' , now() , 'developer2@perennialsys.com' , 'Developer' , NULL , NULL , NULL , NULL , 1 , 0 , 1 , 'Admin' , NULL , 'Perennial' , NULL , now() , NULL , NULL , '8krQ8AZGCi2EcOV1Qw5LNQ==' , NULL , NULL , 1 , 1 , 'manual' , NULL , 'Maharashtra' , 'ACTIVE' , NULL , 'admin' , 'admin123' , NULL , NULL , 0);

--admin role entry--
INSERT into user_role_map value(1,1);

INSERT INTO project_catagory VALUES (1,'2016-09-09 16:54:14','2016-09-09 16:54:14','Research and development'),(2,'2016-09-09 16:54:38','2016-09-09 16:54:38','Consulting');

INSERT INTO project_sub_catagory VALUES (1,'2016-09-09 17:23:06','2016-09-09 17:23:06','Ideation',1),(2,'2016-09-09 17:23:24','2016-09-09 17:23:24','Demonstration',1),(3,'2016-09-09 17:23:53','2016-09-09 17:23:53','Commercialization',1),(4,'2016-09-09 17:24:30','2016-09-09 17:24:30','Improvement',2),(5,'2016-09-09 17:24:42','2016-09-09 17:24:42','Analysis',2),(6,'2016-09-09 17:24:58','2016-09-09 17:24:58','Development',2);

alter table general_terms modify intellectual_property_desc text;
alter table deliverables_outcomes modify training_audience text;
alter table submit_proposal modify training_description text;
alter table submit_proposal modify training_assessment text;
alter table submit_proposal modify training_delivery_schedule text;
alter table submit_proposal modify training_venue text;
alter table submit_proposal modify withdraw_proposal_reason text;

update user set rating = 0;
update awarded_projects set rating_for_client = 0, rating_for_researcher=0;


